#    nocap unsaf aAppe  svec  java  cDyn  kvec
   0 0.000 0.000 0.000  0.000 0.006 0.000 0.000
  25 0.014 0.013 0.013  0.033 0.066 0.012 0.010
  50 0.020 0.020 0.017  0.041 0.116 0.015 0.013
  75 0.029 0.027 0.023  0.051 0.179 0.017 0.015
 100 0.035 0.033 0.029  0.056 0.227 0.018 0.017
 125 0.041 0.039 0.037  0.062 0.285 0.020 0.018
 150 0.049 0.046 0.041  0.073 0.374 0.023 0.021
 175 0.055 0.053 0.048  0.079 0.432 0.025 0.023
 200 0.062 0.058 0.054  0.085 0.482 0.026 0.024


# aAppend at 200 with uint8_t = .175  uint64_t = .190

# (/ (* 57 100) 61)
# (/ (* 51 100) 55)
# (/ (* 46 100) 49)
# bounds checking is 7% of speed

# (/ (* 41 100) 57)
# (/ (* 38 100) 51)
# (/ (* 36 100) 46)
# roughly 25% of speed is lost to aArray overheads

# (/ (* 25 100) 42)
# (/ (* 24 100) 38)
# (/ (* 22 100) 36)
# 40% of speed loss due to not storing capacity

# java stdvec aAppend realloc
# set xlabel "array length"; set ylabel "time"; set key left; unset ytics; set xtics 50 out; set mxtics 10; set xtics nomirror; set format x "%.0f"; set border 0; set linestyle 1 linetype 1 linewidth 7; set linestyle 2 linetype 2 linewidth 7; set linestyle 3 linetype 3 linewidth 7; set linestyle 4 linetype 4 linewidth 7; set linestyle 5 linetype 5 linewidth 7;        plot "plot.txt" using 1:4 smooth cspline linestyle 1 title "java", "plot.txt" using 1:2 smooth cspline linestyle 2 title "aArray", "plot.txt" using 1:3 smooth cspline linestyle 4 title "std::vector", "plot.txt" using 1:5 smooth cspline linestyle 5 title "realloc"

# without java
# set xlabel "array length"; set ylabel "time"; set key left; unset ytics; set xtics 50 out; set mxtics 10; set xtics nomirror; set format x "%.0f"; set border 0; set linestyle 1 linetype rgb "#f5eb2c" linewidth 8; set linestyle 2 linetype rgb "#f52ca5" linewidth 8; set linestyle 4 linetype rgb "#7d2cf5" linewidth 8;        plot "plot.txt" using 1:5 smooth cspline linestyle 1 title "realloc", "plot.txt" using 1:2 smooth cspline linestyle 2 title "aArray", "plot.txt" using 1:3 smooth cspline linestyle 4 title "std::vector";      set term png; set output "media/aArray.png"; replot


# set xlabel "array length"; set ylabel "time"; set key left; unset ytics; set xtics 50 out; set mxtics 10; set xtics nomirror; set format x "%.0f"; set border 0; set linestyle 2 linetype rgb "#2d7fee" linewidth 7; set linestyle 3 linetype 2 linewidth 7; set linestyle 4 linetype 3 linewidth 7; set linestyle 4 linetype 4 linewidth 7; set linestyle 5 linetype 5 linewidth 7;        plot "plot.txt" using 1:2 smooth cspline linestyle 3 title "aArray", "plot.txt" using 1:5 smooth cspline linestyle 5 title "realloc", "plot.txt" using 1:6 smooth cspline linestyle 4 title "kvec"


### possible sort plot
set term png; set output "../docs/graph_rough.png";
set term aqua font "Arial,12" size 345 270;
set term svg size 345,270; set output "../docs/graph_rough.svg"
set xlabel "array length"; set ylabel "time"; set key left; unset ytics; set xtics 1000 out; set mxtics 2; set xtics nomirror; set format x "%.0f"; set border 0; set linestyle 1 linetype rgb "#7d2cf5" linewidth 7; set linestyle 2 linetype rgb "#2d7fee" linewidth 7; set linestyle 3 linetype rgb "#ff6edd" linewidth 7; set linestyle 4 linetype rgb "#008c59" linewidth 7; set linestyle 5 linetype rgb "#00ac89" linewidth 7;
plot "plot_sort.txt" using 1:5 smooth cspline linestyle 4 title "std::stable\\_sort", "plot_sort.txt" using 1:6 smooth cspline linestyle 5 title "std::sort", "plot_sort.txt" using 1:4 smooth cspline linestyle 1 title "mergesort", "plot_sort.txt" using 1:3 smooth cspline linestyle 2 title "qsort", "plot_sort.txt" using 1:2 smooth cspline linestyle 3 title "aSort"
