/**
@file docs-forDoxygen.h
@mainpage

Never use doxygen again
=======================

## OVERVIEW

@htmlonly<noscript style="color:#b44">(javascript required for navigation)</noscript>@endhtmlonly

Arrays and strings are simple things.  Made simpler in c
- By being __safe:__  buffer overflow and other errors give clear messages
- __generic:__  arrays can have any datatype
- __fast:__  optionally uses <a href="#details" onclick="a_details.onclick(); document.body.scrollIntoView(); return false" class="info_link">less memory</a> too
- and __simple:__  as first priority

<div>Quick example:</div>
@code{.c}
char* arr = NULL;

aAppend(&arr, 'I', ' ', 'w', 'a', 'n', 't');
aMap(arr, capitalize_char); // capitalizes each char

arr[3] = 'O';        // standard assignment
aAt2(arr, 5, ' ');   // same as arr[5] = ' ', but overflow safe

// Append a null terminated array and 5 items from a second array
aAppendArray(&arr, SIZE_MAX, "chunky ", 5, "bacon, francis");

// At position 0 replace 1 item with a null terminated array
aReplaceArray(&arr, 0, 1, SIZE_MAX, "Everyone");

aWrite(arr); // prints "Everyone WON chunky bacon"
aFree(&arr);
@endcode

That `char*` could equally be `int*` or an array of pointers;  and still transparently be passed as a c array

Some disadvantages are:
- It is impossible to implement aArray without macros.  They are side-effect safe, and pre-reduced;  so you see the minimum.  But compiler messages due to missing commas, or type mismatches are still less than helpful.  Thankfully these are easy to fix, and more problematic errors, such as buffer overflow, are clear and can be extended with backtraces:

```
example_error.c:300: out of bounds (length=8 but pos=10)
```

- Because aArray is generic, some return values get type cast to reflect the array type.  Sadly `-pedantic` now enables `-Wno-unused-value`, and some functions may need `(void)` placed infront of them to silence this warning:  `(void)aLength2(array, newlen)`  

- This library has only been used in a few programs.  But passes unittests, valgrind, has been fuzzed, passes static analysis, and even bootstraps itself.  The unitests loc is just over 2x the source code

It runs as c99 under clang, gcc, and tcc;  on DragonFly, Linux, macOS, and Windows in 32 and 64bit.  It compiles with <span style="white-space:nowrap">`-pedantic -Wall -Wextra -Weverything`</span>

This page is self-contained documention for <a href="https://tse.gratis/aArray/aArray.h" class="info_link">aArray.h</a> release ($release$)

Public domain.  No restriction, no need to accredit.  “As freely as you have received, freely give”  --  Jesus




## HOW
Even when writing aArray, I wasn't aware it would be possible;  so the library has slowly transformed over several of years

Here are some comments on how it all works.  Implementation details come from <a href="http://graphics.stanford.edu/~seander/bithacks.html" class="info_link">bithacks</a>, <a href="http://www.hackersdelight.org/hdcodetxt/nlz.c.txt" class="info_link">hackersdelight</a>, <a href="https://github.com/BonzaiThePenguin/WikiSort" class="info_link">BonzaiThePenguin</a>, and the initial idea from <a href="http://sglib.sourceforge.net" class="info_link">sglib</a>


### How can c functions take an unknown number of arguments?
Normally functions with a variable number of arguments need a marker to show how many arguments they're given, such as with `stdargs.h` or `varargs.h`

aArray functions don't need a marker, yet their number of arguments is known.  Allowing them to be allocated and memcpy'd in one go, rather than iterated.  This means less effort for the programmer, and a faster runtime

It is side-effect safe and done through `__VA_ARGS__`.  This exapnds to a list of a macro's arguments, which `(type[]){__VA_ARGS__}` turns into an array, and `sizeof((type[]){__VA_ARGS__}) / sizeof(type)` uses to calculate the array length


### How can c functions be polymorphic?
Different types take up different amounts of space, and so specialized functions are required for each type.  But @ref aAppend can take any type

How much space each item of an array takes can be calculated with `sizeofType = sizeof(*array)`.  So we can have a function for each type;  then select the correct one with `typed_functions[sizeofType]`
Polymorphism and variable number of arguments combine together something like this:

@code{.c}
// get number of var-arg arguments, and make them generic
// results in:  arrayLength, (type[]){array}
#define AARRAY_Args(sizeofType, ...) \
  (sizeofType==1? sizeof((int8_t []){__VA_ARGS__}) / sizeof(int8_t ) : \
   sizeofType==2? sizeof((int16_t[]){__VA_ARGS__}) / sizeof(int16_t) : \
   sizeofType==4? sizeof((int32_t[]){__VA_ARGS__}) / sizeof(int32_t) : \
                  sizeof((int64_t[]){__VA_ARGS__}) / sizeof(int64_t)), \
  (sizeofType==1? (void*)(int8_t []){__VA_ARGS__} : \
   sizeofType==2? (void*)(int16_t[]){__VA_ARGS__} : \
   sizeofType==4? (void*)(int32_t[]){__VA_ARGS__} : \
                  (void*)(int64_t[]){__VA_ARGS__})

// select the correctly typed function from an array
#define aExample(arr, ...) \
  AARRAY_aExample_typed_functions[sizeof(*arr)](arr, \
  AARRAY_Args(sizeof(*arr), __VA_ARGS__))
@endcode

This code selects which function to call, then passes it both arrayLength, and the typecast array.  It should get compile time reduced to a single function call


### How can aArrays be zero-indexed c arrays and aArrays at the same time?
In memory, a c array looks like `{item0, item1, item2, ...}`

And an aArray looks like `{length, item0, item1, item2, ...}`

Which can't be zero-indexed.  But rather than returning a pointer to aArray's beginning, the api returns a pointer to `item0`;  making it appear the same as an array, so long as realloc/free isn't used

Then when aArray needs to get an array's length;  it simply moves the pointer back to the true beginning


### How can arrays only store their length --- what about their capacity?
Capacity tells us the length each array can grow to.  But this makes arrays larger;  since an extra variable is needed

Happily capacity can be safely guessed from an array's length

The NOCAPACITY variant of aArrays does this by growing in a fixed progression.  Their capacity is always one of 8, 16, 32, 64, 128, etc.  This means we can assume a NOCAPACITY aArray is its length rounded up to the next number in the progression, i.e. a length of 7 means the aArray's capacity must be at least 8, while a length of 9 fits into a capacity of 16

If the NOCAPACITY aArray length drops a lot, then goes up a little;  it may well cause a realloc;  since the length now fits into a smaller capacity

You could call this an unnecessary realloc, or you could call _not_ doing it an unnecessary waste of space

The default aArray does *not* do this:  it stores and doubles capacity.  Only reducing to smaller memory sizes with @ref aMem

@htmlonly<a name="pingpong" class="anchor" id="pingpong"></a>@endhtmlonly
### How does aSearch work?
@ref aSearch is standard binary search

`aSearchP` though is arguably a new.  I like to call it pingpong, because it works like table-tennis.  The ball is secant --- a poor man’s version of newton-raphson’s method --- this bounces around, using the last two bounces (array lookups), to predict where it should bounce next

This normally finds the answer in less steps than binary search.  But has the less than helpful property that it may also bounce for ever, or even bounce off the table.  Pingpong solves this by adding two bats.  These provide a reducing range, squeezing the ball in at every bounce; so it will always find an answer, and with less array lookups than secant alone

So pingpong uses many less array lookups than binary search...  But also spends a lot longer deciding where to look...  Which is faster in a real sense is a very hard question to answer

You would think that arrays of integers would be a worst case for pingpong --- but even here 32bit integers can barely express a random enough distribution to throw off pingpong.  Binary search is only a safe bet with very short arrays


### Tell me something fun
aArray bootstraps itself with a cppp 'pre-pre-processor'.  In no way related to ppap

@htmlonly<center><a href="https://vimeo.com/186404784"><img alt="silly film" src="//gratis.gitlab.io/aArray/cppp.jpg"></a></center>@endhtmlonly

Internally aArray functions are written out many times for each type.  For instance @ref aFold is written out 48 times:  once for each array type * each return type * 3.  The 3 because it can be passed a c function, block, or c++ lambda.  This duplication is most easily done with macros

But when a syntax error occurs, the compiler would print out all of those macros --- more than a little messy! --- so <a href="//gitlab.com/gratis/cppp" class="info_link">cppp</a> pre-pre-processes them away before the compiler sees them

And since cppp is written using aArray.h, and aArray.h is written using <a href="//gitlab.com/gratis/cppp" class="info_link">cppp</a>, we have a bootstrap loop, which forms part of the unittests each time aArray is regenerated

Thought for compiler writers:  a `#pragma silentmacro` would help improve error messages




## DETAILS
<center>![std::sort is slower](//gratis.gitlab.io/aArray/graph_sort.png)</center>

This shows the relative speed of different functions on macOS.  They are tested against 10 different types of dataset, and the average score is taken.  This is why qsort is slightly slower that mergesort.  For datasets above 30000 items:  mergesort becomes faster than @ref aSort when the data is pre-ordered, and qsort becomes faster when the data has many duplicates.  In general though @ref aSort performs extremely well at all array lengths, and is the fastest across a range of datasets

It was written by <a href="https://github.com/BonzaiThePenguin/WikiSort" class="info_link">BonzaiThePenguin</a>.  He's done seriously good work.  It is a stable sort and operates in a fixed amount of memory

`##define AARRAY_sortCache` fixes the memory to `512`, but you can redefine this to larger values;  possibly improving @ref aSort's performance for larger datasets
<br><br>


Now lets compare search using `bsearch` from Linux and BSD, and @ref aSearch from aArray.  The arrays graphed are of maximally random sorted integers.  64bit and 32bit ints respectively

<center>
  ![aSearch is fastest](//gratis.gitlab.io/aArray/graph_64.png)
  ![aSearchP is fastest](//gratis.gitlab.io/aArray/graph_32.png)</center>

I've profiled a range of situations and hardware, but the writers of the other algorithms will of done so aswell, so @ref aSearch may look fast, but the difference with BSD is small

`aSearchP` though does the worst in the first graph, and best in the second.  Actually both graphs attempt to put it in the worst light possible...  It is an algorithm I like to call <a class="info_link" href="#how" onclick="a_HOW.onclick(); document.getElementById('pingpong').scrollIntoView(); return false">pingpong</a> which should be worth using as arrays become longer, comparisons become slower, or as the dataset becomes more uniform

These graphs are of shorter arrays, and faster comparisons --- the first graph using random `int64_t` integers, and the second random `int32_t`.  Pingpong is still the fastest on the second, and easily fastest for `int16_t`.   It is much better at searching less random distributions, such as pointers, rather than `rand()`

Please be careful using a custom `cmp()` with either binary or pingpong.  This is not safe:  `aSearchPF(array, &index, key, ^(int a, int b){ return a-b; })`.  See @ref aSearchF for more details
<br><br>


If that was fun, I'd like to suggest an alternative search algorithm that I haven't implemented, but may be faster still:

Binary search jumps through large arrays in a set order.  Each iteration loading disparate parts of the array into memory (which is slow).  What you could do is reorder the array;  not from lowest to highest;  but into the order taken by binary search.  It is still binary search, but the first index tested is no-longer the middle, but the first.  The second index tested would be either the second or third.  Then from the second it would test either the 4th or 5th, and from the third it would test either the 6th or 7th

As this sequence becomes longer than a cacheline, you would split off the child search locations into a new block (with that block still placed into the same array, but so the cachelines get minmally loaded, and maximally used) 

I would be surprized if that was not faster than existing binary search --- and 5 years later I find out that it is;  3-5x faster, and called <a href="https://algorithmica.org/en/eytzinger">Eytzinger search</a> invented in the 16th century.  Note the remaining difference.  Binary is depth first order, Eytzinger is breadth fist, and we are breadth first, but in cacheline chunks.  This would be a more complex algorithm, but giving 2-5 branches for every cacheline, whereas Eytzinger gets that for the first, and only single branches per cacheline thereafter...
<br><br>


In comparisons of @ref aAppend v.s. `std::vector`, or <a href="https://attractivechaos.wordpress.com/2008/09/19/c-array-vs-c-vector/" class="info_link">kvec.h</a>, etc;  gcc puts aArray equal with `std::vector` for `int64_t*` arrays, and ahead for `int16_t*` arrays.  Sadly however clang gives @ref aAppend a 13% slow down compared to gcc


### Latest changes
- 2020 09 --- fixed docs for aCapacity
- 2020 09 --- removed need for maths.h
- 2020 07 --- aCapacity added
- 2020 07 --- fix aArray_NOCAPACITY lengths
- 2020 04 --- aSort now defaults to unsigned ordering
- 2020 04 --- Stride variant for pingpong search
- 2019 10 --- fix printing "%i64" for `INT64_MIN`

### Missing features
- aArray for 128bit integers? (Not doing)
- Reduce macro usage? (Blows up size of aArray.h)

No known bugs...


### LICENSE
The goal is not to limit, but to bless and benefit you as best I can

To that end, I place no restriction

Public domain.  No restriction.  No need to accredit

May the peace that passes understanding dwell in you.  And you, rooted and grounded in love, may you know even the breadth length height and depth of his love for you, that surpasses knowledge -- filling you to all the measure of the fullness of God

And to him who is able to do far more abundantly than all that we can ask or imagine, whatever you have faced and in every situation, may he protect you heart and your mind, unto eternity


### NOCAPACITY
NOCAPACITY functions stop arrays from tracking their capacity.  This saves about 8 bytes per array and occasionally causes the array to resize, reducing its length

NOCAPACITY (and STATIC) functions are simply variants of @ref aAppend, @ref aReplace, @ref aAppendArray, @ref aReplaceArray, @ref aDelete, @ref aConcat, @ref aMulti, @ref aMem, @ref aArray, and @ref aFree (`aAppend_NOCAPACITY`, `aReplace_NOCAPACITY`, ...)

You cannot mix NOCAPACITY, STATIC and normal api functions on the same array.  An array created with a NOCAPACITY function must continue to use them, until it is freed with `aFree_NOCAPACITY`


### STATIC
Sometimes you may want to handle memory yourself.  You can get this by using the `aAppend_STATIC` `aConcat_STATIC`, etc variants of the normal api functions

They throw `"out of capacity"` if the array would extend beyond the capacity set when they were created or last updated by `aArray_STATIC`

@code{.c}
char*arrSTATIC = aArray_STATIC(data, sizeof(size_t)*2 + 4, true);
aAppend_STATIC(&arrSTATIC, 1); // throws "out of capacity"

char*arrSTATIC = aArray_STATIC(data, sizeof(size_t)*2 + 4, true);
aLength2(arrSTATIC, 0);
aAppend_STATIC(&arrSTATIC, 1); // this is fine
@endcode

It is not safe to mix STATIC NOCAPACITY and normal functions on the same array

@code{.c}
aAppend_STATIC(&arrSTATIC, 1, 2, 3, 4);
aReplaceArray(&arrSTATIC, 0, 1, 5, replacement); // undetected memory corruption!!!
@endcode

But you can convert between the array types, for instance with `aConcat` `aConcat_STATIC` and `aConcat_NOCAPACITY`


### AARRAY_NOTYPEOF
aArray functions can return a mixture of types.  Which would require manual type casting;  but aArray assumes your compiler supports `__typeof` (or `__decltype` for c++).  This allows the function's return type to be automatically fixed

If your compiler doesn't support `__typeof` or `__decltype`, then you can disable this automatic fixing with `##define AARRAY_NOTYPEOF`

clang and gcc support `__typeof`, but will also sometimes emit warnings about unused returns.  You can silence these warnings by placing (void) infront of the function, for instance

@code{.c}
(void)aAt2(somewhere, 1, over_the_rainbow);
@endcode


### AARRAY_WARN
aArray functions can also recieve a mixture of types.  Which would require manual type casting;  aArray silences the specific clang and msvc warnings for just those parameters.  But this is not possible for gcc.  If you prefer to type cast manually for all compilers, then you can set `##define AARRAY_WARN`

If you then have a lot of manual type casts, you may prefer `AARRAY_nowarn_start`


### AARRAY_nowarn_start
Anything between `AARRAY_nowarn_start`/`AARRAY_nowarn_end` has their `C4047` differing level of indirection warning suppressed (for msvc), or `-Wconversion`, `-Wint-conversion`, `-Wpointer-to-int-cast`, and `-Wbad-function-cast` (for clang and gcc).  They don't both need these, but it's simpler if they both get the same

This may save some casts when using gcc or `AARRAY_WARN`

For example `bad-function-cast` is disabled even under `-Wall` and `-Wextra`.  Enabling it has no effect on gcc but clang would then require `aAt(mystructs,&nbsp;N)->this` to be rewritten as `((mystruct)aAt((uintptr_t*)mystructs, N))->this`

If compiling for clang with c++ then you may also want `AARRAY_nowarn_pedantic_cpp_start`/`AARRAY_nowarn_pedantic_cpp_end`.  But in general don't use aArray with c++

aArray aims to use the same function calls for different array types, but c++ restricts function type casts.  For instance `f(int)` to `f(unsigned int)`, and `f(char*)` to `f(void*)` is undefined behavior in c++.  So while aArray does unittest for c++ conformity, it is not something I advise.  Using c does not have this issue (or any other known UB, etc problems)


### AARRAY_UNSAFE
Defining this before including `aArray.h` removes the bounds and other safety checks, for a possible speed increase


### AARRAY_c and AARRAY_h
`aArray.h` can be included as a single header.  But if you prefer, it can also be a dual header/implementation pair

@code{.c}
#include "aArray.h"
@endcode

Becomes

@code{.c}
#define AARRAY_h
#include "aArray.h"
@endcode

And a separately compiled `.c` file contains

@code{.c}
#define AARRAY_c
#include "aArray.h"
@endcode


### AARRAY_NOCONVENIENCE
Under the assumption that you use aArray so much it becomes tiresome to see it everywhere, some mnemonics are defined for your convenience.  These can be turned off with `##define AARRAY_NOCONVENIENCE`

The most useful is probably `aStr("string")` which converts `"string"` into an aArray string.  Note that the terminating `NULL` is dropped.  This simplifies concatenation, but means you probably want to print them with @ref aWrite, or @ref aFmt with `"%v%c"`, or append ```'\0'``` by hand with ```aA(&str, '\0')```

Also @ref aMap @ref aFilter @ref aFold @ref aLoop @ref aSortF @ref aSearchF @ref, `aSearchPF` and @ref aSearchPS can take either c-functions, <a href="https://stackoverflow.com/questions/5907071/clang-block-in-linux" class="info_link">blocks</a>, or c++ lambdas.  They do this with their underlying `aMap_FUNC`, `aMap_BLOCK`, and `aMap_LAMBDA`.  You can call them explicitly, or use @ref aMap ---  which simply links to whichever of those is enabled by your compiler settings.  So if blocks are enabled, but you are passing a c-function, you will need to be explicit and call `aMap_FUNC`, since @ref aMap will link to `aMap_BLOCK`

Then for those of us who like acronyms, there are:<br>
`aA` → @ref aAppend <br>
`aR` → @ref aReplace <br>
`aAA` → @ref aAppendArray <br>
`aRA` → @ref aReplaceArray <br>
`aD` → @ref aDelete <br>
`aC` → @ref aConcat <br>

`aL` → @ref aLength <br>
`aL2` → @ref aLength2 <br>
`aZL2` → @ref aZLength2 <br>

`aF` → @ref aFmt <br>
`aFE` → @ref aFmtE <br>
`aFF` → @ref aFmtF <br>
`aFA` → @ref aFmtA <br>
`aW` → @ref aWrite <br>
`aWE` → @ref aWriteE <br>
`aWF` → @ref aWriteF <br>

In the compiler I'm writing, 65.5% of function calls are from aArray.  So this library is surprizingly useful;  and the above acronyms help keep my code succinct




## THANKS
This would not of been possible without my family.  I wouldn't even of begun programming without them.  And thankyou to my lovely wife who is so generous to me

Many tools were used in creating aArray.  Particularly helpful ones include <a href="https://www.crummy.com/software/BeautifulSoup/zine/" class="info_link">BeautifulSoup</a>, <a href="https://ninja-build.org/" class="info_link">ninja</a>, <a href="http://lcamtuf.coredump.cx/afl/" class="info_link">afl</a> (especially the rabbit photos), <a href="https://fossil-scm.org/index.html/doc/trunk/www/index.wiki" class="info_link">fossil</a> (safety+usability), <a href="https://www.dragonflybsd.org/" class="info_link">DragonFlyBSD</a>, and <a href="http://kakoune.org/" class="info_link">Kakoune</a>

### My life
Our pastor imprisoned, for ‘attempting to overthrow the government’  <a class="info_link" href="/Joy_in_prison/">Joy in prison</a><br>
We could see them running around, screaming...  There was nothing I could do  <a class="info_link" href="/Liras_rain/">Lira’s rain</a><br>
My first animated short, of some simple miracles  <a class="info_link" href="/Animation/">Animation</a><br>
Genesis is different.  Vegetarianism explains why  <a class="info_link" href="/Creative_Snails/">Creative Snails</a><br> **/






/**
 * Appends one or more `items` to `aArray`
 *
 * `any` represents any type other than `void` upto 8 bytes in width; be it `char`, `double`, `clock_t`, `FILE*`, `long long`, etc
 * 
 * If `aArray` is `NULL`, it will be initialized.  A zero-length aArray is equivalent to a `NULL` aArray --- as in api calls on either have the same effect.  This should result in less `NULL` pointer checking, as it is a safe value
 *
 * @ref aAppend can take an arbitrary number of items.  It is not necessary to give the number of arguments, or mark their end
 *
 * @returns The aArray, with new `items` added.  If the aArray had to be moved in memory (to accommodate the new items) then `*aArray` is also updated to point to it
 * @throws ;;exception;; "array type too wide", "out of memory"
 *
 * Exceptions are not c++ exceptions.  `"array type too wide"` is a compile-time error.  `"out of memory"` is passed to the @ref aError handler
 *
@code{.c}
int *arr = NULL;
aAppend(&arr, 1, 2);
aAppend(&arr, 99, 100);
aFmt("{%v, %i}", arr); // prints "{+1, +2, +99, +100}"
@endcode **/
any* aAppend(any** aArray, any items, ...);
/**
 * Replaces `removeAmount` items at position `pos` in `aArray` with one or more `items`
 *
 * @returns The updated aArray, and `*aArray` now pointing to it
 * @throws ;;exception;; "array type too wide", "out of memory", "out of bounds", "removal is out of bounds"
 *
@code{.c}
unsigned int *joy = aAppend((unsigned int**)NULL, 10, 5, 0, -5, -20);
aFmt("[%v, %u]", aReplace(&joy, 2, 3, 10, 20)); // prints "[10, 5, 10, 20]"
@endcode **/
any* aReplace(any** aArray, size_t pos, size_t removeAmount, any items, ...);
/**
 * Appends one or more `array`s to the end of `aArray`
 *
 * `arrayLen` is the number of `array` items to be appended.  If `arrayLen` is `SIZE_MAX` then `array` is appended upto but not including the first `NULL`.  This makes it easy to append `char*` strings for instance
 *
 * Multiple `array`s can be passed to @ref aAppendArray;  each `array` must be preceded by it's own `arrayLen`
 *
 * @returns The updated aArray, and `*aArray` now pointing to it
 * @throws ;;exception;; "array type too wide", "out of memory", "wrong arg count", "array is NULL"
 *
 * Three of those exceptions simply check parameter validity
 *
@code{.c}
int ages[3] = {0, 5, 10};
aAppendArray((int**)NULL,
  3, ages,
  3, ages,
  2, (int[]){12, 14}); // (int*){0, 5, 10, 0, 5, 10, 12, 14}
@endcode
 *
 * There are convenience macros that abbreviate `aAppendArray` to `aAA` and for instance `aReplace` to `aR`
 *
 * You can switch them off by defining `AARRAY_NOCONVENIENCE`.  But there is also an abbreviation of `aAppendArray` to `aStr`, which simplifies strings
 *
@code{.c}
char* hi = aStr("hello world");
aR(&hi, 0, 1, 'j'); // "jello world"
@endcode **/
any* aAppendArray(any** aArray, size_t arrayLen, any* array, ...);
/**
 * Replaces `removeAmount` items at position `pos` in `aArray` with one or more `array`s
 *
 * `arrayLen` is the number of `array` items to be inserted
 *
 * Multiple `array`s can be passed;  each must be preceded by it's own `arrayLen`
 *
 * @returns The updated aArray, and `*aArray` now pointing to it
 * @throws ;;exception;; "array type too wide", "out of memory", "out of bounds", "removal is out of bounds", "wrong arg count", "array is NULL"
 *
 * Passing repeats of `aArray`, or pointers into `aArray` are safe for all aArray manipulating functions, though the data is overwritten from left to right
 *
@code{.c}
int* nums = NULL;
aAppend(&nums, 1, 2, 3, 4, 100);
aReplaceArray(&nums, 2, 2,
  1, nums,
  2, aAtPtr(nums, 1)); // {1, 2, 1, 2, 1, 100}
@endcode **/
any* aReplaceArray(any** aArray, size_t pos, size_t removeAmount, size_t arrayLen, any* array, ...);

/**
 * @} @{
 * Deletes `amount` items from `aArray` at position `pos`
 *
 * @returns The updated aArray, and `*aArray` now pointing to it
 * @throws ;;exception;; "array type too wide", "out of bounds", "removal is out of bounds"
 *
@code{.c}
long* medalsWonBy_aArray = aAppend((long**)NULL, 15, 26, (long)-1);
aDelete(&medalsWonBy_aArray, 0, 3);
// length of array is now sadly zero
@endcode **/
any* aDelete(any** aArray, size_t pos,  size_t amount);
/**
 * Appends one or more `next_aArray`s to the end of `aArray`
 *
 * Really this is a simple convenience -- it does the same as @ref aAppendArray, but without needing to specify each `next_aArray` length
 *
 * @returns The updated aArray, and `*aArray` now pointing to it
 * @throws ;;exception;; "array type too wide", "out of memory"
 * 
@code{.c}
char* example = aStr("A ");
char* silly = aStr("silly ");

aConcat(&example, silly, silly, example, example);
// "A silly silly A A "
@endcode **/
any* aConcat(any** aArray, any* next_aArray, ...);
/**
 * Other than making your code look complex, @ref aMulti lets you
 * 
 * 1. Create un-initialized aArrays
 * 2. Create aArrays with repeating items
 * 3. Combine multiple aArray updates into a single step
 *
 * `arrayTimes` is the amount of times to insert `arrayLen` from `array` into `aArray`
 * 
 *  If `arrayTimes` is `0` then `arrayLen` un-initialized items will be inserted
 *
 * @returns The updated aArray, and `*aArray` now pointing to it
 * @throws ;;exception;; "array type too wide", "out of memory", "out of bounds", "removal is out of bounds", "wrong arg count", "array is NULL"
 *
@code{.c}
// length=200, every item is 0
aMulti(&data, 0, 0, 200, 1, (int8_t[]){0});
  
// length=200, every item is uninitialized
aMulti(&data, 0, 0, 0, 200, NULL);
@endcode
 *
 * Also
 *
@code{.c}
// Combine aArray updates reduced to a single call
unsigned int* va = NULL;
aAppend(&va, 1, 2, 3);
aReplace(&va, 0, 0, -2, -1, 0);
aReplaceArray(&va, 1, 3, 1, (unsigned int[]){5});
aDelete(&va, aLength(va)-1, 1);
// va now {-2, 5, 2}

unsigned int* vb = NULL;
aMulti(&vb,
  0, 0, 1, 3, (unsigned int[]){1, 2, 3},
  0, 0, 1, 3, (unsigned int[]){-2, -1, 0},
  1, 3, 1, 1, (unsigned int[]){5},
  3, 1, 0, 0, NULL);
// vb now {-2, 5, 2}, but done in one malloc
@endcode **/
any* aMulti(any** aArray, size_t pos, size_t removeAmount, size_t arrayTimes, size_t arrayLen, any* array, ...);
/**
 * Converts `aArray` to a typed memory pointer
 * 
 * @returns Pointer to aArray's memory
 * @throws ;;exception; "array type too wide"
 * 
@code{.c}
// shrink memory to arr length
size_t limit = sizeof(size_t)*2 + aL(arr);
arr = aArray(realloc(aMem(arr), limit), limit, false);

// manipulate memory
size_t length;
char*data = serialize(aMem(arr), &length);
arr = deserialize(aArray(data, length, false));
@endcode **/
any* aMem(any* aArray);
/**
 * Converts type-cast `memptr` of length `size` into an aArray
 *
 * If `maximize` or aArray would overflow, then it's length is set to the maximum within `size`
 *
 * This allows the STATIC variant of aArrays to resize, or for aArrays to be backed by mmap, aalloc, etc
 * 
 * @returns an any* aArray
 * @throws ;;exception;; "array type too wide", "array is NULL", "out of capacity"
 * 
 * Default and STATIC aArrays require an extra `sizeof(size_t)*2` bytes.  NOCAPACITY aArrays require an extra `sizeof(size_t)` bytes.  The "out of capacity exception" is for when these minimums are not met
 * 
@code{.c}
char*x = aArray((char*)malloc(200), 200, true);
aF("%uz", aL(x)); // prints 184
aF("%c", aAt(x, 0)); // prints uninitialized byte
free(aMem(x));

// STATIC aArrays do not realloc,
// instead they throw "out of capacity"
x = aArray_STATIC((char*)malloc(1000), sizeof(size_t)*2 + 3, false);
aL2(x, 0); // length now 0
aAppend_STATIC(&x, 1, 2, 3); // at max length

// set capacity to 4
x = aArray_STATIC(aMem(x), sizeof(size_t)*2 + 4, false);
if(aL(x)==3) aAppend_STATIC(&x, 4); // [1, 2, 3, 4]
@endcode **/
any* aArray(any* memptr, size_t size, bool maximize);
/**
 * Frees `aArray`, setting `*aArray` to `NULL`
 *
 * @note I used to forget `&` de-referencing the pointer to `aArray`, so now it forces a compiler error
 *
@code{.c}
any* myArr = aAppend((any**)NULL, 'x');
aFree(&myArr); // myArr is now NULL
@endcode **/
void aFree(any** aArray);



/**
 * @} @{
 * @returns The length of `aArray`
 *
 * `aLength(nullvar)` returns `0` --- since `NULL` is considered equivalent to a zero-length aArray **/
size_t aLength(any* aArray);
/**
 * Sets `aArray` length to an equal or shorter `len`
 * 
 * @returns The last item still in `aArray`.  I.e. `aArray[len-1]`, or `0` if `len` is `0`
 * @throws ;;exception;; "array type too wide", "out of bounds"
 *
@code{.c}
int* fib = NULL; aAppend(&fib, 1, 1, 2, 3, 5);
aFmt("Prev fib = %uz", aLength2(fib, aLength(fib)-1));
// fib length reduced by one, and 3 printed
  
aFmt("\nlen = %uz", aLength(fib)); // 4
aLength2(fib, 2);
aFmt("\nlen = %uz", aLength(fib)); // 2
@endcode **/
any aLength2(any* aArray, size_t len);
/**
 * Reduces `aArray` length by `len`
 *
 * @returns The last item still in `aArray`.  I.e. `aArray[new_length-1]`, or `0` if new length would be `0`
 * @throws ;;exception;; "array type too wide", "out of bounds"
 *
 * `aZL2(array, 1)` abbreviates @ref aZLength2 and is a variation on "`pop()`".
 * 
@code{.c}
int* fib = NULL; aAppend(&fib, 1, 1, 2, 3, 12);
aZLength2(fib, 1); // removes 12
aFmt("last item = %u", aZLength2(fib, 1)); // removes 3, prints 'last item = 2'
@endcode **/
any aZLength2(any* aArray, size_t len);
/**
 * @returns The capacity of `aArray`
 *
@code{.c}
// 2*sizeof(size_t) is where aArray stores capacity+length
size_t size = 15 + 2*sizeof(size_t);
char* blah = aArray((char*)malloc(size), size, true);
aFA(&out, "%uz ", aCapacity(blah)); // 15
aL2(blah, 3); // aka aLength2
aFA(&out, "%uz ", aCapacity(blah)); // 15
aFree(&blah);

blah = aArray_STATIC((char*)malloc(size), size, true);
aFA(&out, "%uz ", aCapacity_STATIC(blah)); // 15
aL2(blah, 3);
aFA(&out, "%uz ", aCapacity_STATIC(blah)); // 15
aFree_STATIC(&blah);

size = 15 + sizeof(size_t);
blah = aArray_NOCAPACITY((char*)malloc(size), size, true);
// capacity is lowest power-of-two that fits
aFA(&out, "%uz ", aCapacity_NOCAPACITY(blah)); // 8
aL2(blah, 3);
aFA(&out, "%uz ", aCapacity_NOCAPACITY(blah)); // 4
aFree_NOCAPACITY(&blah);
@endcode **/
size_t aCapacity(any* aArray);

/**
 * @} @{
 * @returns The item at position `pos` in `aArray`, indexing from zero
 * @throws ;;exception;; "array type too wide", "out of bounds"
 *
 * A `pos` past the end of `aArray` throws an exception.  Otherwise `aArray[pos]` is identical
 *
@code{.c}
char* equal = aAppend((char**)NULL, 'a', 'b');
// NOTE: cast not needed if your compiler has __typeof or __decltype
if(equal[0] == (char)aAt(equal, 0)) printf("equal"); // "equal"
@endcode **/
any aAt(any* aArray, size_t pos);
/**
 * @returns The item at position `pos` from the end of `aArray`, indexing from zero
 * @throws ;;exception;; "array type too wide", "out of bounds"
 *
 * A `pos` past the beginning of `aArray` throws an exception
 *
@code{.c}
char* letters = aAppend((char**)NULL, 'a', 'r', 't');
aReplace(&letters, 0, 0, aZAt(letters, 0));
aFmt("%v %c", letters); // "t a r t"
@endcode **/
any aZAt(any* aArray, size_t pos);
/**
 * Sets the item at position `pos` in `aArray` to `newItem`
 *
 * @returns `newItem`
 * @throws ;;exception;; "array type too wide", "out of bounds"
 *
@code{.c}
char* letters = aAppend((char**)NULL, 'a', 'b');
aAt2(letters, 1, 'd'); // ['a', 'd']
@endcode **/
any aAt2(any* aArray, size_t pos, any newItem);
/**
 * Sets the item at position `pos` from the end of `aArray` to `newItem`
 *
 * @returns `newItem`
 * @throws ;;exception;; "array type too wide", "out of bounds"
 *
@code{.c}
char* letters = aAppend((char**)NULL, 'a', 'b');
aZAt2(letters, 0, 'd'); // 'a', 'd'
@endcode **/
any aZAt2(any* aArray, size_t pos, any newItem);
/**
 * @returns A pointer to the item at position `pos` in `aArray`
 * @throws ;;exception;; "array type too wide", "out of bounds"
 *
@code{.c}
char* equal = aAppend((char**)NULL, 'a', 'b');
// NOTE: casts not needed if your compiler has __typeof or __decltype
*(char*)aAtPtr(equal, 0) = 'b';
if(equal[0] == (char)aAt(equal, 1)) aFmt("equal"); // "equal"
@endcode
 **/
any* aAtPtr(any* aArray, size_t pos);
/**
 * @returns A pointer to the item at position `pos` from the end of `aArray`
 * @throws ;;exception;; "array type too wide", "out of bounds"
 *
@code{.c}
char* letters = aAppend((char**)NULL, 'n', 'o', '\0');
*(char*)aZAtPtr(letters, 2) = 'l';
printf(letters); // "lo"
@endcode **/
any* aZAtPtr(any* aArray, size_t pos);
/**
 * Finds the first index of `item` in `aArray`
 *
 * @returns first index of `item` in `aArray`, or `-1`
 * @throws ;;exception;; "array type too wide"
 * 
@code{.c}
int* PPS_array = aAppendArray((int**)NULL, numPidgeons, pidgeonPeckSpeed_data);
aIndexOf(PPS_array, 200); // first occuring pidgeon with a peck speed of 200
@endcode **/
size_t aIndexOf(any* aArray, any item);
/**
 * Finds the last index of `item` in `aArray`
 *
 * @returns last index of `item` in `aArray`, or `-1`
 * @throws ;;exception;; "array type too wide"
 *
@code{.c}
int* PPS_array = aAppendArray((int**)NULL, numPidgeons, pidgeonPeckSpeed_data);
aZIndexOf(PPS_array, 190); // last occuring pidgeon with a peck speed of 190
@endcode **/
size_t aZIndexOf(any* aArray, any item);




/**
 * @} @{
 * Apply `f` to every item in `aArray`
 *
 * `f` can be a block, c++11 lambda, or function-pointer
 *
 * Optionally use:  `void aMap(any*, void(*f)(any*,void*data), void*data)`
 * 
 * @throws ;;exception;; "array type too wide", "parameter is NULL"
 * 
 * There are actually 3 api functions here.  `aMap_BLOCK`, `aMap_LAMBDA`, `aMap_FUNC`.  `aMap` simply maps to the first of those available.  If the environment has blocks, then `aMap_BLOCK`, otherwise if lambdas, then `aMap_LAMBDA`, and otherwise `aMap_FUNC`.  You can always call the specific function directly if you prefer
 *
 * `aFilter`, `aFold`, and `aLoop` handle blocks, lambdas, and function-pointers in the same way
 * 
@code{.c}
// ride every pony!
pony**my_little_ponies = ...;
void (*ride_a_pony)(pony**) = oh_what_fun_function;
aMap_FUNC(my_little_ponies, ride_a_pony);

char*rider_says = "Yesss!";
aMap_BLOCK(my_little_ponies,
  ^(pony**p, void*data){
    printf("ride %s? %s", (*p)->name, (char*)data); },
  rider_says);
@endcode **/
void aMap(any* aArray, void (*f)(any*item)));
/**
 * Apply `f` to every item in `aArray`.  Keep the `item` in `aArray` if `f` returns `true`
 *
 * `f` can be a block, c++11 lambda, or function-pointer
 * 
 * Optionally use:  `any* aFilter(any*, int(*f)(any,void*data), void*data)`
 *
 * @throws ;;exception;; "array type too wide", "parameter is NULL"
 *
@code{.c}
int* n1 = aAppend((int**)NULL, 4,5,6,7,8);
aFilter_FUNC(n1, isEven);  // n1 = [4,6,8]
aFilter_BLOCK(n1, ^(int item){ return item > 5; });  // n1 = [6,8]
aFilter_LAMBDA(n1, [](int item){ return item != 8; });  // n1 = [6]
@endcode **/
any* aFilter(any* aArray, int(*f)(any item));
/**
 * Do `base = f(base, item)` for every item in `aArray`
 *
 * `f` can be a block, c++11 lambda, or function-pointer
 * 
 * Optionally use:  `anyB aFold(any*, anyB, anyB(*f)(anyB,any,void*data), void*data)`
 *
 * @returns the final `base`
 * @throws ;;exception;; "array type too wide", "parameter is NULL"
 *
 * This is a nice simple way to combine every item of an aArray into a single result
 * 
@code{.c}
int* numbers = aAppendArray((int**)NULL, 10, numbersArray);
aFmt("\nSum     = %u64", aFold_FUNC(numbers, 0, add_function, NULL));
aFmt("\nProduct = %u64", aFold_BLOCK(numbers, (size_t)1, ^(size_t base, int item){ return base*item; }));

auto f = [](bool base, int item) { return base&&item; };
aFmt("\nAllTrue = %u8", (uintptr_t)aFold_LAMBDA(numbers, true, f));

char* baseB = NULL;
aFmt("\nString  = %v%c", *aFold_BLOCK(numbers, &baseB,
  ^(char** baseBPtr, int item){ (void)aAppend(baseBPtr, (char)(item+64)); return baseBPtr; }));
@endcode **/
anyB aFold(any* aArray, anyB base, anyB(*f)(anyB base, any item));
/**
 * This is a while loop that hopefully keeps your code safer and cleaner
 *
 * Starting from `startPos`, and until out of bounds, or `f` returns `0`, do `pos += f(pos)`
 *
 * `f` can be a block, c++11 lambda, or function-pointer
 * 
 * Optionally use:  `int aLoop(any*, size_t, int(*f)(size_t,void*data), void*data)`
 *
 * @returns `f`'s last returned value
 * @throws ;;exception;; "array type too wide", "parameter is NULL"
 *
 * `aLoop` handles the rarer issues of integer sign conversion and overflow at `pos += f(pos)`; aswell as buffer overflow.  With blocks it becomes:
@code{.c}
aLoop(aArray, startPos, ^(size_t pos){
  // do my stuff
  return deltaPos; } );
@endcode
 * <div style="margin-top:14px">For instance:</div> 
@code{.c}
unsigned int* a = aAppend((unsigned int**)NULL, 1, 2, 3, 4);

// print index 0 and 2
aLoop(a, 0, ^(size_t n) {
  // (uintptr_t) cast here even for clang, because it can miss _Pragmas
  aFmt((uintptr_t)"a[%ull] == %ui\n", n, aAt(a, n)); return 2; });

// print index 3 and 1, then set them to new values
aLoop(a, 3, ^(size_t n){
  aFmt((uintptr_t)"Doing a[%ull] = %ui\n", n, aAt(a, n-1));
  (void)aAt2(a, n, aAt(a, n-1));
  return -2; }); // a becomes {1, 1, 3, 3}
@endcode **/
int aLoop(any* aArray, size_t startPos, int (*f)(size_t pos));
/**
 * Compares `aArray` against one or more `next_Array`s
 *
 * The `any` type width is assumed to be the same -- i.e. both `char*`, or `short*`, etc
 *
 * @returns `1` if equal, `0` otherwise
 * @throws ;;exception;; "array type too wide"
 *
@code{.c}
int
  *black = NULL,
  *white = NULL;
if(aCmp(black, white)) aFmt("We are all family");
@endcode **/
int aCmp(any*aArray, any* next_aArray, ...);



/**
 * @} @{
 * Orders array items as ascending unsigned integers
 * 
 * @returns The re-ordered array
 * @throws ;;exception;; "array type too wide"
 *
 * Requires `<math.h>` linking for the sqrt function
 * 
@code{.c}
int* filmBoredomLevel = aAppend((int**)NULL, 300, 600, 200, 100);
aSort(filmBoredomLevel);
aFmt("[%v, %ui]", filmBoredomLevel); // prints "[100, 200, 300, 600]"
@endcode **/
any* aSort(any* aArray);
/**
 * Orders array items according to the function `f`
 * 
 * `f` must return true/false, i.e. using `<` or `>`
 * 
 * `f` can be a block, c++11 lambda, or function-pointer
 * 
 * @returns The re-ordered array
 * @throws ;;exception;; "array type too wide"
 *
@code{.c}
int sort_strcmp(char*a, char*b) { return strcmp(a,b) < 0; }
// ...
char** verses = NULL;
aAppend(&verses, "There is no greater love", "You are my beloved", "Because of His great love");
aSortF_FUNC(verses, sort_strcmp);
aFmt("%v\n%s", verses);
@endcode **/
any* aSortF(any* aArray, int(*f)(any*key, any*item));
/**
 * Searches a sorted `aArray` for `key`
 * 
 * If `key` is present, then `index` is set to its position, otherwise `index` is set to the last searched position.  Where `key==2`, and `aArray` is `[2, 2]` or `[1, 3]`:  `index` will be set to either `0` or `1`, since both answers are correct
 * 
 * The `aArray` items must be ordered as ascending unsigned integers
 * 
 * @returns `1` if `key` found, `0` otherwise.  And updates `*index`
 * @throws ;;exception;; "array type too wide"
 *
 * `aSearchP` is an alternative to @aSearch that uses <a class="info_link" href="#how" onclick="a_HOW.onclick(); document.getElementById('pingpong').scrollIntoView(); return false">pingpong</a>, rather than binary search.  It's here for your enjoyment
 *
@code{.c}
size_t index;
if(aSearch(filmBoredomLevel, &index, 600))
  printf("Most boring film 'Peppa Pig Goes on Holiday' is at index %zu", index);
@endcode **/
int aSearch(any* aArray, size_t*index, any key);
/**
 * Searches a sorted `aArray` for `key`
 *
 * If `key` is present, then `index` is set to its position, otherwise `index` is set to the last searched position
 * 
 * `f` can be a block, c++11 lambda, or function-pointer
 * 
 * `f` must return less than, equal, or greater than 0, as `item` is less than, equal, or greater than `key`.  It is common to see examples such as `aSearchF(arr, &i, 5, ^(int a, int b){ return a-b; })`.  But this is incorrect, as the difference `a-b` is an `unsigned int`, and so overflows the sign bit.  Either use an if statment, or cast to a larger `int`, i.e. `^(int a, int b){ return ((int64_t)a - (int64_t)b); }` 
 * 
 * @returns `1` if `key` found, `0` otherwise.  And updates `*index`
 * @throws ;;exception;; "array type too wide"
 *
 * `aSearchPF` is an alternative to `aSearchF` that uses <a class="info_link" href="#how" onclick="a_HOW.onclick(); document.getElementById('pingpong').scrollIntoView(); return false">pingpong</a>, rather than binary search
 * 
 * For `aSearchPF`, `f` should return the degree of difference, i.e. `item - key`.  Note that `strcmp` does this, but it is not a great example as it only returns a byte of difference.  The same sign bit overflow issue applies
 * 
@code{.c}
int64_t search_strcmp(char*a, char*b) { return strcmp(a,b); }
// ...
size_t index;
char* key = "He gives power to the weak and strength to the powerless";
if(aSearchPF_FUNC(verses, &index, key, search_strcmp))
  printf("found verse at %zu", index);

aSearchPF_BLOCK(doors, &index, key, ^(uint64_t a, uint64_t b){
  // avoid overflow into the sign bit
  if((a^b)>(UINT64_MAX>>1)) return a<b? INT64_MIN : INT64_MAX;
  // alternative method
  // if(a-b>(UINT64_MAX>>1)||b-a>(UINT64_MAX>>1)) return a<b? INT64_MIN : INT64_MAX;
  return (int64_t)(a-b); });
if(index < aL(doors) && doors[index] < key) index++;
aReplace(&doors, index, 0, key);
aF("inserted key into door %uz", index);
@endcode **/
int aSearchF(any* aArray, size_t*index, any key, int64_t(*f)(any key, any item));
/**
 * Searches a sorted `aArray` for `key`
 *
 * The underlying algorithm is pingpong, so `f` preferably returns the degree of difference
 *
 * The array is assumed to hold `aLength(aArray)/stride` items.  `f` is passed pointers at `stride` offsets
 * 
 * So if `aArray` is `(uint8_t*)`, then `stride` is measured in bytes
 * 
 * @returns `1` if `key` found, `0` otherwise.  And updates `*index`
 * @throws ;;exception;; "array type too wide"
 * 
@code{.c}
struct person { char*name; };

struct person key = { "myself" };
uint8_t*people = aArray((uint8_t*)malloc(200),200,true); aL2(people, sizeof(struct person));
memcpy(people, &key, sizeof(struct person));

size_t index;
if(aSearchPS_FUNC(people, &index, &key, f, sizeof(struct person)))
  aF("egomania is at %uz", index);
// printed "egomania is at 0"
@endcode **/
int aSearchPS(any* aArray, size_t*index, any*key, int64_t(*f)(any*key, any*item), size_t stride);



/**
 * @} @{
 * Prints aArrays;  aswell as chars, strings, integers, and floats, according to `format`
 *
 * `format` can contain plain text, or:
 * - `%%` for %
 * - `%%c` for characters
 * - `%%s` for strings
 * 
@code{.c}
aFmt("%s%c%s", "plat", 'y', "pus"); // prints 'platypus'
@endcode
 *
 * - `%%AAiBB` for integers
 * - `%%AAuBB` for unsigned integers
 *
 * `AA` is any number from `2` - `64`,  and defaults to `10`.  It is the base to print in, such as binary or hex
 *
 * `BB` is the integer's type, and defaults to (int).  It can be:  `8`, `16`, `32`, `64`, or `c` (char), `s` (short), `i` (int), `l` (long), `ll` (long long), `z` (size_t), `m` (intmax_t), `p` (intptr_t).  So (unsigned long long) in base 16 would be `%16ull`;  (uint32_t) in binary would be `%2u32`;  and (signed short) in base 10 would be `%%is`, though `%%i` will always print the sign, whether `+` or `-`
 *
@code{.c}
aFmt("h%cpp%c %u8", 'i', 'o', (uint8_t)2); // prints 'hippo 2'
@endcode
 *
 * - `%%f` for floats
 * - `%%d` for doubles
 *
@code{.c}
float fa=0.01, fb=2.22;
double da=2.22*1000000;
// prints '0.01 2.22 -- 2.22e+06'
aF("%f %f -- %d",
    *(int32_t*)&fa,*(int32_t*)&fb,
    *(int64_t*)&da); // bitwise float->int conversions
@endcode
 *
 * - `%%vAA%%BB` for aArrays
 *
 * `AA` is the text to print between each item.  `BB` can be any of the `%` formatters, except `%%v`.  So '`%%v, %%i`' would print a comma separated list of ints
 *
 * @returns `0` on success, `-1` on I/O error
 * @throws ;;exception;; "format requires more arguments", "format is malformed", "parameter is NULL"
 *
@code{.c}
char* abc = aAppend((char**)NULL, 'a', 'b', 'c');
aFmt("<%v, %c>", abc); //prints '<a, b, c>'

int* nums = aAppend((int**)NULL, 0, 1, 2);
aFmt("[%v | %2i]", nums); //prints '[+0 | +1 | +10]'
aFree(&abc); aFree(&nums);
@endcode **/
int aFmt(char* format, ...);
/**
 * Prints aArrays, etc in `format` to `stderr`.  First `stdout`, then `stderr` is flushed
 *
 * This can be useful for quick debugging.  Assuming no `#define AARRAY_NOCONVENIENCE` we even save a whole two letters, abbreviating it to `aFE`
 *
 * @returns `0` on success, `-1` on I/O error
 * @throws ;;exception;; "format requires more arguments", "format is malformed", "parameter is NULL"
 *
@code{.c}
// prints base 2, base 3, then base 4
aFmtE("as easy as %2ic %3is %4ii", (char)1, (short)2, (int)3);
@endcode **/
int aFmtE(char* format, ...);
/**
 * Prints aArrays, etc in `format` to `file`
 *
 * @returns `0` on success, `-1` on I/O error
 * @throws ;;exception;; "format requires more arguments", "format is malformed", "parameter is NULL"
 *
@code{.c}
// aStr("") is simply a convenience to convert a string to an aArray
char* word = aStr("imperturbablorbably");
FILE* file = fopen("hope.txt", "w");
aFmtF(file, "Let me spell it out for you: %v, %c", word);
fclose(file); // contains "Let me spell it out for you: i, m, p, e, r, t, u, r, b...."
@endcode **/
int aFmtF(FILE* file, char* format, ...);
/**
 * Prints aArrays, etc in `format` to a `(char*)` aArray --- no '\0' appended
 * 
 * `'X'` is placed where the '\0' would be.  This helps catch errors.  Defining `AARRAY_UNSAFE` turns this feature off
 *
 * @returns `0` on success, `-1` on I/O error
 * @throws ;;exception;; "format requires more arguments", "format is malformed", "parameter is NULL", "out of memory"
 *
@code{.c}
char*out = NULL;
aFmtA(&out, "%ui %ui miss a few %ii %ii", 1, 2, 99, 100);
aA(&out, '\0'); // append \0
@endcode **/
int aFmtA(char** aArray, char* format, ...);
/**
 * Prints the full contents of `aArray` and any subsequent aArrays as `char*` text to `file`
 *
 * Subsequent aArrays should have the same `any` type width as the first
 *
 * It is faster than `printf` and does not require aArrays to be `NULL` terminated
 *
 * @returns `0` on success, `-1` on I/O error
 * @throws ;;exception;; "parameter is NULL"
 *
 * There is also `WriteE`, `aWriteF`, corresponding to `aFmtE` and `aFmtF`
 * 
@code{.c}
aAppendArray(&string,
  SIZE_MAX, "A wonderful story of how",
  SIZE_MAX, ", my saviour died for me");
FILE* file = fopen("hope.txt", "w");
aWrite(string); // writes string to stdout
aWriteE(string); // writes string to stderr
aWriteF(file, string); // writes string to file
@endcode **/
int aWrite(any* aArray, ...);



/**
 * @} @{
 * The error handler.  Prints error details and aborts, but can be replaced with a custom error handler
 *
 * This error handler must be set per translation unit
 *
 * `errorLocation` is where the error occurred in your code, i.e.:  `"file:lineNumber"`
 *
 * `errorMsg` is one of:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 *    `"out of memory (allocating=...)"`,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 *    `"out of capacity (size=. but ...)"`,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 *    `"out of bounds (length=. but ...)"`,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 *    `"removal is out of bounds (length=. but ...)"`,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 *    `"array is NULL (array no=...)"`,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 *    `"parameter is NULL"`,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 *    `"array type too wide (max 8 bytes)"`,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 *    `"wrong arg count (args=. but ...)"`, and<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 *    `"infinite loop (jump=0)"`,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 *    `"format requires more arguments"`,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 *    `"format is malformed"`
 *
 * If `errorMsg` formatting fails then `(...)` will be `(no info)`.   Except for a small easter egg, for when you truly manage to run out of memory
 *
 * Here's a little game.  It shows errors being thrown and caught using longjmp
 *
@code{.c}
#include "aArray.h"
#include <setjmp.h>
#include <time.h>

jmp_buf array_err;
void aError_jmp(char* line, char* msg) {
  // our custom error handler
  printf("Error at %s: %s\n", line, msg);
  // choices are exit or longjmp...
  longjmp(array_err, 1); }

int main() {
  printf("Lets play guess the array length\n");
  // guess too high, and an "out of bounds" exception will lose you the game
  srand(time(NULL));
  char* array = aArray((char*)malloc(100), rand() % 100, true);

  // aError exceptions are caught here
  aError = &aError_jmp;
  if(setjmp(array_err))
    printf("Oops, too far! Goodbye\n"), aFree(&array), exit(0);
  
  for(;;) {
    printf("Guess a length: ");
    size_t guess;
    int input_err = scanf("%zu", &guess);
    if (input_err == 0 || input_err == EOF)
      printf("Oh dear, I didn't understand! Goodbye\n"), aFree(&array), exit(0);
    
    // if guess is out of bounds, aAt will call aError_jmp
    // and aError_jmp behaves like an exception
    // i.e. it longjmps to setjmp(array_err) where the error is handled
    aAt(array, guess);
    
    // good luck with winning
    printf("The array might be longer...\n"); } }
@endcode
 *
 * <div style="margin-top:14px">It produces something like:</div>
@verbatim
cc game.c && ./a.out
Lets play guess the array length
Guess a length: 20
The array might be longer...
Guess a length: 30
The array might be longer...
Guess a length: 40
Error at game.c:35: out of bounds (length=36 but pos=40)
Oops, too far! Goodbye
@endverbatim **/
void aError(char* errorLocation, char* errorMsg);
/** @} **/
