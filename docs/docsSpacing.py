#!/usr/bin/python
# -*- coding: utf-8 -*-
import re

aArray = open("docs.h", 'r')
text = aArray.read()
aArray.close()

# let us turn double spaces into <s> </s> in html -- it looks better
out = open('../build/docs-forDoxygen.h', 'w')
re_doubleSpace = re.compile(r'\ \ ')
text = re_doubleSpace.sub(r';;space;;', text)

# let us turn double spaces or tabs in code sections back into normal spaces ^_^
re_colonSpace  = re.compile(r';;space;;')
re_codeBlock   = re.compile("(@code[^@]*@endcode)")
#print(text)
lastEndCode = 0
while True:
  r = re_codeBlock.search(text, lastEndCode)
  if not r: break
  lastEndCode = r.end()
  list1 = list(text)
  list1[r.start():r.end()] = re_colonSpace.sub("  ", r.group(1))
  text = ''.join(list1)

out.write(text)
out.close()
