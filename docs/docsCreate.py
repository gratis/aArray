####### generate graph
from subprocess import call
outputDir = "../build/"
import os
devnull = open(os.devnull, 'w')
if call(["which", "doxygen"], stdout=devnull) != 0:
  print("--- doxygen not found, please install it ---"); exit(-1);
if call(["which", "gnuplot"], stdout=devnull) != 0:
  print("--- gnuplot not found, please install it ---"); exit(-1);
if call(["which", "cppp"], stdout=devnull) != 0:
  print("--- cppp not found, please install it ---"); exit(-1);

if not os.path.exists(outputDir): call(["mkdir", outputDir])
call(["gnuplot", "-e", 'set term png; set output "'+outputDir+'rough_search.png";     set xlabel "array length"; set ylabel "time"; set key left; unset ytics; set xtics 50 out; set mxtics 10; set xtics nomirror; set format x "%.0f"; set border 0; set linestyle 1 linetype rgb "#f5eb2c" linewidth 8; set linestyle 2 linetype rgb "#f52ca5" linewidth 8; set linestyle 3 linetype rgb "#ff6edd" linewidth 8; set linestyle 4 linetype rgb "#7d2cf5" linewidth 8;'+
      'plot "plot_search.txt" using 1:7 smooth cspline linestyle 1 notitle "realloc;;;;",'+
      '"plot_search.txt" using 1:2 smooth cspline linestyle 3 notitle "NOCAPA;;;CITY",'+
      '"plot_search.txt" using 1:4 smooth cspline linestyle 2 notitle "aArray",'+
      '"plot_search.txt" using 1:5 smooth cspline linestyle 4 notitle "std::vector",'+
      'sqrt(-1) linestyle 4 title "std::vector",'+
      'sqrt(-1) linestyle 3 title "NOCAPACITY",'+
      'sqrt(-1) linestyle 2 title "aArray",'+
      'sqrt(-1) linestyle 1 title "realloc"'])
# commented out, to protect hand done version
# call(["convert", outputDir+"rough_search.png", "-scale", "100%", "-mattecolor", "#e2e8f2", "-frame", "4x4+1+1", outputDir+"graph_search.png"])
# call(["convert", outputDir+"rough_sort.png", "-scale", "100%", "-mattecolor", "#e2e8f2", "-frame", "4x4+1+1", outputDir+"graph_sort.png"])


####### generate docs
from bs4 import BeautifulSoup
import re

def addSpace(text):
    # fix doxygen bug with unicode-->htmlentity
    re_fixBug = re.compile(r'&amp;nbsp;')
    text = re_fixBug.sub("\xc2", text)
    # convert space
    re_space = re.compile(r';;space;;')
    return re_space.sub(r'<s> </s>', text)

def removeSpace(text):
    re_space = re.compile(r';;space;;')
    return re_space.sub(r'  ', text)

# io.open to be cross python 2/3 compatible
import io
fileIntro = io.open(outputDir+"html/index.html", 'r', encoding="utf-8")
fileGenerated = open(outputDir+"aArray-forTests.h", 'r')
releasedate = re.search(r'Generated: ([^\\n]+)', fileGenerated.read()).group(1)
fileGenerated.close()
intro = re.sub(r'\$release\$', releasedate.strip(), fileIntro.read())
fileIntro.close()
intro = BeautifulSoup(addSpace(intro), "html.parser")

fileApi = open(outputDir+"html/docs-for_doxygen_8h.html", 'r')
api = BeautifulSoup(addSpace(fileApi.read()), "html.parser")
fileApi.close()

fileSource = open(outputDir+"html/a_array-source_8h_source.html", 'r')
# source = BeautifulSoup(fileSource.read(), "html.parser")
source = fileSource.read()
fileSource.close()
source_start = source.find("<div class=\"fragment\">")
source_end = source.find("<!-- fragment --></div>")
source = source[source_start:source_end]

aArrayDocsPath = outputDir+"aArray.html"
out = open(aArrayDocsPath, 'w')

# remove incorrect file-part from anchors
def removeAnchor(a):
    re_fromHash = re.compile(r'[^#]*#(.*)')
    re_webPage = re.compile(r'docs-for_doxygen_8h.html#[a-z0-9]*$')
    if(a.has_attr('href') and re_webPage.match(a['href'])):
        a['onclick'] = 'openApi(event, \"'+re_fromHash.sub(r'\1', a['href'])+'\")'
        a['href'] = '#'+a.text.strip('\t\r\n')
for a in intro.select('a'): removeAnchor(a)
for a in api.select('a'): removeAnchor(a)

###    if(a.has_attr('href')): a['href'] = re_fromHash.sub(r'\1', a['href'])

# do the same for 'source' -- which didn't go down well in BeautifulSoup
source = re.sub(r'<a( class="code"|) href="docs-for_doxygen_8h.html#([a-z0-9]*)">([^<]*)',
                '<a\\1 onclick="openApi(event, \'\\2\')" href="#\\3">\\3', source)

out.write("""<!doctype html>
<html><head><title>
aArray
</title>
<link rel="icon" href="https://gratis.gitlab.io/favicon-96.ico" sizes="96x96">
<link rel="icon" href="https://gratis.gitlab.io/favicon-16.ico" sizes="16x16">
<link rel="icon" href="https://gratis.gitlab.io/favicon-32.ico" sizes="32x32">
<meta http-equiv="Content-Type" charset="utf-8"/>
<meta name="viewport" content="width=675"/>
<style>
html body h1 { display:none; /* doxygen version changes makes life difficult */ }
h1, h2, h3, h4, h5, h6 {
        margin-bottom:-1.5ex;
	-webkit-transition: text-shadow 0.5s linear;
	-moz-transition: text-shadow 0.5s linear;
	-ms-transition: text-shadow 0.5s linear;
	-o-transition: text-shadow 0.5s linear;
	transition: text-shadow 0.5s linear; margin-right: 15px; }
dt { font-weight: bold; }
a { color: #3d578c; font-weight: normal; text-decoration: none; }
a:hover { text-decoration: underline; }
a.el { font-weight: bold; }
a.code, a.code:visited, a.line, a.line:visited { color: #4665a2; }
pre.fragment {
  border: 1px solid #c4cfe5;
  background-color: #fbfcfd;
  padding: 4px 6px;
  margin: 14px 2px 2px 2px;
  line-height: 125%;
  font-family: monospace, fixed;
	overflow:auto;
	white-space: -moz-pre-wrap;
	white-space: -pre-wrap;
	white-space: -o-pre-wrap;
	white-space: pre-wrap;
	word-wrap: break-word; }
div.fragment {
	word-break:break-word;
  padding: 4px 6px;
  margin: 14px 2px 2px 2px;
	background-color: #fbfcfd;
	border: 1px solid #c4cfe5; }
div.line { font-family: monospace, fixed; font-size: 13px;
	min-height: 13px;
	line-height: 1.0;
	text-wrap: unrestricted;
	white-space: -moz-pre-wrap;
	white-space: -pre-wrap;
	white-space: -o-pre-wrap;
	white-space: pre-wrap;
	word-wrap: break-word;
	text-indent: -4ex;
	padding-left: 4ex;
	padding-bottom: 0px;
	margin: 0px;
	-webkit-transition-property: background-color, box-shadow;
	-webkit-transition-duration: 0.5s;
	-moz-transition-property: background-color, box-shadow;
	-moz-transition-duration: 0.5s;
	-ms-transition-property: background-color, box-shadow;
	-ms-transition-duration: 0.5s;
	-o-transition-property: background-color, box-shadow;
	-o-transition-duration: 0.5s;
	transition-property: background-color, box-shadow; transition-duration: 0.5s; }
span.lineno {
	padding-right: 4px;
	text-align: right;
	border-right: 2px solid #0F0;
	background-color: #e8e8e8; white-space: pre; }
div.groupHeader {
	margin-left: 16px;
	margin-top: 12px; font-weight: bold; }
span.keyword { color: #008000; }
span.keywordtype { color: #604020; }
span.keywordflow { color: #e08000; }
span.comment { color: #800000; }
span.preprocessor { color: #806020; }
span.stringliteral { color: #002080; }
span.charliteral { color: #008080; }
table.memberdecls {
	border-spacing: 0px; padding: 0px; }
.memberdecls td, .fieldtable tr {
	-webkit-transition-property: background-color, box-shadow;
	-webkit-transition-duration: 0.5s;
	-moz-transition-property: background-color, box-shadow;
	-moz-transition-duration: 0.5s;
	-ms-transition-property: background-color, box-shadow;
	-ms-transition-duration: 0.5s;
	-o-transition-property: background-color, box-shadow;
	-o-transition-duration: 0.5s;
	transition-property: background-color, box-shadow; transition-duration: 0.5s; }
.memItemLeft, .memItemRight,
.memTemplItemLeft, .memTemplItemRight {
	background-color: #f9fafc;
	border: none;
	margin: 4px; padding: 1px 0 0 8px; }
.memSeparator {	border-bottom: 1px solid #ebeff6; line-height: 0px; margin: 0px; padding: 0px; }
html .memSeparatorFirst { padding:5px; }
.memItemLeft, .memTemplItemLeft {
	white-space: nowrap;
border-left: 1px solid #ebeff6; }
.memItemRight { width: 100%; }
.memitem {
	padding: 0;
	margin-bottom: 10px;
	margin-right: 5px;
        -webkit-transition: box-shadow 0.5s linear;
        -moz-transition: box-shadow 0.5s linear;
        -ms-transition: box-shadow 0.5s linear;
        -o-transition: box-shadow 0.5s linear;
        transition: box-shadow 0.5s linear;
        display: table !important; width: 100%; }
.memname {
        font-weight: bold; margin-left: 6px; }
.memname td { vertical-align: bottom; }
.memproto {
        border-top: 1px solid #a8b8d9;
        border-left: 1px solid #a8b8d9;
        border-right: 1px solid #a8b8d9;
        padding: 6px 0px 6px 0px;
        color: #253555;
        font-weight: bold;
        text-shadow: 0px 1px 1px rgba(255, 255, 255, 0.9);
        background-repeat:repeat-x;
        background-color: #e2e8f2;
        box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.15);
        border-top-right-radius: 4px;
        border-top-left-radius: 4px;
        -moz-box-shadow: rgba(0, 0, 0, 0.15) 5px 5px 5px;
        -moz-border-radius-topright: 4px;
        -moz-border-radius-topleft: 4px;
        -webkit-box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.15);
        -webkit-border-top-right-radius: 4px;
        -webkit-border-top-left-radius: 4px; }
.memdoc {
        border-bottom: 1px solid #a8b8d9;
        border-left: 1px solid #a8b8d9;
        border-right: 1px solid #a8b8d9;
        padding: 6px 10px 2px 10px;
        background-color: #fbfcfd;
        border-top-width: 0;
        background-repeat:repeat-x;
        background-color: #ffffff;
        border-bottom-left-radius: 4px;
        border-bottom-right-radius: 4px;
        box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.15);
        -moz-border-radius-bottomleft: 4px;
        -moz-border-radius-bottomright: 4px;
        -moz-box-shadow: rgba(0, 0, 0, 0.15) 5px 5px 5px;
        -webkit-border-bottom-left-radius: 4px;
        -webkit-border-bottom-right-radius: 4px;
        -webkit-box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.15); }
.paramkey { text-align: right; }
.paramtype { white-space: nowrap; }
.paramname { color: #602020; white-space: nowrap; }
.paramname em { font-style: normal; }
.paramname code { line-height: 14px; }
.params, .retval, .exception, .tparams {
        margin-left: 0px; padding-left: 0px; }       
.params .paramname, .retval .paramname {
        font-weight: bold; vertical-align: top; }
.params .paramtype {
        font-style: italic; vertical-align: top; }       
.params .paramdir {
        font-family: "courier new",courier,monospace; vertical-align: top; }
dl { padding: 0 0 0 10px; }
dl.section { margin-left:2px;
	padding-left:0px; margin-bottom:4px; }
dl.note { padding-left:3px; border-left:4px solid; border-color:#d0c000; }
dl.warning, dl.attention { padding-left:3px;
        border-left:4px solid; border-color:#ff0000; }
dl.pre, dl.post, dl.invariant { margin-left:-7px; padding-left:3px; border-left:4px solid; border-color:#00d000; }
dl.deprecated { margin-left:-7px; padding-left:3px;
        border-left:4px solid; border-color:#505050; }
dl.todo { margin-left:-7px; padding-left:3px;
        border-left:4px solid; border-color:#00c0e0; }
dl.test { margin-left:-7px; padding-left:3px;
        border-left:4px solid; border-color:#3030e0; }
dl.bug { margin-left:-7px; padding-left:3px;
        border-left:4px solid; border-color: #c08050; }
dd { margin:0px 0px 0px 3ex; }






body { font:400 14px/22px Roboto,sans-serif; min-width:517px;
	word-break:break-word;
	background-color: white;
	color: black; }

html, body { margin:0px; padding:0px; }

.memdoc { padding-bottom:10px }
span.lineno {
        background-color: #fbfcfd;
        border-right:2px solid #ebeff6 }
#tabs2 div.sourceFragment {
        margin-bottom:21px; padding:0; background-color:#fffffc;
        border:0; border-right:2px solid #fdfddd; font-size:80% }
.ttc { display:none }

#tabs2 { max-width:1060px; text-align:left; margin:21px 14px 7px 14px; }
#tabs1 div.tab { position:relative; left:0px; }
#tabs2 div.tab { }
.tabs div.tab_hidden { display:none }
.tabLinks a { text-decoration:none; font-size:130%; font-style:italic; margin:0px 8px; }
.tabLinks a.tab_hidden { text-decoration:underline; }

#info { background-color:#e2e8f2; 
        border-bottom:1px solid #a8b8d9; }
#info2 { max-width:1060px; margin:0 auto; padding:20px 20px 10px 20px; }
.info_link { text-decoration:underline }

#api_details { table-layout:fixed;width:100%;border-spacing:0;border-collapse:collapse; }
#api_details td { padding:0px; }

h3 { margin:4ex 0 -1.5ex 0; padding:0; color:#97c }
h4 { margin:0 0 -1.5ex 0; display:inline }
s, u { text-decoration:none; letter-spacing:0.5em }
u { letter-spacing:0.25em }
#info .tab { font-size:110% }
.quote { color:#888; font-size:110%; margin-left:48px }
code { background-color:#fbfcfd; padding:1px 2px }
#tabLinks1 { display:inline }
#tabLinks2 { text-align:right }
img { display:inline }
.image { display:inline }

</style></head>
<body><div id="info"><div id="info2">
<div class="tabLinks" id="tabLinks1"><span style="font-size:280%;font-style:italic;font-weight:bold">aArray</span> <a>overview</a> / <a>how</a> / <a>details</a> / <a>THANKS</a></div>
<div class="tabs" id="tabs1">
	<div class="tab">""")

# populate tabs with each h2 section from aArray.h
headerNo = 0
from bs4 import Comment
for child in intro.select('.textblock')[0].children:
    oldHeaderNo = headerNo
    if(child.name=='h2'): headerNo += 1
    elif(not isinstance(child, Comment)): out.write(str(child))
    if(headerNo!=oldHeaderNo and headerNo!=1):
        out.write("</div><div class=\"tab tab_hidden\">")

out.write("""</div></div>
<div class=\"tabLinks\" id="tabLinks2" style="margin-top:14px"><a>api</a> / <a>code</a></div>

</div></div>
<script>
'use strict';
var a_api, a_details, a_HOW;
var tabLinks = [], hashchange = false;
function openApi(e, id) {
  e && (e.preventDefault? e.preventDefault() : e.returnValue = false);
  a_api.onclick();
  document.getElementById(id).scrollIntoView(); }
function tabsInit(tabLinksElement, tabsElement) {
	var tabs = {'ts':[], 'tls':[]};
	var ts = tabsElement.childNodes; // all tabs
	var tls = tabLinksElement.childNodes; // all tab links
	// get all tab contents
	var n = -1; while(++n < ts.length)
		if(ts[n].nodeName && ts[n].className) tabs.ts[tabs.ts.length] = ts[n];
	// get and setup tab links
	function searchName(tabName) {
		return location.hash.search('(^[#]?|[|])'+tabName+'($|[|])')+1; }
	n = -1; while(++n < tls.length)
		if(tls[n].nodeName && tls[n].nodeName.toLowerCase()=='a') {
      if(tls[n].innerHTML=='api')          a_api  = tls[n];
      else if(tls[n].innerHTML=='details') a_details = tls[n];
      else if(tls[n].innerHTML=='how')     a_HOW = tls[n];
			tabs.tls[tabs.tls.length] = tls[n];
			if(tabs.tls.length!=1) tls[n].className = 'tab_hidden';
			tls[n].href = '#'+tls[n].innerHTML;
			tls[n].onclick = function(tName, tNum) { return function(e) {
				e && (e.preventDefault? e.preventDefault() : e.returnValue = false);
				var currIndex;
				n = -1; while(++n < tabs.ts.length) {
					currIndex = currIndex || searchName(tabs.tls[n].innerHTML);
					tabs.ts[n].className = n==tNum? 'tab' : 'tab tab_hidden';
					tabs.tls[n].className = n==tNum? '' : 'tab_hidden'; }
				if(!hashchange) {
					var hash = location.hash;
					var currEnd = hash.indexOf('|',currIndex);
					location.hash = !currIndex? hash+(hash==''||hash=='#'?'':'|')+tName :
					hash.substr(0,currIndex)+tName+
						hash.substr(currEnd!=-1?currEnd:hash.length); } }
				}(tls[n].innerHTML, tabs.tls.length-1); }
	// reflect location.hash on load and back/forward
	tabLinks[tabLinks.length] = tls;
	window.onload = window.onhashchange = function() {
		hashchange = true; // don't re-update hash, if we're responding to it's change!
		var o = -1; while(++o < tabLinks.length) {
			tls = tabLinks[o];
			var defaultTab = false;
			var n = -1; while(++n < tls.length)
				if(tls[n].nodeName && tls[n].nodeName.toLowerCase()=='a') {
					if(!defaultTab) { defaultTab = true; tls[n].onclick(); }
					if(searchName(tls[n].innerHTML)) { tls[n].onclick(); break; } } }
		hashchange = false; } }
tabsInit(document.getElementById("tabLinks1"),
				 document.getElementById("tabs1"));</script>""")

for h2 in api.select('h2.groupheader'): h2.extract()

n = 0
for sep in api.select('table.memberdecls td[colspan="2"]'):
    if(not sep.has_attr('class')):
        if(n is 0):
            sep['class'] = 'memSeparator memSeparatorFirst'
            #print('first...')
        else: sep['class'] = 'memSeparator'
    n += 1

#print(api.select('.contents')[0].contents)
refs = str(api.select('.contents')[0].table.extract())
out.write("<center><div class=\"tabs\" id=\"tabs2\"><div class=\"tab\">"+
          "<table id=\"api_details\"><tr><td style=\"vertical-align:top;padding-right:1px;width:18em\">" + refs + "</td><td>")

#re_excepStart = re.compile(r';;exception;;(.*)')
re_excepText = re.compile(r'("[^"]*")')
re_excepTextSpace = re.compile(r'("[^" ]+) ([^" ]+)')
for item in api.select('.contents')[0].contents:
    # wrap exceptions in <code>. Doxygen can't do this
    if(item.name=='div'):
        for table in item.find_all('table'):
            if(table['class'][0]=='exception'):
                table.tr.find('td').extract() # remove first <td>
                tds = table.tr.contents
                code = tds[0]
                n = 0
                while n < 4:
                    n += 1
                    tds[0].replace_with(BeautifulSoup(re.sub(re_excepTextSpace,
                                                             '\\1&nbsp;\\2',
                                                             tds[0].prettify()),
                                                      "html.parser"))
                tds[0].replace_with(BeautifulSoup(re.sub(re_excepText,
                                                         '<code>\\1</code>',
                                                         tds[0].prettify()),
                                                  "html.parser"))
    # print api details and their anchors
    if(item.name=='div' or item.name=='a'): out.write(str(item))

out.write("</td></tr></table></div><div class=\"tab tab_hidden\">")

#out.write("code...")
out.write("<div class=\"fragment sourceFragment\">")
#for item in source.select('.fragment')[0].contents:
#    out.write(str(item))
out.write(source)
out.write("</div>")

out.write("</div></div></center><script>" +
          "tabsInit(document.getElementById(\"tabLinks2\"), document.getElementById(\"tabs2\"));"
          "</script></body></html>")
out.close()

print('created documentation at ' + aArrayDocsPath)
