Public domain.  No restriction, no need to accredit

> *As freely as you have received, freely give*  —  Jesus


## About
Arrays and strings are simple things.  Made simpler in c
- By being __safe:__  buffer overflow and other errors give clear messages
- __generic:__  arrays can have any datatype
- __fast:__  optionally uses less memory too
- and __simple:__  as first priority


## Quick example
```c
char*arr = NULL;

aAppend(&arr, 'I', ' ', 'w', 'a', 'n', 't');
aMap(arr, capitalize_char); // capitializes each char

arr[3] = 'O';        // standard assignment
aAt2(arr, 5, ' ');   // same as arr[5] = ' ', but overflow safe

// Append a null terminated array and 5 items from a second array
aAppendArray(&arr, SIZE_MAX, "chunky ", 5, "bacon, francis");

// At position 0 replace 1 item with a null terminated array
aReplaceArray(&arr, 0, 1, SIZE_MAX, "Everyone");

aWrite(arr); // prints "Everyone WON chunky bacon"
aFree(&arr);
```

The `char*` could equally be `int*` or any other datatype

See: [api + documentation](http://tse.gratis/aArray)


## INSTALL
Simply download [aArray.h](http://tse.gratis/aArray/aArray.h)

Or if you prefer to generate and test `aArray.h` yourself;  you will need to install `ninja`, [cppp](//gitlab.com/gratis/cppp), `doxygen`, `python`, and `gnuplot`

Then ‘simply’ run `ninja` from within the aArray directory
