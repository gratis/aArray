#define UNIQUE_NAME "extra"
#include "unittest.h"
#include "assert.h"

static jmp_buf expected_err;
__attribute((noreturn)) void aError_jmp(char*line, char*msg) {
  (void)line;
  aFA(&out, "Error at _: %s", msg);//aF("@@@");abort();
  longjmp(expected_err, 1); }

char**test1(void) {
  char*a = aAppend((char**)NULL, 'a', 'b', 'c');
   int*b = aAppend((int**)NULL, 1, 2, 3, 4);
  aError = &aError_jmp;
  if(setjmp(expected_err)) {
    aAppendArray(&out, SIZE_MAX, "  ");
    if(setjmp(expected_err)) {
      aAppendArray(&out, SIZE_MAX, "  ");
      if(setjmp(expected_err)) {
        aAppendArray(&out, SIZE_MAX, "  ");
        if(setjmp(expected_err)) {
          aAppendArray(&out, SIZE_MAX, "  ");
          if(setjmp(expected_err)) {
            aAppendArray(&out, SIZE_MAX, "  ");
            if(setjmp(expected_err)) {
              aAppendArray(&out, SIZE_MAX, "  ");
              if(setjmp(expected_err)) {
                aAppendArray(&out, SIZE_MAX, "  ");
                aFA(&out, "", 1);
                aFA(&out, "%v %c, %v %i", a, b);
                aFree((uintptr_t**)&a); aFree((uintptr_t**)&b);
                aError = &AARRAY_aError;
                return aAppend((char***)NULL, aStr("test1 aFmt"), aStr("Error at _: format is malformed  "
                                                                     "Error at _: format is malformed  "
                                                                     "Error at _: format is malformed  "
                                                                     "Error at _: format is malformed  "
                                                                     "Error at _: format is malformed  "
                                                                     "Error at _: format is malformed  "
                                                                     "Error at _: format is malformed  "
                                                                     "a b c, +1 +2 +3 +4")); }
              aFA(&out, "%", 1);
              return NULL; }
            aFA(&out, "%vabc", a);
            return NULL; }
          aFA(&out, "%v", a);
          return NULL; }
        aFA(&out, "%v%x", 1);
        return NULL; }
      aFA(&out, "%65i", 1);
      return NULL; }
    aFA(&out, "%1u", 1);
    return NULL; }
  aFA(&out, "%x", 1);  
  return NULL; }


char**test2(void) {
  aError = &aError_jmp;
  signed long*a = aAppend((signed long**)NULL, 1, 2);

  if(!setjmp(expected_err)) aFA(&out, "%vsep", NULL);
  if(!setjmp(expected_err)) aFA(&out, "%vsep2%v", NULL, NULL);
  if(!setjmp(expected_err)) aFA(&out, "%v%v", NULL);

  aFA(&out, "-%vsep3%ui- ", NULL);
  aFA(&out, "%vsep4%ul ", a);

  if(!setjmp(expected_err)) aFA(&out, "%vsep", a);
  if(!setjmp(expected_err)) aFA(&out, "%vsep3%v", a, a);
  if(!setjmp(expected_err)) aFA(&out, "%v%v", a);
  if(!setjmp(expected_err)) aFA(&out, "%", NULL);
  if(!setjmp(expected_err)) aFA(&out, "-1-%", NULL);
  if(!setjmp(expected_err)) aFA(&out, "-2-%%", NULL);
  if(!setjmp(expected_err)) aFA(&out, "-3-%%%", NULL);
  if(!setjmp(expected_err)) aFA(&out, "-4-%%%%", NULL);

  aFA(&out, "%%", NULL);


  aFree(&a);
  aError = &AARRAY_aError;
  return aAppend((char***)NULL, aStr("test2 aFmt"), aStr("Error at _: format is malformed"
                                                       "Error at _: format is malformed"
                                                       "Error at _: format is malformed"
                                                       "-- "
                                                       "1sep42 "
                                                       "Error at _: format is malformed"
                                                       "Error at _: format is malformed"
                                                       "Error at _: format is malformed"

                                                       "Error at _: format is malformed"
                                                       "Error at _: format is malformed"
                                                       "-2-%"
                                                       "-3-%Error at _: format is malformed"
                                                       "-4-%%%")); }


char**test3(void) {
  unsigned int*a = aAppend((unsigned int**)NULL, 1, 2);
  aConcat(&a, a, a, a, a);
  aFA(&out, "%v%ui ", a);
  aFree(&a);

  signed long*b = NULL;
  aConcat(&b, b, b);
  aFA(&out, "%v %il", b);
  aFree(&b);
  return aAppend((char***)NULL, aStr("test3 aConcat"), aStr("1212121212 ")); }


char**test4(void) {
  char*a = aAppendArray((char**)NULL, SIZE_MAX, "1234567890abcdefghij");

  size_t result;
  result = aIndexOf(a, 'a'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aIndexOf(a, 'b'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aIndexOf(a, 'c'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aIndexOf(a, 'd'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aIndexOf(a, 'e'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aIndexOf(a, 'f'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aIndexOf(a, 'g'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aIndexOf(a, 'h'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aIndexOf(a, 'i'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aIndexOf(a, 'j'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);

  aFA(&out, "\n");  
  result = aZIndexOf(a, 'a'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aZIndexOf(a, 'b'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aZIndexOf(a, 'c'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aZIndexOf(a, 'd'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aZIndexOf(a, 'e'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aZIndexOf(a, 'f'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aZIndexOf(a, 'g'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aZIndexOf(a, 'h'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aZIndexOf(a, 'i'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);
  result = aZIndexOf(a, 'j'); if(result!=(size_t)-1) aFA(&out, "%uz ", result);

  aFree(&a);
  return aAppend((char***)NULL, aStr("test4 aIndexOf indexing"),
                 aStr("10 11 12 13 14 15 16 17 18 19 \n"
                     "10 11 12 13 14 15 16 17 18 19 ")); }


static int16_t*z5 = NULL;
void cfMap2(int16_t*aa) { *aa = (*aa)+1; }
void cfMap(int16_t*aa) { aFA(&out, "%u16", *aa); }
void cfMapPtr(int16_t*aa) { *aa += 1; aFA(&out, "%u16", *aa); }
int cfLoop(size_t pos, void*data) { (void)data; aFA(&out, "%u16", aAt(z5, pos)); return 1; }
char**test5(void) {
  aAppend(&z5, 1, 2, 3, 4, 5);
  aMap_FUNC(z5, cfMap2, NULL);
  aMap_FUNC(z5, cfMap, NULL);    aFA(&out, " ");
  aMap_FUNC(z5, cfMapPtr, NULL); aFA(&out, " ");
  aLoop_FUNC(z5, 1, cfLoop, NULL);

  aError = &aError_jmp;
  if(!setjmp(expected_err)) aMap_FUNC(z5, NULL, NULL);
  if(!setjmp(expected_err)) aMap_FUNC(z5, NULL, NULL);
  if(!setjmp(expected_err)) aMap_FUNC(z5, NULL, NULL);
  if(!setjmp(expected_err)) aLoop_FUNC(z5, 1, NULL, NULL);

  #define pIsNull \
    "Error at _: parameter is NULL" "Error at _: parameter is NULL" \
    "Error at _: parameter is NULL" "Error at _: parameter is NULL"

  #if __has_extension(blocks)
  aFA(&out, "  ");
  aMap_BLOCK(z5, ^(int16_t*aa){ *aa = (*aa)+1; });
  aMap_BLOCK(z5, ^(int16_t*aa){ aFA(&out, (intptr_t)"%u16", (uint64_t)*aa); });           aFA(&out, " ");
  aMap_BLOCK(z5, ^(int16_t*aa){ *aa += 1; aFA(&out, (intptr_t)"%u16", (uint64_t)*aa); }); aFA(&out, " ");
  aLoop_BLOCK(z5, 1, ^(size_t pos){ aFA(&out, (intptr_t)"%u16", (uint16_t)aAt(z5, pos)); return 1; });

  if(!setjmp(expected_err)) aMap_BLOCK(z5, NULL);
  if(!setjmp(expected_err)) aMap_BLOCK(z5, NULL);
  if(!setjmp(expected_err)) aMap_BLOCK(z5, NULL);
  if(!setjmp(expected_err)) aLoop_BLOCK(z5, 1, NULL);

  char result[] = "23456 34567 4567" pIsNull
    "  " "45678 56789 6789" pIsNull;
  #else
  char result[] = "23456 34567 4567" pIsNull;
  #endif

  aFree(&z5);
  aError = &AARRAY_aError;
  return aAppend((char***)NULL, aStr("test5 passing functions"),
                 aStr(result)); }


char**test6(void) {
  aError = &aError_jmp;
  int*a = NULL;
  aAppend(&a, 10);
  //aWriteF(outFILE, a);
  //// not currently detecting these NULLs
  if(!setjmp(expected_err)) aFA(&out, NULL);
  if(!setjmp(expected_err)) aFA(NULL, "hi");
  if(!setjmp(expected_err)) aFF(outFILE, NULL);
  if(!setjmp(expected_err)) aFF(NULL, "hi");
  if(!setjmp(expected_err)) aWriteF(NULL, a);
  aFree(&a);
	uint8_t*mem = malloc(2);
	if(!setjmp(expected_err)) mem = aArray((uint8_t*)NULL, 2, true);
	if(!setjmp(expected_err)) aArray_NOCAPACITY((uint8_t*)mem, 2, true);
	free(mem);

  aError = &AARRAY_aError;
  return aAppend((char***)NULL, aStr("test6 NULL"),
                 aStr("Error at _: parameter is NULL"
                     "Error at _: parameter is NULL"
                     "Error at _: parameter is NULL"
                     "Error at _: parameter is NULL"
                     "Error at _: parameter is NULL"
                     "Error at _: array is NULL (array no=0)"
                     "Error at _: out of capacity (size=2 but require=8)")); }


static int *test7a;
int incrOne(size_t n, void*data)   { (void)n; (void)data; return 1; }
int incrOneIf(size_t n, void*data) { (void)data; if(n==3) return 0; return 1; }
int incrIfTen(size_t n, void*data) { (void)data;
  if(test7a[n]+10 == test7a[n+1]) return 1;
  aFmtA(&out, "%ii ", test7a[n+1]); return 0; }
char**test7(void) {
  test7a = NULL;
  aAppend(&test7a, 10, 20, 30, 40, 50, 5);
  aFmtA(&out, "%ii %ii ", aLoop_FUNC(test7a, 0, incrOne, NULL), aLoop_FUNC(test7a, 0, incrOneIf, NULL));
  aLoop_FUNC(test7a, 0, incrIfTen, NULL);

  aFree(&test7a);

  return aAppend((char***)NULL, aStr("test7"),
                 aStr("+1 +0 +5 ")); }


void setZero(int*aa) { *aa = 0; }
char**test8(void) {
  aError = &aError_jmp;
  int*volatile a = NULL;
  int*volatile b = NULL; aAppend((int**)(uintptr_t)&b, 1); 
  if(!setjmp(expected_err)) aMulti((int**)(uintptr_t)&a, 1, 0, 0, 0, NULL);
  if(!setjmp(expected_err)) aMulti((int**)(uintptr_t)&a, 0, 1, 0, 0, NULL);
  if(!setjmp(expected_err)) aMulti((int**)(uintptr_t)&a, 1, 1, 0, 0, NULL);
  if(!setjmp(expected_err)) aMulti((int**)(uintptr_t)&a, 0, 0, 1, 1, NULL); // length of NULL is not 1

  aMulti((int**)(uintptr_t)&a, 0, 0, 0, 1, NULL);
  if(aL(a)!=1) aFA(&out, "false0");
  aMulti((int**)(uintptr_t)&a, 0, 0, 0, 1, b);
  if(aL(a)!=2) aFA(&out, "false1");
  aMulti((int**)(uintptr_t)&a, 0, 0, 0, 0, NULL); // do nothing
  if(aL(a)!=2) aFA(&out, "false2");


  // giving length of zero with a non null...  Ignore, does same as if b is NULL
  aMulti((int**)(uintptr_t)&a, 0, 0, 1, 0, b);
  // make the memory sanitizer happy -- aMulti has created unitialized data
  aMap_FUNC(a, setZero, NULL); if(aAt(a,0) == a[0]) aFA(&out, "true");

  aFree((int**)(uintptr_t)&a);
  aMulti((int**)(uintptr_t)&a, 0, 0, 0, 1, NULL);
  aMap_FUNC(a, setZero, NULL); if(a[0] == aAt(a,0)) aFA(&out, "true");
  aMulti((int**)(uintptr_t)&a, 0, 0, 1, 1, b);
  if(aAt(a,0) == 1) aFA(&out, "true");

  aMulti((int**)(uintptr_t)&a, 1, 1, 1, 1, (int[]){2}); if(aAt(a,1) == 2 && aL(a) == 2) aFA(&out, "true");

  if(!setjmp(expected_err)) aMulti((int**)(uintptr_t)&a, 0, 3, 1, 1, (int[]){2});
  aMulti((int**)(uintptr_t)&a, 0, 2, 2, 1, (int[]){3}); if(aAt(a,0) == 3 && aL(a) == 2) aFA(&out, "true");

  if(!setjmp(expected_err)) aMulti((int**)(uintptr_t)&a, 0, 0, 1, 0, NULL,
                                       0, 0, 0, 1, NULL,
                                       0, 0, 1, 1, NULL);

  aFree((int**)(uintptr_t)&a); aFree((int**)(uintptr_t)&b);
  aError = &AARRAY_aError;
  return aAppend((char***)NULL, aStr("test8 aMulti"),
                 aStr("Error at _: out of bounds (length=0 but pos=1)"
                      "Error at _: removal is out of bounds (length=0 but pos=0 removal=1)"
                      "Error at _: out of bounds (length=0 but pos=1)"
                      "Error at _: array is NULL (array no=1)"
                      "truetruetruetrue"
                      "Error at _: removal is out of bounds (length=2 but pos=0 removal=3)"
                      "true"
                      "Error at _: array is NULL (array no=3)")); }


char**test9(void) {
  int*a = NULL;
  int b = 0;
  aAppend(&a, b++); if(b != 1) aFA(&out, "false1"); b = 0;
  aAppend(&a, 0, b++); if(b != 1) aFA(&out, "false1"); b = 0;
  if(aL(a) != 3) aFA(&out, "false1");

  aFree(&a);
  int c = 0;
  aReplace(&a, (size_t)b++,0, c++); if(b != 1 || c != 1) aFA(&out, "false1");
  if(b != 1 || c != 1) aFA(&out, "false1"); b = 0; c = 0;
  if(aL(a) != 1 && aAt(a,0) != 0) aFA(&out, "false1");

  if(aAt(a, (size_t)b++) != c++) aFA(&out, "false1");
  if(b != 1 || c != 1) aFA(&out, "false1"); b = 0; c = 0;
  if(*aAtPtr(a, (size_t)b++) != c++) aFA(&out, "false1");
  if(b != 1 || c != 1) aFA(&out, "false1"); b = 0; c = 0;
  if(aAt2(a, (size_t)b++, c++ + 1) != 1) aFA(&out, "false1");
  if(b != 1 || c != 1) aFA(&out, "false1"); b = 0; c = 0;
  if(aL2(a, (size_t)b++) != c++) aFA(&out, "false1");
  if(b != 1 || c != 1) aFA(&out, "false1");
  if(aL(a) != 0) aFA(&out, "false1");

  b = 0; c = 5; int d = 1, e = 4;
  aAppend(&a, 1);
  aMulti(&a, 0, (size_t)++b, c++, d++, (int[]){e++});
  if(aL(a) != 5 || aAt(a, 0) != 4 || aAt(a, 4) != 4 ||
     b != 1 || c != 6 || d != 2 || e != 5) aFA(&out, "false2");

  b = 0; c = 0;
  aFA(&out, "%ii %ii ", b++, ++c);

  aFA(&out, "?");
  aFree(&a);
  return aAppend((char***)NULL, aStr("test9 side-effects"), aStr("+0 +1 ?")); }


struct Test { char ch; };
char**test10(void) {
  aError = &aError_jmp;
  char*a = NULL; char**volatile aa = &a;
  if(!setjmp(expected_err)) aFA(aa, "%s", NULL);
  aFree(aa);
  char **b = NULL;
  aIndexOf(b, NULL);

  struct Test **tt = NULL, *t = NULL;
  t = malloc(sizeof *t);
  t->ch = 'X';
  aFA(&out, "--%c--", (*aAppend(&tt, t))->ch);
  aFA(&out, "--%c--", aAt(tt, 0)->ch);
  aFree(&tt); free(t);

  int *i = NULL;
  size_t ii = 1;
  aAppend(&i, 10);
  aFA(&out, "\n%ii", aAt(i, --ii));
  aFA(&out, " %ii\n", aAt(i, ii--));
  ii = 1;
  if(!setjmp(expected_err)) (void)aAt(i, ++ii);
  ii = 0;
  aFA(&out, "\n%uz", aL((++ii, i)));
  aFA(&out, " %ii", ii);
  aFree(&i);

  aError = &AARRAY_aError;
  return aAppend((char***)NULL, aStr("test10 NULL param"),
                 aStr("Error at _: parameter is NULL"
                     "--X----X--"
                     "\n+10 +10\n"
                     "Error at _: out of bounds (length=1 but pos=2)"
                     "\n1 +1")); }


char**test11(void) {
  struct Test **tt = NULL, *ta = NULL, *tb = NULL;
  ta = malloc(sizeof *ta);
  tb = malloc(sizeof *tb);
  aAppend(&tt, ta);
  (void)aAt2(tt, 0, tb); // int/pointer conversion warning hidden
  aAt(tt, 0)->ch = 'a';
  aFA(&out, "%c", aAt(tt, 0)->ch);
  free(ta); free(tb); aFree(&tt);

  char*x = NULL; aA(&x, 'a', 'b');
  size_t xl = aL(x);
  (void)aL2(x, 2);    if(xl != aL(x)) aFA(&out, "wrong");
  (void)aZL2(x, 0); if(xl != aL(x)) aFA(&out, "wrong");
  (void)aZL2(x, 2); if(0  != aL(x)) aFA(&out, "wrong");
  aFree(&x);

  return aAppend((char***)NULL, aStr("test11"), aStr("a")); }


#if __has_extension(blocks)
char**test12(void) {
  short*a = aAppend((short**)NULL, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18);
  aLoop(a, 0, ^(size_t pos){
      (void)aAt2(a,pos, aAt(a,pos)+1);
      return 4; });
  aFA(&out, "[%v %is]",a);
  aFree(&a);
  return aAppend((char***)NULL, aStr("test12"), aStr("[+2 +2 +3 +4 +6 +6 +7 +8 +10 +10 +11 +12 +14 +14 +15 +16 +18 +18]")); }
#endif

int filter_test(short sp)  { return !(sp%2); }
int filter_test2(short sp) { return sp!=6; }
char**test13(void) {
  short*a = aAppend((short**)NULL, 1,2,3,4,5,6,7,8);
  aFilter_FUNC(a, filter_test);
  aFilter_FUNC(a, filter_test2);
  aFA(&out, "[%v %us]",a);
  aFree(&a);
  return aAppend((char***)NULL, aStr("test13"), aStr("[2 4 8]")); }

#if __has_extension(blocks)
char**test13b(void) {
  short*a = aAppend((short**)NULL, 1,2,3,4,5,6,7,8);
  aFilter(a, ^(short sp){ return !(sp%2); });
  aFilter(a, ^(short sp){ (void)sp; return 1; });
  aFA(&out, "[%v %us]",a);
  aFree(&a);
  return aAppend((char***)NULL, aStr("test13b"), aStr("[2 4 6 8]")); }
#endif

#if __has_extension(blocks)
char**test13c(void) {
  short*a =  aAppend((short**)NULL, 1,2,3,4,5,6,7,8);
  aFilter_BLOCK(a, ^(short sp){ return sp > 4; });
  aFilter_FUNC(a, filter_test);
  aFA(&out, "[%v %us]",a);
  aFree(&a);
  return aAppend((char***)NULL, aStr("test13c"), aStr("[6 8]")); }
#endif

int fold_plus(int a, short b) { return a + b; }
short fold_mult(short a, short b) { return a * b; }
char fold_mult_char(char a, short b) { return a * (char)b; }
short** fold_array(short**a, short b) { aAppend(a, b); return a; }
char**test14(void) {
  short*a =  aAppend((short**)NULL,1,2,3,4,5,6);
  aFA(&out, "%is, ",aFold_FUNC(a,(int)0,fold_plus, NULL));
  //aFA(&out, " %us",aFold_FUNC(a,(char)1,fold_mult_char, NULL));
  aFree(&a);
  aFA(&out, "%is, ",aFold_FUNC(a,(int)0,fold_plus, NULL));
  aAppend(&a, 2);
  aFA(&out, "%is, ",aFold_FUNC(a,(int)8,fold_plus, NULL));
  aAppend(&a, 4,5,6);
  short*aa = NULL; (void)aFold_FUNC(a,&aa,fold_array, NULL);
  aFA(&out, "[%v %is]",aa);
  aFree(&a);
  aFree(&aa);
  return aAppend((char***)NULL, aStr("test14"), aStr("+21, +0, +10, [+2 +4 +5 +6]")); }

int descending_ints(int item1, int item2)       { return item1 > item2; }
int descending_shorts(short item1, short item2) { return item1 > item2; }
char**test15(void) {
  short*a = aAppend((short**)NULL,3,1,2,6,4,5);
  aSort(a);
  aFA(&out, "[%v %us]",a);
  aSortF_FUNC(a, descending_shorts);
  aFA(&out, "[%v %us]",a);

  aFree(&a); aSort(a); aSortF_FUNC(a, descending_shorts);
  aFA(&out, " [%v %us]",a);
  aAppend(&a, 5); aSort(a); aSortF_FUNC(a, descending_shorts);
  aFA(&out, "[%v %us]",a);
  aReplace(&a, 0,0, 15); aSort(a); aSortF_FUNC(a, descending_shorts);
  aFA(&out, "[%v %us]",a);

  int*aa = aAppend((int**)NULL,1000,15,2); aSort(aa);
  aFA(&out, "[%v %ui]",aa);
  aSortF_FUNC(aa, descending_ints);
  aFA(&out, "[%v %ui]",aa);

  aFree(&a); aFree(&aa);
  return aAppend((char***)NULL, aStr("test15"), aStr("[1 2 3 4 5 6][6 5 4 3 2 1] [][5][15 5][2 15 1000][1000 15 2]")); }

#if __has_extension(blocks)
char**test15_2(void) {
  short*a = aAppend((short**)NULL,3,1,2,6,4,5);
  aSort(a);
  aFA(&out, "[%v %us]",a);
  aSortF(a, ^(short b, short c){ return b > c; });
  aFA(&out, "[%v %us]",a);

  aFree(&a); aSort(a); aSortF(a, ^(short b, short c){ return b > c; });
  aFA(&out, " [%v %us]",a);
  aAppend(&a, 5); aSort(a); aSortF(a, ^(short b, short c){ return b > c; });
  aFA(&out, "[%v %us]",a);
  aReplace(&a, 0,0, 15, 12, 13); aSort(a); aSortF(a, ^(short b, short c){ return b > c; });
  aFA(&out, "[%v %us]",a);
  aFree(&a);
  return aAppend((char***)NULL, aStr("test15_2"), aStr("[1 2 3 4 5 6][6 5 4 3 2 1] [][5][15 13 12 5]")); }
#endif

char**test16(void) {
  float*f = NULL; float a=1.1f, b=2.2f,c=3.3f;
  aAppend(&f, *(int32_t*)&a, *(int32_t*)&b, *(int32_t*)&c);
  aFA(&out, "%f %f %f %f [%v %f]",*(int32_t*)&(f[0]),*(int32_t*)&a,*(int32_t*)&b,*(int32_t*)&c,f);

  double*d = NULL; double g=1.1, h=2.2,i=3.3;
  aAppend(&d, *(int64_t*)&g, *(int64_t*)&h, *(int64_t*)&i);
  aFA(&out, " %d %d [%v %d]",*(int64_t*)&g,*(int64_t*)&h,d);

  int*minus = NULL; aAppend(&minus, -1, -2, -3, -4);
  aFA(&out, "\n%v %i, %v %u, %i %u", minus, minus, -1, -1);
  aFA(&out, "\n%i8 %u8", -1, -1);

  aFree(&f); aFree(&d); aFree(&minus);
  return aAppend((char***)NULL, aStr("test16"), aStr("1.1 1.1 2.2 3.3 [1.1 2.2 3.3] 1.1 2.2 [1.1 2.2 3.3]"
                                                     "\n-1 -2 -3 -4, 4294967295 4294967294 4294967293 4294967292, -1 4294967295"
                                                     "\n-1 255")); }

char**test17(void) {
  int*t = NULL;
  aAppend(&t, 10);
  aAppend(&t, 15);
  aAppend(&t, 20);
  aAppend(&t, 25);
  aFA(&out, "=== ");
  size_t testIndex = 6;
  int item_zero = 0, item_three = 3;
  while(testIndex--) {
    if(testIndex>aLength(t))
      aMulti(&t,
             aLength(t), 0, (testIndex-aLength(t)), 1, (intptr_t)&item_zero,
             1, 1, 1, 1, (intptr_t)&item_three);
    else aReplace(&t, testIndex, 0, (int)3); }
    // 3 10 3 3 3 20 3 25 3 0
  aFmtA(&out, "%v %2ui ===", (intptr_t)t);
  aFree(&t);
  return aAppend((char***)NULL, (intptr_t)aStr("test17"), (intptr_t)aStr("=== 11 1010 11 11 11 10100 11 11001 11 0 ===")); }

char**test18(void) {
  // Arbitrary array updates reduced to single call
  int *va = NULL;
  aAppend(&va, 1, 2, 3);
  aReplace(&va, 0,0, -2, -1, 0);
  aReplaceArray(&va, 1, 3, 1, (int[]){5});
  aDelete(&va, aLength(va)-1, 1);
  // va now {-2, 5, 2}

  int*vb = NULL;
  aMulti(&vb
    	   ,0, 0, 1, 3, (int[]){1, 2, 3}
    	   ,0, 0, 1, 3, (int[]){-2, -1, 0}
    	   ,1, 3, 1, 1, (int[]){5}
           ,3, 1, 0, 0, NULL);

  // vb now the same as va, but done in one realloc
  aFA(&out, "%v %i, %v %i", va, vb, -1, -1);
  aFree(&va); aFree(&vb);
  return aAppend((char***)NULL, aStr("test1"), aStr("-2 +5 +2, -2 +5 +2")); }

char**test19(void) {
  // Arbitrary array updates reduced to single call
  int*vb = NULL;
  aMulti(&vb
    	   ,0, 0, 2, 3, (int[]){1, 2, 3}
    	   ,1, 4, 1, 1, (int[]){-2, -1, 0}
    	   ,3, 0, 2, 1, (int[]){5}
           ,3, 1, 0, 0, NULL);

  // vb now the same as va, but done in one realloc
  aFA(&out, "%v %i", vb);
  aFree(&vb);
  return aAppend((char***)NULL, aStr("test19"), aStr("+1 -2 +3 +5")); }

void is5(int*i) { if(*i!=5) aFA(&out, "nope"); }
char**test20(void) {
  // Arbitrary array updates reduced to single call
  int*s = NULL; aAppend(&s, 5);
  aReplaceArray(&s, 0, 0, 1, s, 1, s, 1, s);
  aMap_FUNC(s, is5, NULL);
  aFree(&s); aAppend(&s, 5);
  aMulti(&s,
         0, 0, 1, 1, s,
         0, 0, 1, 1, s,
         0, 0, 1, 1, s,
         0, 0, 1, 1, s,
         0, 0, 1, 1, s,
         0, 0, 1, 1, s,
         0, 0, 1, 1, s,
         0, 0, 1, 1, s);
  aMap_FUNC(s, is5, NULL);
  aFree(&s); aAppend(&s, 5);
  aConcat(&s, s, s, s, s, s, s, s, s);

  aFA(&out, "%v %i", s);
  aFree(&s);
  return aAppend((char***)NULL, aStr("test20"), aStr("+5 +5 +5 +5 +5 +5 +5 +5 +5")); }

struct big { uint64_t a, b; };
struct mid { uint64_t a; };
int fold_int_a_int(int a, int item) { return a + item; }
int fold_int_a_max(int a, uintmax_t item) { return (int)((unsigned int)a) + (int)item; }
uintmax_t fold_max_a_int(uintmax_t a, int item) { return a + (unsigned int)item; }
uintmax_t fold_max_a_max(uintmax_t a, uintmax_t item) { return a + item;aF("==."); }
#if __has_feature(address_sanitizer)
__attribute((no_sanitize("address")))
#endif
char**test21(void) {
  aError = &aError_jmp;
  struct mid*m = NULL;
  if(!setjmp(expected_err)) aAppend(&m, NULL);
  struct big*b = NULL;
  ////
  //// b is >8 bytes, so this should throw an exception
  //// should also cause ub !!!
  aFE("Note: confirming error message abort on undefined behaviour\n");
  if(!setjmp(expected_err)) aAppend(&b, NULL);
  ////
  ////
  ////
  uintmax_t minus = (uintmax_t)-1;
  int*as = NULL; aA(&as, 1, 2, 3);
  struct mid**ms = NULL; aA(&ms, NULL, NULL, 1, 2, minus);
  int output1 = aFold_FUNC(as, 0, fold_int_a_int, NULL);
  int output2 = aFold_FUNC(ms, 0, fold_int_a_max, NULL);
  aFA(&out, "\noutput = %i %i", output1, output2);
  aFree(&m); aFree(&b);  aFree(&as); aFree(&ms);
  aError = &AARRAY_aError;
  return aAppend((char***)NULL, aStr("test21"), aStr("Error at _: array type too wide (max 8 bytes)\n"
                                                     "output = +6 +2")); }

void addOne(int*aptr) { *aptr = (*aptr) + 1; }
int compareInt(int a,int b) { return a - b; }
char**test22(void) {
  int*a = NULL; aA(&a, 1, 4, 2);
  unsigned int*b = (unsigned int*)(int*)a;
  aMap_FUNC(a, addOne, NULL);
  aMap_FUNC(b, addOne, NULL);
  aSort(a);
  aSort(b);
  size_t i;
  aSearch(a, &i, 5);
  aSearch(b, &i, 5);
  aSearchF_FUNC(a, &i, 4, compareInt);
  aSearchF_FUNC(b, &i, 4, compareInt);
  aFree(&a);
  aFA(&out, "all fine");
  return aAppend((char***)NULL, aStr("test22 -- testing ub for function call types"), aStr("all fine")); }

int bcmpp8(const void*a, const void*b) { return (int16_t)(((uint8_t*)a)[0]) - (int16_t)(((uint8_t*)b)[0]); }
int64_t ccmp8(uint8_t a, uint8_t b)  { return a - b; }
int64_t icmp8(uint8_t a, uint8_t b)  { return ((int16_t)a) - ((int16_t)b); }
int64_t scmpp8(uint8_t*a, uint8_t*b) { return ((int32_t)a[0])-b[0]; }
int64_t ccmp16(uint16_t a, uint16_t b)  { return ((int32_t)a)-b; }
int64_t scmpp16(uint16_t*a, uint16_t*b) { return ((int32_t)a[0])-b[0]; }
void test_search_(uint8_t* mem, uint64_t k, size_t length) {
	//if((k%100)==0)aF("l%uzk%uz", length, k);
	int f1,f2,f3,f4,f5;
	size_t p1,p2,p3,p4,p5;
	uint8_t*a = aArray((uint8_t*)mem, length, true);
	a = aSort(a);
	void*ptr = bsearch(&k, a, aL(a), 1, bcmpp8);
	int f0 = ptr!=NULL; size_t p0 = ptr? (size_t)(((uint8_t*)ptr) - a) : 0;
	f1 = aSearch(a, &p1, k);
	f2 = aSearchF_FUNC(a, &p2, k, ccmp8);
	f3 = aSearchPF_FUNC(a, &p3, k, icmp8);
	f4 = aSearchPS_FUNC(a, &p4, &k, scmpp8, 1);
	f5 = aSearchP(a, &p5, k);
	if(!(f1==f2 && f1==f3 && f1==f4 && p1==p2 && p3==p4 && f0==f1 && (f0? p0==p1 : true))) {
		aFE("(%uz %uz)", f0, p0);
		aFE("(%uz %uz %uz %uz %uz %uz %uz %uz)", f1, f2, f3, f4, p1, p2, p3, p4);
		aFE("[%v %u8] %uz, %u8  ", a, aL(a), k); }
	assert(f0==f1 && f1==f2 && f1==f3 && f1==f4 && f1==f5);
	assert(f0? p0==p1 : true);
	assert(p1==p2 && p3==p4 && p3==p5);
	uint16_t*aa = aArray((uint16_t*)(void*)mem, length, true);
	aSort(aa);
	f1 = aSearchPF_FUNC(aa, &p1, k, ccmp16);
	f2 = aSearchPS_FUNC(aa, &p2, &k, scmpp16, 1);
	if(!(f1==f2 && p1==p2)) {
		aF("(%uz %uz %uz %uz)", f1, f2, p1, p2);
		aF("[%v %u16], %u16", aa, k); }
	assert(f1==f2 && p1==p2); }

char**test_search(void) {
	size_t memsize = sizeof(size_t)*2+10*sizeof(uint32_t);
	uint8_t*mem = malloc(memsize);
	srand(0);
	size_t n = 2000;
	while(n--) {
		uint32_t*a = aArray((uint32_t*)(void*)mem, memsize, true);
		size_t nn = aL(a); while(nn--) (void)aAt2(a, nn, rand());
		size_t k = 255; while(k--) test_search_(mem, (uint64_t)rand(), memsize); }
	free(mem);
	aFA(&out, "all fine");
  return aAppend((char***)NULL, aStr("test_search"), aStr("all fine")); }

int bcmpp64(const void*a, const void*b) {
	uint64_t aa = (((uint64_t*)a)[0]), bb = (((uint64_t*)b)[0]);
	//return (((uint64_t*)a)[0]) - (((uint64_t*)b)[0]);
	return aa==bb? 0 : aa>bb? 1 : -1; }
int64_t ccmp64(uint64_t a, uint64_t b)  {
	return a==b? 0 : a>b? 1 : -1; }
int64_t scmpp64(uint64_t*a, uint64_t*b) {
	return a[0]==b[0]? 0 : a[0]>b[0]? 1 : -1; }
char**test_search_edgecases(void) {
	size_t memsize = sizeof(size_t)*2+3*sizeof(uint64_t);
	uint8_t*mem = malloc(memsize);
	uint64_t*vals = (uint64_t[]){0,1,2,3,UINT64_MAX,UINT64_MAX-1,UINT64_MAX-2,UINT32_MAX,UINT32_MAX-1,UINT32_MAX-2,UINT16_MAX,UINT16_MAX-1,UINT16_MAX-2};
	uint64_t*a = aArray((uint64_t*)(void*)mem, 16+24, true);
	uint64_t k = 0;
	size_t vk = -1ull, va=-1ull,vb=-1ull,vc=-1ull; while(++va<13) { while(++vb<13) { while(++vc<13) { while(++vk<13) {
		k = vals[vk];
		(void)aAt2(a, 0, vals[va]);
		(void)aAt2(a, 1, vals[vb]);
		(void)aAt2(a, 2, vals[vc]);
		(void)aSort(a);
		void*ptr = bsearch(&k, a, aL(a), 8, bcmpp64);
		int f0 = ptr!=NULL; size_t p0 = ptr? (size_t)(((uint64_t*)ptr) - a) : 0;
		int f1,f2,f3,f4;
		size_t p1,p2,p3,p4;
		f1 = aSearch(a, &p1, k);
		f2 = aSearchF_FUNC(a, &p2, k, ccmp64);
		f3 = aSearchPF_FUNC(a, &p3, k, AARRAY_searchCompare__uint64_t);
		f4 = aSearchPS_FUNC(a, &p4, &k, scmpp64, 1);
		if(!(f1==f2 && f1==f3 && f1==f4 && p1==p2 && p3==p4 && f0==f1 && (f0? p0==p1 : true))) {
			aFE("(%uz %uz)", f0, p0);
			aFE("(%uz %uz %uz %uz %uz %uz %uz %uz)", f1, f2, f3, f4, p1, p2, p3, p4);
			aFE("[%v %u64] %uz, %u64  ", a, aL(a), k); }
		assert(f0==f1 && f0==f2 && f3==f4);
		assert(f0? p0==p1 : true);
		assert(p1==p2);
		if(f1) assert(aAt(a,p1)==k);
		else if(p1<aL(a) && aAt(a,p1)<k) {
			p1++; assert(p1==aL(a) || aAt(a,p1)>k); }
		else if(p4<aL(a) && aAt(a,p4)<k) {
			p4++; assert(p4==aL(a) || aAt(a,p4)>k); } }
	vk = -1ull; }
	vc = -1ull; }
	vb = -1ull; }
	va = -1ull;
	free(mem);
	aFA(&out, "all fine");
  return aAppend((char***)NULL, aStr("test_search_edgecases"), aStr("all fine")); }

char**test23(void) {
	aFA(&out, "%i64 | %u64 | %i64 | %u64 | %i64 | %u64 | %i64 | %u64", INT64_MAX,INT64_MAX, INT64_MIN,INT64_MIN, UINT64_MAX, UINT64_MAX, (uint64_t)0, (uint64_t)0);
  return aAppend((char***)NULL, aStr("test23"), aStr("+9223372036854775807 | 9223372036854775807 | -9223372036854775808 | 9223372036854775808 | -1 | 18446744073709551615 | +0 | 0")); }

// without bsearch overflow
// (0 0)(0 0 1 1 1 1 2 2)[0 0 18446744073709551615] 3, 18446744073709551615
// 
// INT64_MIN%10 = ??? -- is this a hardware bug we're working around
// 
int main(int argsnum, char**args) {
  aF("running tests ...\n");

  unittest(test1);
  unittest(test2);
  unittest(test3);
  unittest(test4);
  unittest(test5);
  unittest(test6);
  //unittest(test7);
  //unittest(test8);
  //unittest(test9);
  //unittest(test10);
  //unittest(test11);
  #if __has_extension(blocks)
  unittest(test12);
  unittest(test13b);
  unittest(test13c);
	unittest(test15_2);
  #endif
  unittest(test13);
  unittest(test14);
  unittest(test15);
  unittest(test16);
  unittest(test17);
  //unittest(test18);
  //unittest(test19);
  //unittest(test20);
  //unittest(test21);
  //unittest(test22);
	//unittest(test_search);
	//unittest(test_search_edgecases);
  //unittest(test23);

  if(test_failed) { aF("tests failed\n"); exit(-1); }
  if(argsnum>1) { // remember test was successful
    FILE*outfile = fopen(args[1], "w");
    if(!outfile) { aF("couldn't find file %s",args[2]); exit(-1); } }
  aF("\n... tests passed\n"); }
