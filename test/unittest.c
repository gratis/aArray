#define UNIQUE_NAME "c"
#include "unittest.h"

char**test1(void) {
  int*t = NULL;
  aAppend(&t, 10);
  aAppend(&t, 15);
  aAppend(&t, 20);
  aAppend(&t, 25);
  aFA(&out, "=== ");
  size_t testIndex = 6;
  int item_zero = 0, item_three = 3;
  while(testIndex--) {
    if(testIndex>aLength(t))
      aMulti(&t,
             aLength(t), 0, (testIndex-aLength(t)), 1, (intptr_t)&item_zero,
             1, 1, 1, 1, (intptr_t)&item_three);
    else aReplace(&t, testIndex, 0, (int)3); }
    // 3 10 3 3 3 20 3 25 3 0
  aFmtA(&out, "%v %2ui ===", (intptr_t)t);
  aFree(&t);
  return aAppend((char***)NULL, (intptr_t)aStr("test1 ints"), (intptr_t)aStr("=== 11 1010 11 11 11 10100 11 11001 11 0 ===")); }

char**test2(void) {
  char*s = NULL;
  aAppend(&s, 'a');
  aAppend(&s, 'a');
  aAppend(&s, 'a');
  s[2] = 'x';
  s[0] = 'y';
  ////aInsert(&s, 5, 'b');
  aMulti(&s, 
         3, 0, 2, 1, (intptr_t)(char[]){0},
         5, 0, 1, 1, (intptr_t)(char[]){'b'});
  size_t n = (size_t)-1; while(++n < aLength(s))
                           aFA(&out, "%c|", s[n]?s[n]:' ');
  aFree(&s);
  return aAppend((char***)NULL, (intptr_t)aStr("test2 chars"), (intptr_t)aStr("y|a|x| | |b|")); }
char**test4(void) {
  char*s = NULL;
  aAppend(&s, 'a', 'b', 'c');
  aAppend(&s, 'd', 'e');
  aAppend(&s, 'd', 'e');
  (void)aAt2(s, aL(s)-5, 'f');
  aAppend(&s, 'a', 'b', 'c');
  aAppend(&s, 'd', 'e');
  aFA(&out,"%c%c%c%c",s[2],aAt(s,4),
         aAt(s,aL(s)-1),*aAtPtr(s,aL(s)-2));
  aAppend(&s, 'y', 'y', '\0');
  char*c = (char*)aAtPtr(s,aL(s)-4);
  *c = ' '; *(c+sizeof(char)) = 'm';
  aFA(&out, "%s%c", (intptr_t)c, *aAtPtr(s, aL(s)-4));
  (void)aLength2(s,aL(s)-1);
  char*sheep = "sheep";
  aAppendArray(&s, 6, (intptr_t)sheep);
  aFA(&out,"%s", (intptr_t)aAtPtr(s,aL(s)-6)); aFree(&s);
  return aAppend((char***)NULL, (intptr_t)aStr("test4 string ops"), (intptr_t)aStr("feed my sheep")); }
//return "string ops\0" "feed my sheep\0"; }
char**test5(void) {
  char*s = NULL;
  aAppendArray(&s, 4, (intptr_t)"cdle");
  aReplace(&s, 1, 0, 'o', 'o', 'o', 'o', 'o');
  aAppendArray(&s, 11, (intptr_t)" was a dear");
  aReplaceArray(&s, aLength(s)-7, 0, 3, (intptr_t)"n't");
  aAppend(&s, 'e','s','t');
  aDelete(&s, aLength(s)-3, 3);
  aAppend(&s, 'y');
  if(aWriteF(outFILE, s)) aFA(&out, "!"); aFree(&s);
  char*outfile = FILEtoArray(outFILE);
  aConcat(&out, outfile); aFree(&outfile);
  return aAppend((char***)NULL, (intptr_t)aStr("test5 arrays"), (intptr_t)aStr("cooooodle wasn't a deary")); }
char**test6(void) {
  long*ss = NULL;
  long ll = 5000;
  aAppendArray(&ss, 3, (intptr_t)(long[]){1,5,10});
  aAppend(&ss,ll,2,3);
  aReplaceArray(&ss, 0, 0, 1, (intptr_t)(long[]){0});
  aReplaceArray(&ss, 0, 7, 1, (intptr_t)(long[]){200});
  long item_one = 1;
  aMulti(&ss, aLength(ss), 0,
    3, 1, (intptr_t)&item_one);
  aReplace(&ss,aLength(ss)-1,1,2);
  aFmtF(outFILE, "%v %2ul ", (intptr_t)ss);
  aFree(&ss);
  char*sss = NULL;
  aAppendArray(&sss, SIZE_MAX, (intptr_t)"abcde happy joy Jesu!!\n!\n");
  aAppend(&sss, (char)50000);
  aDelete(&sss, aLength(sss)-1, 1);
  if(aWriteF(outFILE, sss)) aFA(&out, "!"); aFree(&sss);
  char*outfile = FILEtoArray(outFILE);
  aConcat(&out, outfile); aFree(&outfile);
  return aAppend((char***)NULL, (intptr_t)aStr("test6 arrays 2"), (intptr_t)aStr("11001000 1 1 10 abcde happy joy Jesu!!\n!\n")); }
char**test7(void) {
  char*s = NULL;
  aAppend(&s, 'c');
  aAppend(&s, 'c');
  aAppendArray(&s, SIZE_MAX, (intptr_t)"ddeeff");
  aReplace(&s, 0,0, 'a');
  aReplace(&s, 2,0,'a');
  aReplace(&s, 4,0,'a');
  char*t = NULL; aAppendArray(&t, SIZE_MAX, (intptr_t)"hello__"); aAppend(&t, '\0');
  aConcat(&s, s,t,s);
  aReplaceArray(&s, aLength(s)-2, 2, SIZE_MAX, (intptr_t)"hello", 3, (intptr_t)"hey");
  //aFree(&s); aAppendArray(&s, SIZE_MAX, "abcde");
  aDelete(&s,1,43);
  if(aWriteF(outFILE, s)) aFA(&out, "!"); aFree(&s); aFree(&t);
  char*outfile = FILEtoArray(outFILE);
  aConcat(&out, outfile); aFree(&outfile);
  return aAppend((char***)NULL, (intptr_t)aStr("test7 arrays 3"), (intptr_t)aStr("ahey")); }

#include <math.h>
void bottles(size_t n) {
  char*temp;
  char*song = NULL;
  char number[23] = {0};
  snprintf(number, 23, "%zu", n);
 stanza: {
    char*wall = " bottles of beets on the wall. ";
    aReplaceArray(&song, 0, aLength(song),
                  SIZE_MAX, (intptr_t)number, SIZE_MAX, (intptr_t)wall, SIZE_MAX, (intptr_t)number, 17, (intptr_t)wall,
                  SIZE_MAX, (intptr_t)".\nPass some round and glug them down.\n");
    snprintf(number, 23, "%zu", n >>= 1);
    if(n > 1) {
      temp = (char*)aAppendArray(&song, SIZE_MAX, (intptr_t)number, 30, (intptr_t)wall, SIZE_MAX, (intptr_t)"\n\n");
      if(aWriteF(outFILE, temp)) aFA(&out, (intptr_t)"!");
      goto stanza; } }
  temp = (char*)aAppendArray(&song, SIZE_MAX, (intptr_t)"How many bottles of beets?");
  if(aWriteF(outFILE, temp)) aFA(&out, "!");
  aFree(&song); }
char**test8(void) {
  bottles(31);
  char*outfile = FILEtoArray(outFILE);
  aConcat(&out, outfile); aFree(&outfile);

  return aAppend((char***)NULL, (intptr_t)aStr("bottles"), (intptr_t)aStr(
"31 bottles of beets on the wall. 31 bottles of beets.\n"
"Pass some round and glug them down.\n"
"15 bottles of beets on the wall.\n"
"\n"
"15 bottles of beets on the wall. 15 bottles of beets.\n"
"Pass some round and glug them down.\n"
"7 bottles of beets on the wall.\n"
"\n"
"7 bottles of beets on the wall. 7 bottles of beets.\n"
"Pass some round and glug them down.\n"
"3 bottles of beets on the wall.\n"
"\n"
"3 bottles of beets on the wall. 3 bottles of beets.\n"
"Pass some round and glug them down.\n"
"How many bottles of beets?")); }

#ifndef __has_feature
#define __has_feature(x) 0  // Compatibility with non-clang compilers
#endif
#ifndef __has_extension
#define __has_extension __has_feature // Compatibility with pre-3.0 compilers
#endif

#if __has_extension(blocks)
char**test9(void) {
  char*cs1 = aAppendArray((char**)NULL, SIZE_MAX, "happy");
  aMap(cs1, ^(char*c){ aFF(outFILE, (intptr_t)"|%c|",(uint64_t)*c); *c = (*c)+1; });
  aFF(outFILE, "--");
  aMap(cs1, ^(char*c){ aFF(outFILE, (intptr_t)"|%c|",(uint64_t)*c); });
  aFF(outFILE, "--");
  aLoop(cs1, aL(cs1)-1, ^(size_t n){
      (void)aAt2(cs1,n,aAt(cs1,n)-1);
      aFF(outFILE, (intptr_t)"|%c|",(uint64_t)cs1[n]); return -2; });
  aFree(&cs1);
  char*outfile = FILEtoArray(outFILE);
  aConcat(&out, outfile); aFree(&outfile);
  return aAppend((char***)NULL, (intptr_t)aStr("Loop/Map1"), (intptr_t)aStr("|h||a||p||p||y|--|i||b||q||q||z|--|y||p||h|")); }
#endif

void vm1(char*c, void*data) { (void)data; aFF(outFILE, "|%c|",*c); *c = (*c)+1; }
void vm2(char*c, void*data) { (void)data; aFF(outFILE, "|%c|",*c); }
static char*test9_cs1;
int vl1(size_t n, void*data) { (void)data;
  (void)aAt2(test9_cs1,n,aAt(test9_cs1,n)-1);
  aFF(outFILE, "|%c|",test9_cs1[n]); return -2; }
char**test9_FUNC(void) {
  char*cs1 = aAppendArray((char**)NULL, SIZE_MAX, (intptr_t)"happy");
  aMap_FUNC(cs1, &vm1, NULL);
  aFF(outFILE, "--");
  aMap_FUNC(cs1, &vm2, NULL);
  aFF(outFILE, "--");
  test9_cs1 = cs1;
  aLoop_FUNC(cs1, aL(cs1)-1, &vl1, NULL);  
  char*outfile = FILEtoArray(outFILE);
  aConcat(&out, outfile); aFree(&outfile);
  return aAppend((char***)NULL, (intptr_t)aStr("Loop/Map2"), (intptr_t)aStr("|h||a||p||p||y|--|i||b||q||q||z|--|y||p||h|")); }

char**test10(void) {
  short*s = NULL;
  aAppend(&s, 1, 2, 3, 4);
  aAppend(&s, aAt(s,0), aAt(s,1), aAt(s,2), aAt(s,3));
  aAppendArray(&s,
               1, (intptr_t)aAtPtr(s, 0),
               1, (intptr_t)aAtPtr(s, 1),
               1, (intptr_t)aAtPtr(s, 2),
               1, (intptr_t)aAtPtr(s, 3),
               1, (intptr_t)aAtPtr(s, 4),
               1, (intptr_t)aAtPtr(s, 5),
               1, (intptr_t)aAtPtr(s, 6),
               1, (intptr_t)aAtPtr(s, 7),
               16, (intptr_t)aAtPtr(s, 0));
  aFmtA(&out, "%v %us ", (intptr_t)s); aFree(&s);
  return aAppend((char***)NULL, (intptr_t)aStr("aAPtr"), (intptr_t)aStr("1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 ")); }

char**test11(void) {
  char*cs = aAppend((char**)NULL, 'a',' ','m','a','n','!');
  aReplaceArray(&cs, aLength(cs)-4, 3, SIZE_MAX, (intptr_t)"saviour");
  aReplaceArray(&cs, 2,0, SIZE_MAX, (intptr_t)"wonderful ", 10, (intptr_t)aAtPtr(cs, 2));
  if(aWriteF(outFILE, cs)) aFA(&out, "!");
  aFree(&cs);
  char*outfile = FILEtoArray(outFILE);
  aConcat(&out, outfile); aFree(&outfile);
  return aAppend((char***)NULL, (intptr_t)aStr("saviour"), (intptr_t)aStr("a wonderful wonderful saviour!")); }

char**test12(void) {
  int*s = NULL;
  aAppend(&s, 80,1080,280);
  aDelete(&s,1,1);
  aFmtA(&out, "%v %ui", (intptr_t)s);
   aFree(&s);
  return aAppend((char***)NULL, (intptr_t)aStr("errors"), (intptr_t)aStr("80 280")); }

char**test13(void) {
  short*s = NULL;
  aAppend(&s,1);
  //aInsert(&s,3, 3);
  aMulti(&s, 
         1, 0, 2, 1, (intptr_t)(short[]){0},
         3, 0, 1, 1, (intptr_t)(short[]){3});
  aReplace(&s,aLength(s)-1,0, 2);
  //aInsert(&s,aLength(s)-10, 4);
  //aInsert(&s,-11, 3);
  aMulti(&s, 
         0, 0, 1, 1, (intptr_t)(short[]){4},
         1, 0, 2, 2, (intptr_t)(short[]){0,4},
         6, 0, 0, 0, (intptr_t)s);
  aFmtA(&out, "%v %us ", (intptr_t)s);
  aFree(&s);
  return aAppend((char***)NULL, (intptr_t)aStr("negative pos"), (intptr_t)aStr("4 0 4 0 4 1 0 0 2 3 ")); }

/*char**test14s(void) {
  int*s = NULL;
  aAppendTINY(&s, 0, 1, 2);
  aInsertArrayTINY(&s, 1, 6, (int[]){10, 11, 12, 13, 14, 15});
  aFA(&out, ">>");
  aDeleteTINY(&s,0,4);
  //aReplaceTINY(&s,0,4,);
  aFmtA(&out, "%v %16ui ", s);
  aFree(&s);
  return aAppend((char***)NULL, (intptr_t)aStr("test14s TINY"), (intptr_t)aStr(">>d e f 1 2 ")); }

char**test14f(void) {
  int*s = NULL;
  aAppendPAGE(&s, 0, 1, 2);
  aInsertArrayPAGE(&s, 1, 6, (int[]){10, 11, 12, 13, 14, 15});
  aFA(&out, ">>");
  aDeletePAGE(&s,0,4);
  aFmtA(&out, "%v %16ui ", s);
  aFree(&s);
  return aAppend((char***)NULL, (intptr_t)aStr("test14f PAGE"), (intptr_t)aStr(">>d e f 1 2 ")); }*/

char**test14static(void) {
  int*s = malloc(sizeof(size_t)*2+sizeof(int)*3);
  int*ss = aArray_STATIC(s, sizeof(size_t)*2+sizeof(int)*3, false); (void)aL2(ss, 0);
  aAppend_STATIC(&ss, 0, 1, 2);
  aReplaceArray_STATIC(&ss, 1, 2, 2, (intptr_t)(int[]){10, 11, 12, 13, 14, 15});
  aFA(&out, ">>");
  //aRemove_STATIC(&ss,0,1);
  aFmtA(&out, "%v %16ui ", (intptr_t)ss);
  free(s);
  return aAppend((char***)NULL, (intptr_t)aStr("test14static STATIC"), (intptr_t)aStr(">>0 a b ")); }

char**testSideEffects(void) {
  int*s = NULL;
  aA(&s,10);
  int index = 0, amount = 1, value = 20;
  aR(&s,(size_t)index++,(size_t)amount++,value++);
  aR(&s,0,0, 10);
  aR(&s,(size_t)--index,(size_t)--amount,--value);
  aFmtA(&out, "%v %ui ", (intptr_t)s);
  aFree(&s);
  return aAppend((char***)NULL, (intptr_t)aStr("testSideEffects"), (intptr_t)aStr("20 20 ")); }

char**testSideEffectsB(void) {
  int*ss[] = {NULL,NULL,NULL,NULL};
  aA(&ss[0],10);
  aA(&ss[1],20, 21, 22);

  int index = 0;
  aR(&ss[index++], 1,0, 11);
  index = 0;
  aA(&ss[index++], 12);
  aFmtA(&out, "%v %ui ", (intptr_t)ss[0]); aFA(&out, "| "); aFmtA(&out, "%v %ui ", (intptr_t)ss[1]);

  index = 0;
  aFA(&out, "-%ui- ", aLength2(ss[index++], 1));
  aFmtA(&out, "%v %ui ", (intptr_t)ss[0]);

  index = 0;
  aFA(&out, "\n>> %ui ", aAt(ss[index++],0));
  aFA(&out, "%ui ",    aAt2(ss[--index],0,5));
  aFA(&out, "%ui",     aAt(ss[index++],0));

  aFree(&ss[0]); aFree(&ss[1]);
  return aAppend((char***)NULL, (intptr_t)aStr("testSideEffectsB"), (intptr_t)aStr("10 11 12 | 20 21 22 -10- 10 \n>> 10 5 5")); }

static jmp_buf array_err;
__attribute((noreturn)) void aError_jmp(char*line, char*msg) {
  (void)line; // suppress unused-parameter warning
  aFA(&out, "Error file:line, %s\n", (intptr_t)msg);
  longjmp(array_err, 1); }

char**test_aError(void) {
  aFA(&out, "Lets play guess the array length\n");
  // Guess too high, and an "out of bounds" exception will lose you the game
  char*array = NULL;
  //srand(time(NULL));
  //aInsert(&array, rand() % 100, 0);
  aMulti(&array, 0, 0,
                /*(rand() % 100)*/52, 1, (intptr_t)(char[]){0});

  void(*aError_temp)(char*, char*) = aError;
  if(setjmp(array_err)) {
    aFA(&out, "Oops, too far! Goodbye\n");
    aError = aError_temp;
    aFree(&array);
    return aAppend((char***)NULL, (intptr_t)aStr("test_aError"), (intptr_t)aStr("Lets play guess the array length\nGuess a length: Error file:line, out of bounds (length=52 but pos=105)\nOops, too far! Goodbye\n")); }
  aError = aError_jmp;
  
  //size_t guess = 102; //// 101 with aReplaceWithN
  size_t guess = 100; int input_err;
  while(guess != aLength(array)) {
    aFA(&out, "Guess a length: ");
    //input_err = scanf("%zu", &guess);
    input_err = 1; guess = 105;
    if (input_err == 0 || input_err == EOF) {
      aFA(&out, "Oh dear, I don't understand what you wrote!\n");
      continue; }
    (void)aAt(array, guess);
    aFA(&out, "The array might be longer...\n"); } PORTABLE_unreachable(); }


__attribute((noreturn)) void aError_jmpB(char*line, char*msg) {
  (void)line; // suppress unused-parameter warning
  aFA(&out, "%s", (intptr_t)msg);
  longjmp(array_err, 1); }

char**testError(void) {
  void(*aError_temp)(char*, char*) = aError;
  aError = aError_jmpB;
  //aFA(&out, "1...");
  volatile int errnum = 0;
  int*static_backing = malloc(sizeof(size_t)*2+sizeof(int)*2);
  int*static_check = aArray(static_backing, sizeof(size_t)*2+sizeof(int)*2, false);
  (void)aL2(static_check, 0);
  int*check = NULL;
  if(setjmp(array_err)) ++errnum;
  switch(errnum) {
  case 0:
    aAppend_STATIC(&static_check, 1);
    aAppend(&check, 1);
    // aLoop no longer to give error
    //aLoop(static_check, 1, ^(size_t n){ (void)n; return 1; });
    errnum++;
    // fall through
  case 1:
    aAppendArray(&check, 0, (intptr_t)NULL);
    aAppendArray(&check, SIZE_MAX, (intptr_t)NULL, 1, (intptr_t)NULL);
    // fall through
  case 2: aReplace(&check, 0, 500, 2);
    // fall through
  case 3:
	  (void)aLength2(check, 2);
    // fall through
  case 4:
    (void)aAt(check, 5);
    // fall through
  case 5: aMulti(&check, 0, 0, 1, 2);
    // fall through
  case 6: aAppendArray(&check, 0);
    // fall through
  case 7: aAppend_STATIC(&static_check, 1, 2);
    // fall through
  case 8:
    free(aMem(static_check));
    aFree(&check);
    aError = aError_temp;
    return aAppend((char***)NULL, (intptr_t)aStr("testError"), /*"out of bounds (length=1 but pos=1)"*/
      (intptr_t)aStr("array is NULL (array no=1)"
      "removal is out of bounds (length=1 but pos=0 removal=500)"
      "out of bounds (length=1 but pos=2)"
      "out of bounds (length=1 but pos=5)"
      "wrong arg count (args=5 but should be 1 + multiple of 5)"
      "wrong arg count (args=2 but should be 1 + multiple of 2)"
      "out of capacity (size=24 but require=28)")); }
  PORTABLE_unreachable(); }


int setTo1(int i) { (void)i; return 1; }
void setTo1Ptr(int*i) { *i = setTo1(*i); }
char**testNULL(void) {
  // test for errors with a NULL array
  int*v_test = NULL;
  aIndexOf(v_test, 1);
  //aAt(v_test, 0);
  //aAt2(v_test, 6, 16);
  aMap_FUNC(v_test, setTo1, NULL);
  aMap_FUNC(v_test, setTo1Ptr, NULL);
  //aLoop(v_test, 0, ^(size_t n){ return 1; });
  aFmtA(&out, "%v %2ui", (intptr_t)v_test);
  if(aWriteF(outFILE, v_test)) aFA(&out, "!");
  aFree(&v_test);
  aFF(outFILE, "vl=%uz", aLength(v_test));
  (void)aLength2(v_test, 0);
  // test aIndexOf integer overflow check
  size_t length = (size_t)-7; int jump = 200;
  char*out2 = FILEtoArray(outFILE);
  aConcat(&out, out2); aFree(&out2);
  aFA(&out, "--%uz  %uz--",length-(length%jump)+(length%jump?jump:0), length);
  return aAppend((char***)NULL, (intptr_t)aStr("testNULL"), (intptr_t)aStr("vl=0--184  18446744073709551609--")); }

char**testZeroLen(void) {
  int*t = NULL;
  aA(&t, 1, 2, 3, 4);
  aD(&t, 0, 4);
  aA(&t, 5);
  aFmtA(&out, "%v %u32 ", (intptr_t)t);
  aD(&t, 0, 1);
  aA(&t, 6, 7);
  aFmtA(&out, "%v %u32 ", (intptr_t)t);
  aFree(&t);
  return aAppend((char***)NULL, (intptr_t)aStr("testZeroLen"), (intptr_t)aStr("5 6 7 ")); }

char**testBigFit(void) {
  // make sure NOCAPACITY works for largish arrays
  int8_t*t = NULL;
  size_t n = (size_t)-1;
  while(++n < 200000) aAppend_NOCAPACITY(&t, (int8_t)n);
  aFmtA(&out, "%uz ", aL(t));
  //(void)aLength2_NOCAPACITY(t, 10);
  aReplace_NOCAPACITY(&t, 5, 199000, 0);

  aFmtA(&out, "%uz", aL(t));
  aFree_NOCAPACITY(&t);

  // check aFmt buffer is large enough
  char buffer[(8 * sizeof(uintmax_t))+10] = {0};
  AARRAY_u_to_str(UINTMAX_MAX, buffer, 2);
  if(strlen(buffer)==(8 * sizeof(uintmax_t))) aFA(&out, " good");

  return aAppend((char***)NULL,
                 (intptr_t)aStr("BigFit"),
                 (intptr_t)aStr("200000 1001 good")); }

char**testMem(void) {
  char*t = NULL;
  aMem(t);
  aMem(t);
  if(t!=NULL) aFA(&out, "mem");
  aA(&t, 5, 4, 3, 2, 1);
  char*tt = t;
  if(tt!=aArray(aMem(t), sizeof(size_t)*2+2, false)) aFA(&out, "mem");
  (void)aL2(t, 2);
  aA(&t, 9, 8, 7, 6);
  (void)aL2(t, 4);
  aFA(&out, "[%v %uc]", t);
  aFree(&t);

  aMem_NOCAPACITY(t);
  if(t!=NULL) aFA(&out, "mem");
  aAppend_NOCAPACITY(&t, 1, 2, 3);
  tt = t;
  t = aArray_NOCAPACITY(aMem_NOCAPACITY(t), sizeof(size_t), false);
  if(tt!=t) aFA(&out, "mem");
  (void)aL2(t, 2);
  aMem_NOCAPACITY(t);
  aAppend_NOCAPACITY(&t, 2);
  aFA(&out, "[%v %uc]", t);
  aFree_NOCAPACITY(&t);

	tt = malloc(200);
  t = aArray_STATIC(tt, 200, false); (void)aL2(t, 0);
  aAppend_STATIC(&t, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
  if(tt != aMem_STATIC(t)) aFA(&out, "mem");
  tt = t;
  t = aArray_STATIC(aMem_STATIC(t), 200, false);
  if(tt!=t) aFA(&out, "mem");
  t = aArray_STATIC(aMem_STATIC(t), sizeof(size_t)*2, false);
  t = aArray_STATIC(aMem_STATIC(t), sizeof(size_t)*2+2, false); (void)aL2(t, 0);
  aAppend_STATIC(&t, 2, 3);
  aMem_STATIC(t);
  aFA(&out, "[%v %uc]", t);
  free(aMem(t));
  return aAppend((char***)NULL,
                 (intptr_t)aStr("Mem"),
                 (intptr_t)aStr("[5 4 9 8][1 2 2][2 3]")); }

char**testCmp(void) {
	char*a = aStr(".1234");
	char*b = aStr("x1234");
	char*c = aStr("x123x");
	char*d = aStr("x123x");
	aFA(&out, "%i %i",
		aCmp(a,b,c) == 0 && aCmp(a,b) == 0 && aCmp(c,d,b) == 0 && aCmp(b,c) == 0,
		aCmp(c,d) == 1 && aCmp(c,c) == 1 && aCmp(c,d,c));
	aFree(&a); aFree(&b); aFree(&c); aFree(&d);
  return aAppend((char***)NULL,
                 (intptr_t)aStr("Cmp"),
                 (intptr_t)aStr("+1 +1")); }

#if __has_extension(blocks)
void plus1(char*c) { *c += 1; }
void plusN(char*c, void*d) { *c += *(char*)d; }
int  is_l(char c) { return c != 'l'; }
int  is_N(char c, void*d) { return c != *(char*)d; }
size_t sumChar(size_t s, char c) { return s + (size_t)c; }
char      CharN(size_t s, char c, void*d) { (void)s; (void)c; return *(char*)d; }
size_t sumCharN(size_t s, char c, void*d) { return s + (size_t)c + (size_t)*(char*)d; }
static size_t n = 0;
int loop1(size_t nn)         { n += nn;  return 1; }
int loopN(size_t nn, void*d) { *(char*)d += nn; return 1; }
char**testOptional(void) {
	char*a = aStr("hello");
	aMap_FUNC(a, plus1);
	char d = -1;
	aFA(&out, "%v%c ", a);
	aMap_FUNC(a, plusN, &d);
	aFA(&out, "%v%c ", a);
	aMap(a, ^(char*c){ *c += 2; });
	aFA(&out, "%v%c ", a);
	d = -2;
	aMap(a, ^(char*c, void*dd){ *c += *(char*)dd; }, &d);
	aFA(&out, "%v%c ", a);
	aFilter_FUNC(a, is_l);
	aFA(&out, "%v%c ", a);
	aFree(&a); a = aStr("hello");
	d = 'e';
	aFilter_FUNC(a, is_N, &d);
	aFA(&out, "%v%c ", a);
	aFilter(a, ^(char c){ return c != 'l'; });
	aFA(&out, "%v%c ", a);
	d = 'h';
	aFilter(a, ^(char c, void*dd){ return c != *(char*)dd; }, &d);
	aFA(&out, "%v%c -- ", a);
	aFree(&a); a = aStr("hello");
	aFA(&out, "%c ", aFold_FUNC(a, 0, sumChar)/aL(a));
	d = 'z';
	aFA(&out, "%c ", aFold_FUNC(a, 0, sumCharN, &d)/(aL(a)*2)); // avg of 'hello' moved half way to 'z'
	aFA(&out, "%c -- ", aFold_FUNC(a, 0, CharN, &d));
	// vowel count
	aFA(&out, "%uz ", aFold(a, 0, ^(size_t b, char c){ return b + (c=='a'||c=='e'||c=='i'||c=='o'||c=='u'); }));
	// char count
	d = 'l';
	aFA(&out, "%uz ", aFold(a, 0, ^(size_t b, char c, void*dd){ return b + (c==*(char*)dd); }, &d));
	aLoop_FUNC(a, 0, loop1);
	d = 0;
	aLoop_FUNC(a, 0, loopN, &d);
	aFA(&out, "-- %uz %uc ", n, d);
	aLoop(a, 0,       ^(size_t nn){ (void)aA(&out, a[nn]+1); return  1; });
	aLoop(a, aL(a)-1, ^(size_t nn){ (void)aA(&out, a[nn]);   return -1; });
	aFree(&a);
  return aAppend((char***)NULL,
                 (intptr_t)aStr("Optional"),
                 (intptr_t)aStr("ifmmp hello jgnnq hello heo hllo ho o -- j r z -- 2 2 -- 10 10 ifmmpolleh")); }
#endif

int main(int argsnum, char**args) {
  aF("running tests ...\n");

  unittest(test1);
  unittest(test2);
  unittest(test4);
  unittest(test5);
  unittest(test6);
  unittest(test7);
  unittest(test8);
  #if __has_extension(blocks)
  unittest(test9);
  #endif
  unittest(test9_FUNC);
  unittest(test10);
  unittest(test11);
  unittest(test12);
  unittest(test13);

  //unittest(test14s); unittest(test14f);
  unittest(test14static);
  unittest(testSideEffects);
  unittest(testSideEffectsB);
  unittest(test_aError);
  #if __has_extension(blocks)
  unittest(testNULL);
  #endif
  unittest(testError);
  unittest(testZeroLen);
  unittest(testBigFit);
  unittest(testMem);
  unittest(testCmp);
  #if __has_extension(blocks)
  unittest(testOptional);
  #endif

  if(test_failed) { aF("tests failed\n"); exit(-1); }
  if(argsnum>1) { // remember test was successful
    FILE*outfile = fopen(args[1], "w");
    if(!outfile) { aF("couldn't find file %s",(intptr_t)args[2]); exit(-1); } }
  aF("\n... tests passed\n"); }
