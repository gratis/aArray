#define UNIQUE_NAME "linking"
#include "unittest.h"

#include "../build/aArray-forTests.h"
#include "../build/aArray-forTests.h"
#include "test_linking.h"
#include "test_linking2.h"

static int* a = NULL;

int test(void) {
	aAppend(&a,1);
	return aAt(a, 0); }

int main(int argsnum, char** args) {
	aF("running tests ...\n");
	
	if(test()!=1) test_failed = true;
	if(test2()!=3) test_failed = true;
	aFree(&a);

	if(test_failed) { aF("tests failed\n"); exit(-1); }
	if(argsnum>1) { // remember test was successful
		FILE* outfile = fopen(args[1], "w");
		if(!outfile) { aF("couldn't find file %s",args[2]); exit(-1); } }
	aF("\n... tests passed\n"); }
