#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include "kvec.h"
#include <vector>
#include <assert.h>
#define AARRAY_c
#include "../aArray.h"
int M = 100000;
void test_line(int len) {
	int i, j; clock_t t1 = 0, t2 = 0, d1, d2;

  std::vector<int64_t> array1;
 	//int64_t*array2 = NULL;
 	//aAlloc(&array2,24); aL2(array2, 0);
 	d1 = clock();
	for (i = 0; i < M; ++i)	for (j = 0; j < len; ++j) array1.push_back(j);
 	t1 += clock() - d1;
  	
 	//d2 = clock();
	//for (i = 0; i < M; ++i)	for (j = 0; j < len; ++j) aAppend(&array2, j);
 	//t2 += clock() - d2;

 	for(j = len - 3; len > 100 && j > 1 && j > len - 100; j--)
  	assert(array1[j]+1==array1[j+1]);

	printf("%.3f ", (float)t1/CLOCKS_PER_SEC);


	/*			t = clock();
	for (i = 0; i < M; ++i) { std::vector<int> array;
		array.reserve(len);
		for (j = 0; j < len; ++j) array[j] = j; }
		printf("C++ vector, preallocated: %.3f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);*/

	
	/*t = clock();
	for (i = 0; i < M; ++i) {
		int* array = aMulti((int**)NULL, 0, 0, len, 0, NULL);
		for (j = 0; j < len; ++j) aAt2(array, j, j);
		aFree(&array); }
	printf("aVector, aMulti: %.3f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);

	t = clock();
	int* array = NULL;
	for (i = 0; i < M; ++i) {
		if(!array) array = aMulti((int**)NULL, 0, 0, len, 0, NULL);
		for (j = 0; j < len; ++j) aAt2(array,j,j); }
	aFree(&array);
	printf("aVector, aMulti: %.3f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);

	t = clock();
	for (i = 0; i < M; ++i) {
		int* array = NULL;
		for (j = 0; j < len;) aAppendTINY(&array, j, ++j, ++j, ++j, ++j, ++j, ++j, ++j, ++j, ++j);
		aFree(&array); }
		printf("aVector, aAppendTINY: %.3f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);*/
	
	/*t = clock();
	for (i = 0; i < M; ++i) {
		int *array = (int*)malloc(len * sizeof(int));
		for (j = 0; j < len; ++j) array[j] = j; free(array); }
	//printf("C array, preallocated: %.3f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);
	printf("%.3f ", (float)(clock() - t) / CLOCKS_PER_SEC);
	
	t = clock();
	for (i = 0; i < M; ++i) {
		int *array = 0, max = 0; for (j = 0; j < len; ++j) {
			if (j == max) { max = !max? 1 : max << 1;
				array = (int*)realloc(array, sizeof(int)*max); }
			array[j] = j; } free(array); }
	//printf("C array, dynamic: %.3f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);
	printf("%.3f ", (float)(clock() - t) / CLOCKS_PER_SEC);
	
	t = clock();
	for (i = 0; i < M; ++i) {
		kvec_t(int) array; kv_init(array);
		kv_resize(int, array, len);
		for (j = 0; j < len; ++j) kv_a(int, array, j) = j; kv_destroy(array); }
	//printf("C vector, dynamic (kv_a): %.3f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);
	printf("%.3f\n", (float)(clock() - t) / CLOCKS_PER_SEC);*/
	
	/*t = clock(); for (i = 0; i < M; ++i) { kvec_t(int) array; kv_init(array);
		for (j = 0; j < len; ++j) kv_push(int, array, j);
		kv_destroy(array); }
		printf("C vector, dynamic (kv_push): %.3f sec\n", (float)(clock() - t) / CLOCKS_PER_SEC);*/

	fflush(stdout);
}

int main() {
	//test_line(200000);
	//exit(0);
	int len = 0;
	printf("#    stdvec  aAppend  malloc  realloc  kvec\n");
	while(len <= 10000000) {
		printf("%d ", len);
		test_line(len);
		printf("\n");
		len += 100; }


	return 0; }

