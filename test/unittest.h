#include "../build/aArray-forTests.h"

#include <setjmp.h>
#include <errno.h>

#if defined(__clang__)
  #define UNITTEST_nowarn_start \
    AARRAY_nowarn_pedantic_cpp_start \
    _Pragma("clang diagnostic ignored \"-Wimplicit-fallthrough\"") \
    _Pragma("clang diagnostic ignored \"-Wdisabled-macro-expansion\"") \
    _Pragma("clang diagnostic ignored \"-Wmissing-prototypes\"") \
    _Pragma("clang diagnostic ignored \"-Wcast-qual\"")
#elif defined(__GNUC__)
  #define UNITTEST_nowarn_start \
    AARRAY_nowarn_pedantic_cpp_start \
    _Pragma("GCC diagnostic ignored \"-Wignored-qualifiers\"")
  AARRAY_nowarn_start
#else
  #define UNITTEST_nowarn_start
#endif
#define UNITTEST_nowarn_end \
  AARRAY_nowarn_pedantic_cpp_end

UNITTEST_nowarn_start

/*AARRAY_nowarn_start*/
//#pragma gcc diagnostic ignored "-Wint-conversion"

char*FILEtoArray(FILE*file) {
	// all (uintptr_t) casts are just for c++
	if(!file) { aFE((uintptr_t)"file is NULL"); exit(-1); }
	fseek(file,0,SEEK_END);
	size_t file_size = (size_t)ftell(file);
	rewind(file);

	char *buffer = NULL;
	aMulti(&buffer, 0, 0, 0, file_size, (uintptr_t)NULL);
	size_t read_size = fread(buffer,sizeof(char),file_size,file);
	if(read_size!=file_size) { aFE((uintptr_t)"read file failure"); exit(-1); }
	//fclose(file);
	return buffer; }

#if !defined(__cplusplus)
typedef enum { false, true } bool;
#endif
static bool test_failed = false;

static char*out = NULL;
static FILE*outFILE = NULL;

void unittest(char**(*test)(void)) {
	errno = 0; if(!(outFILE = fopen("build/output" UNIQUE_NAME ".txt", "w+")))
							 // aF is abrev of aFmt
							 { aF((uintptr_t)"build/output" UNIQUE_NAME ".txt could not be created (errno %ii)", errno); exit(-1); }
	//int outFILE_lock = flock(fileno(outFILE)), LOCK_SH);
	
	char**result = test();
	if(!result) { aF((uintptr_t)"no result given"); exit(-1); }
	if(!out || !aL(out)) {
		aA(&result[0], '\0');
		aF((uintptr_t)"%s: no output given", (uintptr_t)(aL(result)? result[0] : "__")); exit(-1); }
	
	if(aL(result)!=2) { aF((uintptr_t)"test() result malformed"); exit(-1); }
	
	size_t n = aL(out);
	if(n!=aL(result[1])) {
		aF((uintptr_t)"%v%c: received length is %uz not %uz\n", (uintptr_t)result[0], n, aL(result[1]));
		goto error; }
	while(n) { if(out[n-1]!=result[1][n-1]) break; n--; }
	if(n) goto error;
	aFree(&result[0]); aFree(&result[1]); aFree(&result);
	aFree(&out); fclose(outFILE);
	return;
 error:
	test_failed = true;
	aF((uintptr_t)"received: %v%c\n"
		 "expected: %v%c\n\n", (uintptr_t)out, (uintptr_t)result[1]);
	aFree(&result[0]); aFree(&result[1]); aFree(&result);
	aFree(&out); fclose(outFILE); }

#if defined(__builtin_unreachable)
#define PORTABLE_unreachable() __builtin_unreachable()
#elif defined(_MSC_VER)
#define PORTABLE_unreachable() __assume(0)
#else
#define PORTABLE_unreachable() abort()
#endif

UNITTEST_nowarn_end

