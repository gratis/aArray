#define UNIQUE_NAME "special"
#include "unittest.h"

UNITTEST_nowarn_start

static jmp_buf expected_err;
__attribute((noreturn)) void aError_jmp(char*line, char*msg) {
  (void)line;
  aFA(&out, (uintptr_t)"Error at _: %s", (uintptr_t)msg);
  longjmp(expected_err, 1); }

void foo(std::function<void(char*)> func) {
  if(!func) {} // if the call-wrapper has wrapped a callable object
  typedef void*function_t(void*);
  function_t**ptr_ptr_fun = func.target<function_t*>();
  if(ptr_ptr_fun != nullptr) {
    function_t*ptr_fun = *ptr_ptr_fun;
    ptr_fun(nullptr); } }

void printInt(int aa) { aFA(&out, (uintptr_t)"<%ii>", aa); }
char**test1(void) {
  aError = &aError_jmp;
  int*volatile a = nullptr; int one = 1;
  aAppend(&a, 10, 11, 12);


  aLoop_LAMBDA(a, 0, [a](size_t aa) -> int{ a[aa] += 1; return 1; });
  //aMap2_LAMBDA(a, [one](int aa){ return aa+one; });
  aMap_LAMBDA(a, [one](void*aa){ *(int*)aa = (*(int*)aa)+one; });
  aMap_LAMBDA(a, [](void*_aa){ int*aa = (int*)_aa; *aa = (*aa)+1; });
  aMap_LAMBDA(a, [](void*aa){ aFA(&out, (uintptr_t)"-%ii-", *(uintptr_t*)aa); });

  if(!setjmp(expected_err)) aMap_LAMBDA(a, NULL);
  if(!setjmp(expected_err)) aFilter_LAMBDA(a, NULL);
  if(!setjmp(expected_err)) (void)aFold_LAMBDA(a, 0, NULL);
  if(!setjmp(expected_err)) aLoop_LAMBDA(a, 0, NULL);

  aFA(&out, (uintptr_t)"<%v %ii>", (uintptr_t)a);

  aFree(&a);
  aError = &AARRAY_aError;
  return aAppend((char***)NULL, (uintptr_t)aStr("test1 lambdas"),
                 (uintptr_t)aStr("-+13--+14--+15-"
                                "Error at _: parameter is NULL"
                                "Error at _: parameter is NULL"
                                "Error at _: parameter is NULL"
                                "Error at _: parameter is NULL"
                                "<+13 +14 +15>")); }

char**test2(void) {
  aError = &aError_jmp;
  int*volatile a = aAppend((int**)NULL, 0);
  aDelete(&a, 0, 1);

  // test higher order functions with [] and [a]
  aLoop_LAMBDA(a, 0, [a](size_t) { aFmtA(&out, "HI"); return 1; });
  aMap_LAMBDA(a, [](void*){ aFmtA(&out, "HI"); });
  aFilter_LAMBDA(a, [](int){ aFmtA(&out, "HI"); return true; });
  auto f = [](uint64_t, uint64_t){ aFmtA(&out, "HI"); return 0; };
  (void)aFold_LAMBDA(a, 0, f);

  aAppend(&a, 1);

  aLoop_LAMBDA(a, 0, [a](size_t) { aFmtA(&out, "BYE"); return 1; });
  aMap_LAMBDA(a, [](void*){ aFmtA(&out, "BYE"); });
  aFilter_LAMBDA(a, [](int){ aFmtA(&out, "BYE"); return true; });
  auto ff = [](uint64_t, uint64_t){ aFmtA(&out, "BYE"); return 0; };
  (void)aFold_LAMBDA(a, 0, ff);

  aFA(&out, (uintptr_t)"<%v %ii>", (uintptr_t)a);

  aFree(&a);
  aError = &AARRAY_aError;
  return aAppend((char***)NULL, (uintptr_t)aStr("test2 lambdas2"),
                 (uintptr_t)aStr("BYEBYEBYEBYE<+1>")); }

char**test3(void) {
  aError = &aError_jmp;
  int*a = aAppend((int**)NULL, 0, 1, 2, 3);
  int*b = aAppend((int**)NULL, 0, 1, 2, 3);
  int*c = aAppend((int**)NULL, 0, 1, 2);

  aFA(&out, (uintptr_t)"%ii ", !aCmp(a,b));
  aFA(&out, (uintptr_t)"-- %ii ", !aCmp(a,c));
  aFA(&out, (uintptr_t)"%ii ", !aCmp(c,b));
  aFree(&c);
  aFA(&out, (uintptr_t)"%ii ", !aCmp(a,c));
  aFA(&out, (uintptr_t)"%ii ", !aCmp(c,b));
  (void)aL2(b, 0);
  aFA(&out, (uintptr_t)"%ii ", !aCmp(a,b));
  // zero-length should be equal to NULL
  aFA(&out, (uintptr_t)"-- %ii ", !aCmp(c,b));
  aFA(&out, (uintptr_t)"%ii ", !aCmp(b,c));

  aFA(&out, (uintptr_t)"%ii ", !aCmp(a,a));
  aFA(&out, (uintptr_t)"%ii ", !aCmp(b,b));
  aFA(&out, (uintptr_t)"%ii ", !aCmp(c,c));


  char*aa = aStr("abc");
  char*bb = aStr("abc");
  char*cc = nullptr;
  aFA(&out, (uintptr_t)"%ii ", !aCmp(aa,bb));
  //
  aFA(&out, (uintptr_t)"-- %ii ", !aCmp(aa,cc));
  aFA(&out, (uintptr_t)"%ii ", !aCmp(cc,bb));

  (void)aZL2(bb,1);
  aFA(&out, (uintptr_t)"%ii ", !aCmp(aa,bb));
  (void)aZL2(aa,1);
  aFA(&out, (uintptr_t)"(%ii) ", !aCmp(bb,aa));
  (void)aZAt2(aa, 0, 'x');
  aFA(&out, (uintptr_t)"%ii ", !aCmp(bb,aa));
  aFA(&out, (uintptr_t)"%ii ", !aCmp(aa,cc));
  aFA(&out, (uintptr_t)"%ii -- ", !aCmp(cc,bb));

  if(setjmp(expected_err)) aFA(&out, (uintptr_t)" ---- ", !aCmp(bb,a));
  if(setjmp(expected_err)) aFA(&out, (uintptr_t)" ---- ", !aCmp(b,aa));

  (void)aZAt2(bb, 0, 'x');
  aFA(&out, (uintptr_t)"%ii ", aCmp(aa,bb,aa,bb));
  aFA(&out, (uintptr_t)"%ii ", aCmp(aa,bb,aa,bb,cc));

  aFree(&a); aFree(&b); aFree(&aa); aFree(&bb);
  aError = &AARRAY_aError;
  return aAppend((char***)NULL, (uintptr_t)aStr("test3 aCmp"),
                 (uintptr_t)aStr("+0 -- +1 +1 +1 +1 +1 -- +0 +0 +0 +0 +0 +0 "
                                 "-- +1 +1 +1 (+0) +1 +1 +1 -- +1 +0 ")); }

int64_t search_int(int a, int b) { return a-b; }
char**test4(void) {
  int*a = nullptr; aA(&a, 1, 5, 4, 3, 5, 6, 7); aSort(a);
  aFA(&out, "[%v%u]-", (uintptr_t)a);
  size_t i;
  aFA(&out, "0");
  if(aSearch(a, &i, 1)) aFA(&out, "1");
  if(aSearchF_FUNC(a, &i, 1, search_int)) aFA(&out, "2");
  if(aSearchF_LAMBDA(a, &i, 1, [](int c, int d){ return (int64_t)(c-d); })) aFA(&out, "3");
  #if __has_extension(blocks)
  if(aSearchF_BLOCK (a, &i, 6,  ^(int b, int c){ return (int64_t)(b-c); })) aFA(&out, "4");
  #else
  if(aSearchF_LAMBDA(a, &i, 1, [](int b, int c){ return (int64_t)(b-c); })) aFA(&out, "4");
  #endif
  if(aSearchF_LAMBDA(a, &i, 2, [](int c, int d){ return (int64_t)(c-d); })) aFA(&out, "--");
  else aFA(&out, "5");
  if(aSearchF_LAMBDA(a, &i, 7, [](int c, int d){ return (int64_t)(c-d); })) aFA(&out, "6");
  if(aSearchF_LAMBDA(a, &i, 8, [](int c, int d){ return (int64_t)(c-d); })) aFA(&out, "--");
  aFA(&out, "-%uz-",i);
	aSortF_LAMBDA(a, [](int c, int d){ return c > d; });
  aFA(&out, "[%v%u]", (uintptr_t)a);
	aFree(&a);
	char**b = nullptr; aA(&b, (uintptr_t)"aa", (uintptr_t)"bb", (uintptr_t)"cc", (uintptr_t)"dd");
	aSearchF_LAMBDA(b, &i, "cc", [](uint64_t c, uint64_t d) {
  	return (int64_t)strcmp((char*)c,(char*)d); });
	aFA(&out, "-%uz", i);
	aFree(&a); aFree(&b);
  return aAppend((char***)NULL, (uintptr_t)aStr("test4 aSearchF"),
                 (uintptr_t)aStr("[1345567]-0123456-7-[7655431]-2")); }

char**test5(void) {
  int*a = nullptr; aA(&a, 1, 5, 4, 3, 5, 6, 7); aSort(a);
  aFA(&out, "[%v%u]-", (uintptr_t)a);
  size_t i;
  aFA(&out, "0");
  if(aSearchP(a, &i, 1)) aFA(&out, "1");
  if(aSearchPF_FUNC(a, &i, 1, search_int)) aFA(&out, "2");
  if(aSearchPF_LAMBDA(a, &i, 1, [](int c, int d){ return c-d; })) aFA(&out, "3");
  #if __has_extension(blocks)
  if(aSearchPF_BLOCK (a, &i, 1,  ^(int b, int c){ return b-c; })) aFA(&out, "4");
  #else
  if(aSearchPF_LAMBDA(a, &i, 1, [](int b, int c){ return b-c; })) aFA(&out, "4");
  #endif
  if(aSearchPF_LAMBDA(a, &i, 2, [](int c, int d){ return c-d; })) aFA(&out, "--");
  else aFA(&out, "5");
  if(aSearchPF_LAMBDA(a, &i, 7, [](int c, int d){ return c-d; })) aFA(&out, "6");
  if(aSearchPF_LAMBDA(a, &i, 8, [](int c, int d){ return c-d; })) aFA(&out, "--");
  aFA(&out, "-%uz-",i);
	aSortF_LAMBDA(a, [](int c, int d){ return c > d; });
  aFA(&out, "[%v%u]", (uintptr_t)a);
	aFree(&a);
	char**b = nullptr; aA(&b, (uintptr_t)"aa", (uintptr_t)"bb", (uintptr_t)"cc", (uintptr_t)"dd");
	aSearchPF_LAMBDA(b, &i, "cc", [](uint64_t c, uint64_t d) {
  	return (uintptr_t)strcmp((char*)c,(char*)d); });
	aFA(&out, "-%uz", i);
	aFree(&a); aFree(&b);
  return aAppend((char***)NULL, (uintptr_t)aStr("test5 aSearchPF"),
                 (uintptr_t)aStr("[1345567]-0123456-7-[7655431]-2")); }

int main(int numargs, char**args) {
	aF((uintptr_t)"running tests ...\n");

  unittest(test1);
  unittest(test2);
  unittest(test3);
  unittest(test4);
  unittest(test5);

  if(test_failed) { aF((uintptr_t)"tests failed\n"); exit(-1); }
  if(numargs>1) { // remember test was successful
    FILE*outfile = fopen(args[1], "w");
    if(!outfile) { aF((uintptr_t)"couldn't find file %s", (uintptr_t)args[2]); exit(-1); } }
  aF((uintptr_t)"\n... tests passed\n"); }

UNITTEST_nowarn_end
