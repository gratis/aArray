#define UNIQUE_NAME "cpp"
#include "unittest.h"

UNITTEST_nowarn_start

char**test1(void) {
  int*t = nullptr;
  aAppend(&t, 10);
  aAppend(&t, 15);
  aAppend(&t, 20);
  aAppend(&t, 25);
  aFA(&out, (uintptr_t)"=== ");
  size_t testIndex = 6;
  int item_zero = 0, item_three = 3;
  while(testIndex--) {
    if(testIndex>aLength(t))
      aMulti(&t, 
      	aLength(t), 0, (testIndex-aLength(t)), 1, (uintptr_t)&item_zero,
      	1, 1, 1, 1, (uintptr_t)&item_three);
    else aReplace(&t, testIndex,0, (int)3); }
  aFmtA(&out, (uintptr_t)"%v %2ui ===", (uintptr_t)t);	
  aFree(&t);
  return aAppend((char***)NULL, (uintptr_t)aStr("test1 ints"),
      					 (uintptr_t)aStr("=== 11 1010 11 11 11 10100 11 11001 11 0 ===")); }
char**test2(void) {
  char*s = nullptr;
  aAppend(&s, 'a');
  aAppend(&s, 'a');
  aAppend(&s, 'a');
  s[2] = 'x';
  s[0] = 'y';
  ////aInsert(&s, 5, 'b');
  aMulti(&s,
      	 3, 0, 2, 1, (uintptr_t)std::move((char[]){0}),
      	 5, 0, 1, 1, (uintptr_t)std::move((char[]){'b'}));
  size_t n = (size_t)-1; while(++n < aLength(s))
  aFA(&out, (uintptr_t)"%c|", s[n]?s[n]:' ');
  aFree(&s);
  return aAppend((char***)NULL, (uintptr_t)aStr("test2 chars"), (uintptr_t)aStr("y|a|x| | |b|")); }
char**test4(void) {
  char*s = nullptr;
  aAppend(&s, 'a', 'b', 'c');
  aAppend(&s, 'd', 'e');
  aAppend(&s, 'd', 'e');
  (void)aAt2(s, aL(s)-5, 'f');
  aAppend(&s, 'a', 'b', 'c');
  aAppend(&s, 'd', 'e');
  aFA(&out,(uintptr_t)"%c%c%c%c",s[2],aAt(s,4),
         aAt(s,aL(s)-1),*aAtPtr(s,aL(s)-2));
  aAppend(&s, 'y', 'y', '\0');
  char*c = aAtPtr(s,aL(s)-4);
  *c = ' '; *(c+sizeof(char)) = 'm';
  aFA(&out, (uintptr_t)"%s%c", (uintptr_t)c, *aAtPtr(s, aL(s)-4));
  (void)aLength2(s,aL(s)-1);
  char*sheep = (char*)"sheep";
  aAppendArray(&s, 6, (uintptr_t)sheep);
  aFA(&out,(uintptr_t)"%s", (uintptr_t)aAtPtr(s,aL(s)-6)); aFree(&s);
  return aAppend((char***)NULL, (uintptr_t)aStr("test4 string ops"), (uintptr_t)aStr("feed my sheep")); }
//return "string ops\0" "feed my sheep\0"; }
char**test5(void) {
  char*s = nullptr;
  aAppendArray(&s, 4, (uintptr_t)"cdle");
  aReplace(&s, 1, 0, 'o', 'o', 'o', 'o', 'o');
  aAppendArray(&s, 11, (uintptr_t)" was a dear");
  aReplaceArray(&s, aLength(s)-7,0, 3, (uintptr_t)"n't");
  aAppend(&s, 'e','s','t');
  aDelete(&s, aLength(s)-3, 3);
  aAppend(&s, 'y');
  aWriteF(outFILE, s); aFree(&s);
  out = FILEtoArray(outFILE);
  return aAppend((char***)NULL, (uintptr_t)aStr("test5 arrays"), (uintptr_t)aStr("cooooodle wasn't a deary")); }
char**test6(void) {
  long*ss = nullptr;
  long ll = 5000;
  aAppendArray(&ss, 3, (uintptr_t)std::move((long[]){1,5,10}));
  aAppend(&ss,ll,2,3);
  aReplaceArray(&ss, 0,0, 1, (uintptr_t)std::move((long[]){0}));
  aReplaceArray(&ss, 0, 7, 1, (uintptr_t)std::move((long[]){200}));
  long item_one = 1;
  aMulti(&ss, aLength(ss), 0,
    3, 1, (uintptr_t)&item_one);
  aReplace(&ss,aLength(ss)-1,1,2);
  aFmtF(outFILE, (uintptr_t)"%v %2ul ", (uintptr_t)ss);
  aFree(&ss);
  char*sss = nullptr;
  aAppendArray(&sss, SIZE_MAX, (uintptr_t)"abcde happy joy Jesu!!\n!\n");
  aAppend(&sss, (char)50000);
  aDelete(&sss, aLength(sss)-1, 1);
  aWriteF(outFILE, sss); aFree(&sss);
  out = FILEtoArray(outFILE);
  return aAppend((char***)NULL, (uintptr_t)aStr("test6 arrays 2"), (uintptr_t)aStr("11001000 1 1 10 abcde happy joy Jesu!!\n!\n")); }
char**test7(void) {
  char*s = nullptr;
  aAppend(&s, 'c');
  aAppend(&s, 'c');
  aAppendArray(&s, SIZE_MAX, (uintptr_t)"ddeeff");
  aReplace(&s, 0,0,'a');
  aReplace(&s, 2,0,'a');
  aReplace(&s, 4,0,'a');
  char*t = nullptr; aAppendArray(&t, SIZE_MAX, (uintptr_t)"hello__"); aAppend(&t, '\0');
  aConcat(&s, s,t,s);
  aReplaceArray(&s, aLength(s)-2, 2, SIZE_MAX, (uintptr_t)"hello", 3, (uintptr_t)"hey");
  //aFree(&s); aAppendArray(&s, SIZE_MAX, "abcde");
  aDelete(&s,1,43);
  aWriteF(outFILE, s); aFree(&s); aFree(&t);
  out = FILEtoArray(outFILE);
  return aAppend((char***)NULL, (uintptr_t)aStr("test7 arrays 3"), (uintptr_t)aStr("ahey")); }


#include <math.h>
void bottles(size_t n) {
  char*song = nullptr;
  char number[23] = {0};
  snprintf(number, 23, "%zu", n);
 stanza: {
    char*wall = (char*)" bottles of beets on the wall. ";
    aReplaceArray(&song, 0, aLength(song),
                  SIZE_MAX, (uintptr_t)number, SIZE_MAX, (uintptr_t)wall, SIZE_MAX, (uintptr_t)number, 17, (uintptr_t)wall,
                  SIZE_MAX, (uintptr_t)".\nPass some round and glug them down.\n");
    snprintf(number, 23, "%zu", n = (size_t)sqrt(n));
    if(n > 1) {
      aWriteF(outFILE, (char*)aAppendArray(&song, SIZE_MAX, (uintptr_t)number, 30, (uintptr_t)wall, SIZE_MAX, (uintptr_t)"\n\n"));
      goto stanza; } }
  aWriteF(outFILE, (char*)aAppendArray(&song, SIZE_MAX, (uintptr_t)"How many bottles of beets?"));
  aFree(&song); }
char**test8(void) {
  bottles(31);
  out = FILEtoArray(outFILE);
  return aAppend((char***)NULL, (uintptr_t)aStr("bottles"), (uintptr_t)aStr("31 bottles of beets on the wall. 31 bottles of beets.\n"
"Pass some round and glug them down.\n"
"5 bottles of beets on the wall.\n"
"\n"
"5 bottles of beets on the wall. 5 bottles of beets.\n"
"Pass some round and glug them down.\n"
"2 bottles of beets on the wall.\n"
"\n"
"2 bottles of beets on the wall. 2 bottles of beets.\n"
"Pass some round and glug them down.\n"
"How many bottles of beets?")); }

#ifndef __has_feature
#define __has_feature(x) 0  // Compatibility with non-clang compilers
#endif
#ifndef __has_extension
#define __has_extension __has_feature // Compatibility with pre-3.0 compilers
#endif

#if __has_extension(blocks)
char**test9(void) {
  char*cs1 = aAppendArray((char**)NULL, SIZE_MAX, (uintptr_t)"happy");
  aMap(cs1, ^(char*c){ aFF(outFILE, (uintptr_t)"|%c|",(uintptr_t)*c); *c = (*c)+1; });
  aFF(outFILE, (uintptr_t)"--");
  aMap(cs1, ^(char*c){ aFF(outFILE, (uintptr_t)"|%c|",(uintptr_t)*c); });
  aFF(outFILE, (uintptr_t)"--");
  aLoop(cs1, aL(cs1)-1, ^(size_t n){
      (void)aAt2(cs1,n,aAt(cs1,n)-1);
      aFF(outFILE, (uintptr_t)"|%c|",(uintptr_t)cs1[n]); return -2; });
  aFree(&cs1);
  out = FILEtoArray(outFILE);
  return aAppend((char***)NULL, (uintptr_t)aStr("Loop/Map"), (uintptr_t)aStr("|h||a||p||p||y|--|i||b||q||q||z|--|y||p||h|")); }
#endif

void vm1(char*c, void*data) { (void)data; aFF(outFILE, (uintptr_t)"|%c|",*c); *c = (*c)+1; }
void vm2(char*c, void*data) { (void)data; aFF(outFILE, (uintptr_t)"|%c|",*c); }
static char*test9_cs1;
int vl1(size_t n, void*data) { (void)data;
  (void)aAt2(test9_cs1,n,aAt(test9_cs1,n)-1);
  aFF(outFILE, (uintptr_t)"|%c|",test9_cs1[n]); return -2; }
char**test9_FUNC(void) {
  char*cs1 = aAppendArray((char**)NULL, SIZE_MAX, (uintptr_t)"happy");
  aMap_FUNC(cs1, &vm1, NULL);
  aFF(outFILE, (uintptr_t)"--");
  aMap_FUNC(cs1, &vm2, NULL);
  aFF(outFILE, (uintptr_t)"--");
  test9_cs1 = cs1;
  aLoop_FUNC(cs1, aL(cs1)-1, &vl1, NULL);	
  out = FILEtoArray(outFILE);
  return aAppend((char***)NULL, (uintptr_t)aStr("Loop/Map"), (uintptr_t)aStr("|h||a||p||p||y|--|i||b||q||q||z|--|y||p||h|")); }

char**test10(void) {
  short*s = nullptr;
  aAppend(&s, 1, 2, 3, 4);
  aAppend(&s, aAt(s,0), aAt(s,1), aAt(s,2), aAt(s,3));
  aAppendArray(&s,
      				 1, (uintptr_t)aAtPtr(s, 0),
      				 1, (uintptr_t)aAtPtr(s, 1),
      				 1, (uintptr_t)aAtPtr(s, 2),
      				 1, (uintptr_t)aAtPtr(s, 3),
      				 1, (uintptr_t)aAtPtr(s, 4),
      				 1, (uintptr_t)aAtPtr(s, 5),
      				 1, (uintptr_t)aAtPtr(s, 6),
      				 1, (uintptr_t)aAtPtr(s, 7),
      				 16, (uintptr_t)aAtPtr(s, 0));
  aFmtA(&out, (uintptr_t)"%v %us ", (uintptr_t)s); aFree(&s);
  return aAppend((char***)NULL, (uintptr_t)aStr("aAPtr"), (uintptr_t)aStr("1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 1 2 3 4 ")); }

char**test11(void) {
  char*cs = aAppend((char**)NULL, 'a',' ','m','a','n','!');
  aReplaceArray(&cs, aLength(cs)-4, 3, SIZE_MAX, (uintptr_t)"saviour");
  aReplaceArray(&cs, 2,0, SIZE_MAX, (uintptr_t)"wonderful ", 10, (uintptr_t)aAtPtr(cs, 2));
  aWriteF(outFILE, cs);
  aFree(&cs);
  out = FILEtoArray(outFILE);
  return aAppend((char***)NULL, (uintptr_t)aStr("saviour"), (uintptr_t)aStr("a wonderful wonderful saviour!")); }

char**test12(void) {
  int*s = nullptr;
  aAppend(&s, 80,1080,280);
  //aAppend(&s, "a");
  aDelete(&s,1,1);
  aFmtA(&out, (uintptr_t)"%v %ui", (uintptr_t)s);
 	aFree(&s);
  return aAppend((char***)NULL, (uintptr_t)aStr("errors"), (uintptr_t)aStr("80 280")); }

char**test13(void) {
  short*s = nullptr;
  aAppend(&s,1);
  //aInsert(&s,3, 3);
  aMulti(&s,
      	 1, 0, 2, 1, (uintptr_t)std::move((short[]){0}),
      	 3, 0, 1, 1, (uintptr_t)std::move((short[]){3}));
  aReplace(&s,aLength(s)-1,0, 2);
  //aInsert(&s,aLength(s)-10, 4);
  //aInsert(&s,-11, 3);
  aMulti(&s,
      	 0, 0, 1, 1, (uintptr_t)std::move((short[]){4}),
      	 1, 0, 2, 2, (uintptr_t)std::move((short[]){0,4}),
      	 6 ,0, 0, 0, (uintptr_t)s);
  aFmtA(&out, (uintptr_t)"%v %us ", (uintptr_t)s);
  aFree(&s);
  return aAppend((char***)NULL, (uintptr_t)aStr("negative pos"), (uintptr_t)aStr("4 0 4 0 4 1 0 0 2 3 ")); }

char**test14static(void) {
  int*s = aArray((int*)malloc(200), 200, false); (void)aL2(s, 0);
  aAppend_STATIC(&s, 0, 1, 2);
  aReplaceArray_STATIC(&s, 1, 2, 2, (uintptr_t)std::move((int[]){10, 11, 12, 13, 14, 15}));
  aFA(&out, (uintptr_t)">>");
  //aRemove_STATIC(&s,0,1);
  aFmtA(&out, (uintptr_t)"%v %16ui ", (uintptr_t)s);
  free(aMem_STATIC(s));
  return aAppend((char***)NULL, (uintptr_t)aStr("test14static STATIC"), (uintptr_t)aStr(">>0 a b ")); }

char**testSideEffects(void) {
  int*s = nullptr;
  aA(&s,10);
  int index = 0, amount = 1, value = 20;
  aR(&s,(size_t)index++,(size_t)amount++,value++);
  aR(&s,0,0,10);
  aR(&s,(size_t)--index,(size_t)--amount,--value);
  aFmtA(&out, (uintptr_t)"%v %ui ", (uintptr_t)s);
  aFree(&s);
  return aAppend((char***)NULL, (uintptr_t)aStr("testSideEffects"), (uintptr_t)aStr("20 20 ")); }

char**testSideEffectsB(void) {
  int*ss[] = {nullptr,nullptr,nullptr,nullptr};
  aA(&ss[0],10);
  aA(&ss[1],20, 21, 22);

  int index = 0;
  aR(&ss[index++], 1,0, 11);
  index = 0;
  aA(&ss[index++], 12);
  aFmtA(&out, (uintptr_t)"%v %ui ", (uintptr_t)ss[0]); aFA(&out, (uintptr_t)"| "); aFmtA(&out, (uintptr_t)"%v %ui ", (uintptr_t)ss[1]);

  index = 0;
  aFA(&out, (uintptr_t)"-%ui- ", aLength2(ss[index++], 1));
  aFmtA(&out, (uintptr_t)"%v %ui ", (uintptr_t)ss[0]);

  index = 0;
  aFA(&out, (uintptr_t)"\n>> %ui ", aAt(ss[index++],0));
  aFA(&out, (uintptr_t)"%ui ",    aAt2(ss[--index],0,5));
  aFA(&out, (uintptr_t)"%ui",     aAt(ss[index++],0));

  aFree(&ss[0]); aFree(&ss[1]);
  return aAppend((char***)NULL, (uintptr_t)aStr("testSideEffectsB"), (uintptr_t)aStr("10 11 12 | 20 21 22 -10- 10 \n>> 10 5 5")); }

static jmp_buf array_err;
__attribute((noreturn)) void aError_jmp(char*line, char*msg) {
  (void)line; // suppress unused-parameter warning
  aFA(&out, (uintptr_t)"Error file:line, %s\n", (uintptr_t)msg);
  longjmp(array_err, 1); }

char**test_aError(void) {
  aFA(&out, (uintptr_t)"Lets play guess the array length\n");
  // Guess too high, and an "out of bounds" exception will lose you the game
  char*volatile array = nullptr;
  //srand(time(NULL));
  //aInsert(&array, rand() % 100, 0);
  aMulti(&array, 0, 0,
      					//(rand() % 100)
      	 52, 1, (uintptr_t)std::move((char[]){0}));

  void(*aError_temp)(char*, char*) = aError;
  if(setjmp(array_err)) {
    aFA(&out, (uintptr_t)"Oops, too far! Goodbye\n");
    aError = aError_temp;
    aFree(&array);
    return aAppend((char***)NULL, (uintptr_t)aStr("test_aError"), (uintptr_t)aStr("Lets play guess the array length\nGuess a length: Error file:line, out of bounds (length=52 but pos=105)\nOops, too far! Goodbye\n")); }
  aError = aError_jmp;
  
  //size_t guess = 102; //// 101 with aReplaceWithN
  size_t guess = 100; int input_err;
  while(guess != aLength(array)) {
    aFA(&out, (uintptr_t)"Guess a length: ");
    //input_err = scanf("%zu", &guess);
    input_err = 1; guess = 105;
    if (input_err == 0 || input_err == EOF) {
      aFA(&out, (uintptr_t)"Oh dear, I don't understand what you wrote!\n");
      continue; }
    (void)aAt(array, guess);
    aFA(&out, (uintptr_t)"The array might be longer...\n"); } PORTABLE_unreachable(); }

char**test_aError_MsgLen(void) {
  char buf[200] = {0};
  snprintf(buf, 200, "removal is out of bounds (length=%zu but pos=%zu removal=%zu)",
      		(size_t)-1, (size_t)-1, (size_t)-1);
  aFA(&out, (uintptr_t)"strlen==%uz, %ui",strlen(buf)+1, AARRAY_aError_MsgLen);
  return aAppend((char***)NULL, (uintptr_t)aStr("test_aError_MsgLen"), (uintptr_t)aStr("strlen==113, 113")); }

__attribute((noreturn)) void aError_jmpB(char*line, char*msg) {
  (void)line; // suppress unused-parameter warning
  aFA(&out, (uintptr_t)"%s", (uintptr_t)msg);
  longjmp(array_err, 1); }

char**testError(void) {
  void(*aError_temp)(char*, char*) = aError;
  aError = aError_jmpB;
  //aFA(&out, "1...");
  volatile int errnum = 0;
  int*volatile static_check = aArray_STATIC((int*)malloc(200), sizeof(size_t)*2+sizeof(int)*2, false);
  (void)aL2(static_check, 0);
  int*volatile check = nullptr;
  if(setjmp(array_err)) ++errnum;
  switch(errnum) {
  case 0:
    aAppend_STATIC(&static_check, 1);
    aAppend(&check, 1);
    // aLoop no longer to give error
    //aLoop(static_check, 1, ^(size_t n){ (void)n; return 1; });
    errnum++;
    // fall through
  case 1:
    aAppendArray(&check, 0, (uintptr_t)NULL);
    aAppendArray(&check, SIZE_MAX, (uintptr_t)NULL, 1, (uintptr_t)NULL);
    // fall through
  case 2: aReplace(&check, 0, 500, 2);
    // fall through
  case 3:
	  (void)aLength2(check, 2);
    // fall through
  case 4:
    (void)aAt(check, 5);
    // fall through
  case 5: aMulti(&check, 0, 0, 1, 2);
    // fall through
  case 6: aAppendArray(&check, 0);
    // fall through
  case 7: aAppend_STATIC(&static_check, 1, 2);
    // fall through
  case 8:
    free(aMem_STATIC(static_check));
    aFree(&check);
    aError = aError_temp;
    return aAppend((char***)NULL, (uintptr_t)aStr("testError"), //"out of bounds (length=1 but pos=1)"
      (uintptr_t)aStr("array is NULL (array no=1)"
      "removal is out of bounds (length=1 but pos=0 removal=500)"
      "out of bounds (length=1 but pos=2)"
      "out of bounds (length=1 but pos=5)"
      "wrong arg count (args=5 but should be 1 + multiple of 5)"
      "wrong arg count (args=2 but should be 1 + multiple of 2)"
      "out of capacity (size=24 but require=28)")); }
  aFA(&out, "--byebye--");
  return aAppend((char***)NULL, (uintptr_t)aStr("testError"), (uintptr_t)aStr("")); }


int  setTo1(int*i) { (void)i; return 1; }
void setTo1Ptr(int*i) { (void)i; *i = 1; }

char**testNULL(void) {
  // test for errors with a NULL array
  int*v_test = nullptr;
  aIndexOf(v_test, 1);
  //aAt(v_test, 0);
  //aAt2(v_test, 6, 16);
  aMap_FUNC(v_test, setTo1, NULL);
  aMap_FUNC(v_test, setTo1Ptr, NULL);
  //aLoop(v_test, 0, ^(size_t n){ return 1; });
  aFmtA(&out, (uintptr_t)"%v %2ui", (uintptr_t)v_test);
  aWriteF(outFILE, v_test);
  aFree(&v_test);
  aFF(outFILE, (uintptr_t)"vl=%uz", aLength(v_test));
  (void)aLength2(v_test, 0);
  // test aIndexOf integer overflow check
  size_t length = (size_t)-7; int jump = 200;
  char*out2 = FILEtoArray(outFILE);
  aConcat(&out, out2); aFree(&out2);
  aFA(&out, (uintptr_t)"--%uz  %uz--",length-(length%jump)+(length%jump?jump:0), length);
  return aAppend((char***)NULL, (uintptr_t)aStr("testNULL"), (uintptr_t)aStr("vl=0--184  18446744073709551609--")); }

char**testZeroLen(void) {
  int*t = nullptr;
  aA(&t, 1, 2, 3, 4);
  aD(&t, 0, 4);
  aA(&t, 5);
  aFmtA(&out, (uintptr_t)"%v %u32 ", (uintptr_t)t);
  aD(&t, 0, 1);
  aA(&t, 6, 7);
  aFmtA(&out, (uintptr_t)"%v %u32 ", (uintptr_t)t);
  aFree(&t);
  return aAppend((char***)NULL, (uintptr_t)aStr("testZeroLen"), (uintptr_t)aStr("5 6 7 ")); }

#if __has_extension(blocks)
bool isEven(int num, void*d) { (void)d; return !(num%2); }
char toChar(int item) { return (char)item; }
int add_function(int base, int item) { return base+item; }
char**testExFilterFold(void) {
  int*n1 = aAppend((int**)NULL, 4,5,6,7,8);
  aFilter_FUNC(n1, isEven, NULL);  // n2 = [4,6,8]
  aFmtA(&out, "\n%v %ui", (uintptr_t)n1);
  aFilter_BLOCK(n1, ^(int item){ return item > 5; });  // n3 = [6,8]
  aFmtA(&out, "\n%v %ui", (uintptr_t)n1);
  aFilter_LAMBDA(n1, [](int item){ return item != 8; });  // n4 = [6]
  aFmtA(&out, "\n%v %ui", (uintptr_t)n1);
  aFree(&n1);

  int numbersArray[] = {1,2,3,4,5,6,7,8,9,10};
  int*numbers = aAppendArray((int**)NULL, 10, (uintptr_t)numbersArray);
  aFmtA(&out, "\nSum     = %u64", (uintptr_t)aFold_FUNC(numbers, 0, add_function, NULL));
  aFmtA(&out, "\nProduct = %u64", (uintptr_t)aFold_BLOCK(numbers, (size_t)1, ^(size_t base, int item){ return base*item; }));

  auto f = [](bool base, int item) { return base&&item; };
  aFmtA(&out, "\nAllTrue = %u8",  (uintptr_t)aFold_LAMBDA(numbers, true, f));

  char*baseB = nullptr;
  aFmtA(&out, "\nString  = %v%c", (uintptr_t)*aFold_BLOCK(numbers, &baseB,
    ^(char**baseBPtr, int item){ (void)aAppend((char**)baseBPtr, (item+64)); return baseBPtr; }));

  aFree(&baseB);
  aFree(&numbers);
  return aAppend((char***)NULL, (uintptr_t)aStr("ex24"), (uintptr_t)aStr(
                 "\n4 6 8\n6 8\n6"
                 "\nSum     = 55"
                 "\nProduct = 3628800"
                 "\nAllTrue = 1"
                 "\nString  = ABCDEFGHIJ")); }
#endif

void plus1(char*c) { *c += 1; }
void plusN(char*c, void*d) { *c += *(char*)d; }
int  is_l(char c) { return c != 'l'; }
int  is_N(char c, void*d) { return c != *(char*)d; }
size_t sumChar(size_t s, char c) { return s + (size_t)c; }
char      CharN(size_t s, char c, void*d) { (void)s; (void)c; return *(char*)d; }
size_t sumCharN(size_t s, char c, void*d) { return s + (size_t)c + (size_t)*(char*)d; }
static size_t n = 0;
int loop1(size_t nn)         { n += nn;  return 1; }
int loopN(size_t nn, void*d) { *(char*)d += nn; return 1; }
char**testOptional(void) {
	char*a = aStr("hello");
	aMap_FUNC(a, plus1);
	char d = -1;
	aFA(&out, "%v%c ", (uintptr_t)a);
	aMap_FUNC(a, plusN, &d);
	aFA(&out, "%v%c ", (uintptr_t)a);
	aMap_LAMBDA(a, [](void*c){ *(char*)c += 2; });
	aFA(&out, "%v%c ", (uintptr_t)a);
	d = -2;
	aMap_LAMBDA(a, [](void*c, void*dd){ *(char*)c += *(char*)dd; }, &d);
	aFA(&out, "%v%c ", (uintptr_t)a);
	aFilter_FUNC(a, is_l);
	aFA(&out, "%v%c ", (uintptr_t)a);
	aFree(&a); a = aStr("hello");
	d = 'e';
	aFilter_FUNC(a, is_N, &d);
	aFA(&out, "%v%c ", (uintptr_t)a);
	aFilter_LAMBDA(a, [](char c){ return c != 'l'; });
	aFA(&out, "%v%c ", (uintptr_t)a);
	d = 'h';
	aFilter_LAMBDA(a, [](char c, void*dd){ return c != *(char*)dd; }, &d);
	aFA(&out, "%v%c -- ", (uintptr_t)a);
	aFree(&a); a = aStr("hello");
	aFA(&out, "%c ", aFold_FUNC(a, 0, sumChar)/aL(a));
	d = 'z';
	aFA(&out, "%c ", aFold_FUNC(a, 0, sumCharN, &d)/(aL(a)*2)); // avg of 'hello' moved half way to 'z'
	aFA(&out, "%c -- ", aFold_FUNC(a, 0, CharN, &d));
	// vowel count
	auto l1 = [](size_t b, char c)->size_t{ return b + (c=='a'||c=='e'||c=='i'||c=='o'||c=='u'); };
	size_t temp1;
	temp1 = aFold_LAMBDA(a, (size_t)0, l1);
	aFA(&out, "%uz ", temp1);
	// char count
	d = 'l';
	auto l2 = [](size_t b, char c, void*dd)->size_t{ return b + (c==*(char*)dd); };
	temp1 = aFold_LAMBDA(a, (size_t)0, l2, &d);
	aFA(&out, "%uz ", temp1);
	aLoop_FUNC(a, 0, loop1);
	d = 0;
	aLoop_FUNC(a, 0, loopN, &d);
	aFA(&out, "-- %uz %uc ", n, d);
	aLoop_LAMBDA(a, 0,       [&a](size_t nn){ (void)aA(&out, a[nn]+1); return  1; });
	aLoop_LAMBDA(a, aL(a)-1, [&a](size_t nn){ (void)aA(&out, a[nn]);   return -1; });
	aFree(&a);
  return aAppend((char***)NULL,
                 (intptr_t)aStr("Optional"),
                 (intptr_t)aStr("ifmmp hello jgnnq hello heo hllo ho o -- j r z -- 2 2 -- 10 10 ifmmpolleh")); }

int main(int argsnum, char**args) {
  aF((uintptr_t)"running tests ...\n");

  unittest(test1);
  unittest(test2);
  unittest(test4);
  unittest(test5);
  unittest(test6);
  unittest(test7);
  unittest(test8);

  #if __has_extension(blocks)
  unittest(test9);
  #endif
  unittest(test9_FUNC);
  unittest(test10);
  unittest(test11);
  unittest(test12);
  unittest(test13);

  //unittest(test14s); unittest(test14f);
  unittest(test14static);
  unittest(testSideEffects);
  unittest(testSideEffectsB);
  unittest(test_aError);
  #if __has_extension(blocks)
  unittest(testNULL);
  #endif
  unittest(test_aError_MsgLen);
  unittest(testError);
  unittest(testZeroLen);

  #if __has_extension(blocks)
  unittest(testExFilterFold);
  #endif

  unittest(testOptional);

  if(test_failed) { aF((uintptr_t)"tests failed\n"); exit(-1); }
  if(argsnum>1) { // remember test was successful
    FILE*outfile = fopen(args[1], "w");
    if(!outfile) { aF((uintptr_t)"couldn't find file %s", (uintptr_t)args[2]); exit(-1); } }
  aF((uintptr_t)"\n... tests passed\n"); }

UNITTEST_nowarn_end
