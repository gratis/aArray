#define UNIQUE_NAME "examples"
#include "unittest.h"

static jmp_buf expected_err;
__attribute((noreturn)) void aError_jmp(char*line, char*msg) {
  (void)line;
  aFA(&out, "Error at _: %s", msg);
  longjmp(expected_err, 1); }



void capitalize_char(char*c) { *c = (*c)>=97 && (*c)<=122? (*c)-32 : (*c); }
char**ex1(void) {
  char*arr = NULL;

  aAppend(&arr, 'I', ' ', 'w', 'a', 'n', 't');
  aMap_FUNC(arr, capitalize_char, NULL); // would capitialize each char

  arr[3] = 'O';              // standard array indexing
  (void)aAt2(arr, 5, ' ');   // same as arr[5] = ' ', but overflow safe

  // Append a null terminated array, and 5 items from another array
  aAppendArray(&arr, SIZE_MAX, "chunky ", 5, "bacon, francis");
  // At position 0 replace 1 item with a null terminated array
  aReplaceArray(&arr, 0, 1, SIZE_MAX, "Everyone");

  aWriteF(outFILE, arr); // prints "Everyone WON chunky bacon"
  aFree(&arr);
  out = FILEtoArray(outFILE);
  return aAppend((char***)NULL, aStr("ex1"), aStr("Everyone WON chunky bacon")); }

char**ex2(void) {
  int*volatile arrSTATIC = malloc(200); arrSTATIC = aArray(arrSTATIC, sizeof(size_t)*2+sizeof(int)*4, false);
  (void)aL2(arrSTATIC, 0);
  // aError exceptions are caught here
  aError = &aError_jmp;
  if(setjmp(expected_err)) {
    aError = &AARRAY_aError;
    aFA(&out, ".");
    free((void*)aMem_STATIC(arrSTATIC));
    return aAppend((char***)NULL, aStr("ex2"), aStr("Error at _: out of capacity (size=32 but require=48)."));	}
  int*replacement = (int[]){5, 6, 7};
  /**/
  aAppend_STATIC(&arrSTATIC, 1, 2, 3, 4);
  aReplaceArray_STATIC(&arrSTATIC, 0, 1, 5, replacement); // throws "out of capacity"
  PORTABLE_unreachable(); }

/*char**ex3(void) {
  int*arrSTATIC = aAppend_STATIC((int**)NULL, 1, 2, 3, 4);
  aReplaceArrayPAGE(&arrSTATIC, 0, 1, 5, 6); // undetected memory corruption!!!
  return aAppend((char***)NULL, aStr("ex3"), aStr("")); }

char**ex4(void) {
  int*arrTINY = aAppend((int**)NULL, 1);
  ////
  int*arrPAGE = aConcatPAGE((int**)NULL, arrTINY);
  aFree(&arrTINY);
  ////
  aFA(&out, "%v %ii", arrPAGE);
  aFree(&arrPAGE);
  return aAppend((char***)NULL, aStr("ex4"), aStr("+1")); }*/

char**ex5(void) {
  int *arr = NULL;
  aAppend(&arr, 1, 2);
  aAppend(&arr, 99, 100);
  aFmtA(&out, "{%v, %i}", arr); // print "{+1, +2, +99, +100}"
  aFree(&arr);
  return aAppend((char***)NULL, aStr("ex5"), aStr("{+1, +2, +99, +100}")); }

char**ex6(void) {
  long long*volatile holidayQuota = NULL;
  aError = &aError_jmp;
  if(setjmp(expected_err)) {
    aError = &AARRAY_aError;
    aFA(&out, ".");
    aFree((long long**)(uintptr_t)&holidayQuota);
    return aAppend((char***)NULL, aStr("ex6"), aStr("[1 361 2 1]"
                                                    "Error at _: out of bounds (length=4 but pos=7)."));	}
  aAppend((long long**)(uintptr_t)&holidayQuota, 1, 2, 1);
 
  aReplace((long long**)(uintptr_t)&holidayQuota, 1,0, 361); // a proper holiday
  aFmtA(&out, "[%v %ull]", holidayQuota); // "[1 361 2 1]"

  aReplace((long long**)(uintptr_t)&holidayQuota, 7,0, 368); // throws exception: out of bounds (length=5 but pos=7)
  aFree((long long**)(uintptr_t)&holidayQuota);
  PORTABLE_unreachable(); }

char**ex7(void) {
  unsigned int *joy = aAppend((unsigned int**)NULL, 10, 5, 0, -5, -20);
  aFmtA(&out, "%v %u", aReplace(&joy, 2, 3, 10, 20)); // prints 10 5 10 20
  aFree(&joy);
  return aAppend((char***)NULL, aStr("ex7"), aStr("10 5 10 20")); }

char**ex8(void) {
  int*ages = aAppendArray((int**)NULL,
                           3, (int[]){0, 5, 10},
                           2, (int[]){12, 14}); // (int*){0, 5, 10, 12, 14}
  aFA(&out, "%v, %ii", ages);
  aFree(&ages);
  return aAppend((char***)NULL, aStr("ex8"), aStr("+0, +5, +10, +12, +14")); }

char **ex8_2(void) {
  char *hi = aStr("hello world");
  aR(&hi, 0, 1, 'j');
  /**/
  aAppend(&hi, 0);
  aFA(&out, "%s", hi);
  aFree(&hi);
 	return aAppend((char***)NULL, aStr("ex8_2"), aStr("jello world")); }

char**ex9(void) {
  char*iu = aAppend((char**)NULL, 'i', 'u');
  char*i_love_u = aReplaceArray(&iu, 1,0, SIZE_MAX, " ♥ ");
  // iu and i_love_u now point to the same place
  aFA(&out, "%v%c", iu);
  aFree(&i_love_u);
  return aAppend((char***)NULL, aStr("ex9"), aStr("i ♥ u")); }

char**ex10(void) {
  int*nums = NULL;
  aAppend(&nums, 1, 2, 3, 4, 100);
  aReplaceArray(&nums, 2, 2,
                1, nums,
                2, aAtPtr(nums, 1)); // {1, 2, 1, 2, 1, 100}
  aFA(&out, "%v %ii", nums);
  aFree(&nums);
  return aAppend((char***)NULL, aStr("ex10"), aStr("+1 +2 +1 +2 +1 +100")); }

char**ex11(void) {
  char*example = aStr("A ");
  char*silly = aStr("silly ");

  aConcat(&example, silly, silly, example, example);
  aConcat(&example, example);
  aConcat(&out, example);
  aFree(&example);
  aFree(&silly);
  return aAppend((char***)NULL, aStr("ex11"), aStr("A silly silly A A A silly silly A A ")); }

char**ex12(void) {
  long *medalsWonBy_aArray = aAppend((long**)NULL, 15, 26, (long)-1);
  aDelete(&medalsWonBy_aArray, 0, 3);
  aFA(&out, "-%v %il-", medalsWonBy_aArray);
  aFree(&medalsWonBy_aArray);
  return aAppend((char***)NULL, aStr("ex12"), aStr("--")); }

char**ex13(void) {
  int8_t*data = NULL;
  int16_t*data2 = NULL;
  /**/
  // length=200, every item is 0
  aMulti(&data, 0, 0, 10, 1, (int8_t[]){1});

  // length=200, every item is uninitialized
  aMulti(&data2, 0, 0, 0, 10, NULL);
  /**/
  aFA(&out, "%v %u8", data);
  aFA(&out, "-%uz %uz-", aL(data), aL(data2));
  (void)aLength2(data2, 0);
  aFA(&out, " -%uz-", aL(data2));
  aFree(&data);
  aFree(&data2);
  return aAppend((char***)NULL, aStr("ex13"), aStr("1 1 1 1 1 1 1 1 1 1-10 10- -0-")); }

char**ex14(void) {
  // Arbitrary array updates reduced to single call
  unsigned int *va = NULL;
  aAppend(&va, 1, 2, 3);
  aReplace(&va, 0,0, -2, -1, 0);
  aReplaceArray(&va, 1, 3, 1, (unsigned int[]){5});
  aDelete(&va, aLength(va)-1, 1);
  // va now {-2, 5, 2}

  unsigned int *vb = NULL;
  aMulti(&vb,
    	   0, 0, 1, 3, (unsigned int[]){1, 2, 3},
    	   0, 0, 1, 3, (unsigned int[]){-2, -1, 0},
    	   1, 3, 1, 1, (unsigned int[]){5},
    	   3, 1, 0, 0, NULL);
  // vb now the same as va, but done in one realloc
  aFA(&out, "%v %i, %v %i", va, vb);
  aFree(&va); aFree(&vb);
  return aAppend((char***)NULL, aStr("ex14"), aStr("-2 +5 +2, -2 +5 +2")); }

char**ex14a(void) {
	float*init = NULL;
	aMem(init);
	(void)aLength2(init, 0);
	aFA(&out, "%uz ", aL(init));

	float*reduce = NULL;
	aAppend(&reduce, 1.0f, 2.2f, 3.4f);
	aMem(reduce);
	aFA(&out, "%uz ", aL(reduce));
  aFree(&init); aFree(&reduce);
  return aAppend((char***)NULL, aStr("ex14a"), aStr("0 3 ")); }

char**ex14b(void) {
	// aMem has `fake` examples
	return NULL; }

char**ex14c(void) {
	char*x = aArray((char*)malloc(200), 200, true);
	if(aL(x)==200-sizeof(size_t)*2) aFA(&out, "%uz ", aL(x)); // prints 200-(sizeof(size_t)*2)
	if(-1000<(int)aAt(x, aL(x)-1)) aFA(&out, "x "); // prints last uninitialized byte
	free(aMem(x));

	x = aArray_STATIC((char*)malloc(1000), sizeof(size_t)*2 + 3, false);
	(void)aL2(x, 0);
	aAppend_STATIC(&x, 1, 2, 3); // reached maximum
	x = aArray_STATIC(aMem(x), sizeof(size_t)*2 + 4, false);
	// length is still 3, but capacity is 4
	if(aL(x)==3) aAppend_STATIC(&x, 4); // [1, 2, 3, 4]
	aFA(&out, "%v%uc", x);
	free(aMem(x));
  return aAppend((char***)NULL, aStr("ex14c"), aStr("184 x 1234")); }

char**ex15(void) {
  long long*myArr = aAppend((long long**)NULL, 'x');
  aFree(&myArr); // myArr is now NULL
  aFA(&out, "%up", myArr);
  return aAppend((char***)NULL, aStr("ex15"), aStr("0")); }

char**ex16(void) {
  int*fib = NULL; aAppend(&fib, 1, 1, 2, 3, 5);
  aFA(&out, "Prev fib = %uz", aLength2(fib, aLength(fib)-1));
  // fib length reduced by one, and 3 printed

  aFA(&out, "\nlen = %uz", aLength(fib)); // 4
  (void)aLength2(fib, 2);
  aFA(&out, "\nlen = %uz", aLength(fib)); // 2
  aFree(&fib);
  
  fib = NULL; aAppend(&fib, 1, 1, 2, 3, 12);
  (void)aZLength2(fib, 1);
  aFA(&out, "\n\nlast item = %u", aZLength2(fib, 1)); // 'last item = 2'
  aFree(&fib);  
  return aAppend((char***)NULL, aStr("ex16"), aStr("Prev fib = 3\n"
                                                   "len = 4\n"
                                                   "len = 2\n\n"
                                                   "last item = 2")); }

char**ex16a(void) {
	// sizeof(size_t) is where aArray stores capacity+length
	size_t size = 15*2 + 2*sizeof(size_t);
	short* blah = aArray((short*)malloc(size), size, true);
	aFA(&out, "%uz ", aCapacity(blah)); // 15
	(void)aL2(blah, 3);
	aFA(&out, "%uz ", aCapacity(blah)); // 15
	aFree(&blah);

	blah = aArray_STATIC((short*)malloc(size), size, true);
	aFA(&out, "%uz ", aCapacity_STATIC(blah)); // 15
	(void)aL2(blah, 3);
	aFA(&out, "%uz ", aCapacity_STATIC(blah)); // 15
	aFree_STATIC(&blah);

	size = 15*2 + sizeof(size_t); // only stores length
	blah = aArray_NOCAPACITY((short*)malloc(size), size, true);
	aFA(&out, "%uz ", aL(blah)); // 8
	aFA(&out, "%uz ", aCapacity_NOCAPACITY(blah)); // 8
	(void)aL2(blah, 3);
	aFA(&out, "%uz ", aL(blah)); // 3
	aFA(&out, "%uz ", aCapacity_NOCAPACITY(blah)); // 4
	aFree_NOCAPACITY(&blah);
  return aAppend((char***)NULL, aStr("ex16a"), aStr("15 15 15 15 8 8 3 4 ")); }

char**ex17(void) {
  char*equal = aAppend((char**)NULL, 'a', 'b');
  // NOTE: cast not needed if your compiler has __typeof or __decltype
  if(equal[0] == (char)aAt(equal, 0)) aFA(&out, "equal"); // "equal"
  aFree(&equal);
  return aAppend((char***)NULL, aStr("ex17"), aStr("equal")); }

char**ex18(void) {
  char*equal = aAppend((char**)NULL, 'a', 'b');
  // NOTE: cast not needed if your compiler has __typeof or __decltype
  *(char*)aAtPtr(equal, 0) = 'b';
  if(equal[0] == (char)aAt(equal, 1)) aFA(&out, "equal"); // "equal"
  aFree(&equal);
  return aAppend((char***)NULL, aStr("ex18"), aStr("equal")); }

char**ex19(void) {
  char*equal = aAppend((char**)NULL, 'a', 'b');
  (void)aAt2(equal, 1, 'd'); // 'a', 'd'
  aFA(&out, "%v %c", equal);
  aFree(&equal);
  return aAppend((char***)NULL, aStr("ex19"), aStr("a d")); }

char**ex17a(void) {
  char*letters = aAppend((char**)NULL, 'a', 'r', 't');
  aReplace(&letters, 0,0, aZAt(letters, 0));
  aFmtA(&out, "%v %c", letters); // "t a r t"
  aFree(&letters);
  return aAppend((char***)NULL, aStr("ex17a"), aStr("t a r t")); }

char**ex18a(void) {
  char*equal = aAppend((char**)NULL, 'n', 'o', '\0');
  *(char*)aZAtPtr(equal, 2) = 'l';
  aFmtA(&out, "%s", equal); // "lo"
  aFree(&equal);
  return aAppend((char***)NULL, aStr("ex18a"), aStr("lo")); }

char**ex19a(void) {
  char*equal = aAppend((char**)NULL, 'a', 'b');
  (void)aZAt2(equal, 0, 'd'); // 'a', 'd'
  aFA(&out, "%v %c", equal);
  aFree(&equal);
  return aAppend((char***)NULL, aStr("ex19a"), aStr("a d")); }



char**ex20(void) {
  int numPidgeons = 8;
  int pidgeonPeckSpeed_data[] = {10, 20, 190, 190, 200, 150, 10, 200};
  /**/
  int* PPS_array = aAppendArray((int**)NULL, numPidgeons, pidgeonPeckSpeed_data);

  aFA(&out, "%uz ", aIndexOf(PPS_array, 200)); // first occuring pidgeon with a peck speed of 200
  aFA(&out, "%uz ", aZIndexOf(PPS_array, 190)); // last occuring pidgeon with a speed of 190
  aFree(&PPS_array);
  return aAppend((char***)NULL, aStr("ex20"), aStr("4 3 ")); }

#if __has_extension(blocks)
char**ex21(void) {
  int*a = aAppend((int**)NULL, 1, 2, 3);
  aMap(a, ^(int*i){ aFA(&out, (intptr_t)"%i ", (uint64_t)*i); }); // prints "1 2 3 "
  aFree(&a);
  return aAppend((char***)NULL, aStr("ex21"), aStr("+1 +2 +3 ")); }

typedef struct pony { char*name; } pony;
static char*pony_name1 = "x", *pony_name2 = "y";
void oh_what_fun_function(pony**p) { aFA(&out, "Ride pony %s. ", (*p)->name); }
char**ex22(void) {
	pony*p;
	p = malloc(sizeof(pony)); p->name = pony_name1;
  pony**my_little_ponies = NULL;
  aA(&my_little_ponies, p);
	p = malloc(sizeof(pony)); p->name = pony_name2;
  aA(&my_little_ponies, p);
  /**/
  void (*ride_a_pony)(pony**) = oh_what_fun_function;
  aMap_FUNC(my_little_ponies, ride_a_pony);

	char*rider_says = "Yesss!";
	aMap_BLOCK(my_little_ponies,
	  ^(pony**pp, void*data){
	    aFA(&out, "ride %s? %s", (*pp)->name, (char*)data); },
	  rider_says);

	aMap(my_little_ponies, ^(pony**pp){ free((pony**)*pp); });
  aFree(&my_little_ponies);
  return aAppend((char***)NULL, aStr("ex22"), aStr("Ride pony x. Ride pony y. ride x? Yesss!ride y? Yesss!")); }
#endif

void function_requiring_pointer(int*p) { *p = *p+1; }
char**ex22a(void) {
  int*a = aAppend((int**)NULL, 1, 2, 3);
  void (*f)(int*ptr) = function_requiring_pointer;
  aMap_FUNC(a, f, NULL);
  aFA(&out, "%v %u", a);
  aFree(&a);
  return aAppend((char***)NULL, aStr("ex22a"), aStr("2 3 4")); }

#if __has_extension(blocks)
char**ex23(void) {
  int*a = aAppend((int**)NULL, 1, 2, 3, 4);
  aMap(a, ^(int*i){ aFA(&out, (intptr_t)"%i ", (uint64_t)(*i)+1); }); // increments items in a by 1
  aFree(&a);
  return aAppend((char***)NULL, aStr("ex23"), aStr("+2 +3 +4 +5 ")); }
#endif

#if __has_extension(blocks)
int return_1(size_t a) { (void)a; return 1; }
char**ex24(void) {
  size_t startPos = 0;
  int*array = aAppend((int**)NULL, 1, 2, 3, 4, 5);
  int(*f)(size_t) = return_1;
  int pos_delta = 1;
  /**/
  size_t pos = startPos;
  while(pos < aLength(array)) {
    pos_delta = f(pos); // do my stuff
    aFA(&out, "%i ", array[pos]);
    if(!pos_delta) break; // f(pos) returning 0 will `break` the loop
    // play safe with int promotion
    if(pos_delta > 0) pos += (size_t) pos_delta;
    else              pos -= (size_t)-pos_delta; }

  aFA(&out, "aLoop: ");
  aLoop(array, startPos, ^(size_t pos2){
    	/**/(void)pos2;
    	// do my stuff
    	aFA(&out, (intptr_t)"%ii ", (uint64_t)array[pos2]);
    	return pos_delta; } );
  aFree(&array);
  return aAppend((char***)NULL, aStr("ex24"), aStr("+1 +2 +3 +4 +5 aLoop: +1 +2 +3 +4 +5 ")); }
#endif

#if __has_extension(blocks)
char**ex25(void) {
  unsigned int*a = aAppend((unsigned int**)NULL, 1, 2, 3, 4);

  // print index 0 and 2
  aLoop(a, 0, ^(size_t n) {
    	// (uintptr_t) cast here, because we cant selectively suppress warnings in blocks
    	aFmtA(&out, (uintptr_t)"a[%ull] == %ui\n", n, aAt(a, n)); return 2; });

  // print index 3 and 1, then set them to new values
  aLoop(a, 3, ^(size_t n){
    	aFmtA(&out, (uintptr_t)"Doing a[%ull] = %ui\n", n, aAt(a, n-1));
    	(void)aAt2(a, n, aAt(a, n-1));
    	return -2; }); // a becomes {1, 1, 3, 3}
  aFA(&out, "%v %ui",a);
  aFree(&a);
  return aAppend((char***)NULL, aStr("ex25"), aStr("a[0] == 1\n"
                                                   "a[2] == 3\n"
                                                   "Doing a[3] = 3\n"
                                                   "Doing a[1] = 1\n"
                                                   "1 1 3 3")); }
#endif

char**ex25_1(void) {
  int
   *black = NULL,
   *white = NULL;
  if(aCmp(black, white)) aFmtA(&out, "We are all family");
  return aAppend((char***)NULL, aStr("ex25_1"), aStr("We are all family")); }

char**ex25_2(void) {
  int*filmBoredomLevel = aAppend((int**)NULL, 300, 600, 200, 100);
  aSort(filmBoredomLevel);
  aFmtA(&out, "[%v, %ui]", filmBoredomLevel); // prints "[100, 200, 300, 600]"

  aFmtA(&out, "\n");
  size_t index;
  if(aSearch(filmBoredomLevel, &index, 600))
    aFmtA(&out, "Most boring film 'Peppa Pig Goes on Holiday' is at index %uz", index);
  aFree(&filmBoredomLevel);
  return aAppend((char***)NULL, aStr("ex25_2"), aStr("[100, 200, 300, 600]\n"
                                                     "Most boring film 'Peppa Pig Goes on Holiday' is at index 3")); }

int sort_strcmp(char*a, char*b) { return strcmp(a,b) < 0; }
char*search_strcmp(char*a, char*b) { return (char*)(uintptr_t)strcmp(a,b); }
char**ex25_3(void) {
  char**verses = NULL;
  aAppend(&verses, "There is no greater love", "You are my beloved", "Because of His great love");
  aSortF_FUNC(verses, sort_strcmp);
  aFmtA(&out, "%v\n%s", verses);

	aFmtA(&out, "\n----\n");
	size_t index;
	char*key = "He gives power to the weak and strength to the powerless";
	if(aSearchF_FUNC(verses, &index, key, search_strcmp))
	  aFA(&out, "found verse at %uz", index);

  #if __has_extension(blocks)
	aSearchF_BLOCK(verses, &index, key, ^(char*a, char*b){ return (char*)(uintptr_t)strcmp(a,b); });
	#else
	aSearchF_FUNC(verses, &index, key, search_strcmp);
	#endif
	// tested in unittest_special.cpp
	//aSearchF_LAMBDA(verses, key, &index, [](char*a, char*b){ return (char*)strcmp(a,b); });
	aReplace(&verses, index, 0, key);
	aFA(&out, "inserted verse at %uz", index);

  aFmtA(&out, "\n----\n%v\n%s", verses);
  aFree(&verses);
  return aAppend((char***)NULL, aStr("ex25_3"), aStr("Because of His great love"
                                                     "\nThere is no greater love"
                                                     "\nYou are my beloved"
                                                     "\n----"
                                                     "\ninserted verse at 1"
                                                     "\n----"
                                                     "\nBecause of His great love"
                                                     "\nHe gives power to the weak and strength to the powerless"
                                                     "\nThere is no greater love"
                                                     "\nYou are my beloved")); }

char**ex25_4(void) {
  char *hymn = aStr("You opened my eyes to your wonders anew"
                 "You captured my heart with this love"
                 "Because nothing on Earth is as beautiful as you");
  aWriteF(outFILE, hymn);
  out = FILEtoArray(outFILE);
  aFree(&hymn);
  return aAppend((char***)NULL, aStr("ex25_4"), aStr("You opened my eyes to your wonders anew"
                 "You captured my heart with this love"
                 "Because nothing on Earth is as beautiful as you")); }

struct person { char*name; };
int64_t f(struct person*a, struct person*b) { return strcmp(a->name, b->name); }
char**ex25_5(void) {
	struct person key = { "myself" };
	uint8_t*people = aArray((uint8_t*)malloc(200),200,true); (void)aL2(people, sizeof(struct person));
	memcpy(people, &key, sizeof(struct person));
	size_t index;
	if(aSearchPS_FUNC(people, &index, &key, f, sizeof(struct person)))
		aFA(&out, "egomania is found at %uz", index);
	free(aMem(people));
  return aAppend((char***)NULL, aStr("ex25_5"), aStr("egomania is found at 0")); }

char**ex26(void) {
  char*string = NULL;
  /**/
  aAppendArray(&string,
  SIZE_MAX, "A wonderful story of how",
  SIZE_MAX, ", my saviour died for me");
  //FILE*file = fopen("hope.txt", "w");
  aWriteF(outFILE, string); // writes: "A wonderful story of how my saviour died for me"
  out = FILEtoArray(outFILE);
  aFree(&string);
  return aAppend((char***)NULL, aStr("ex26"), aStr("A wonderful story of how, my saviour died for me")); }

char**ex27(void) {
  aFmtA(&out, "h%cpp%c %u8", 'i', 'o', (uint8_t)2); // prints 'hippo 2'
  aFmtA(&out, "%s%c%s", "plat", 'y', "pus"); // prints 'platypus'

  aFmtA(&out, "  ");
  float fa=0.01f, fb=2.22f;
  double da=2.22*1000000;
  // print "0.01 2.22 -- 2.22e+06"
  aFA(&out, "%f %f -- %d",
      *(int32_t*)&fa,*(int32_t*)&fb,
      *(int64_t*)&da); // stop float->int conversion
  aFmtA(&out, "  ");

  char*abc = aAppend((char**)NULL, 'a', 'b', 'c');
  aFmtA(&out, "<%v, %c>", abc); //prints '<a, b, c>'

  int*nums = aAppend((int**)NULL, 0, 1, 2);
  aFmtA(&out, "[%v | %2i]", nums); //prints '[+0 | +1 | +10]'
  aFree(&abc); aFree(&nums);
  return aAppend((char***)NULL, aStr("ex27"), aStr("hippo 2platypus  0.01 2.22 -- 2.22e+06  <a, b, c>[+0 | +1 | +10]")); }

#include <setjmp.h>
#include <time.h>
char**ex28(void) {
  aFA(&out, "Lets play guess the array length\n");
  // guess too high, and an "out of bounds" exception will lose you the game
  char*array = NULL;
  srand((unsigned int)time(NULL));
  aMulti(&array, 0, 0,
    	   // a 0-99 length array
    	   /*rand()*/52 % 100, 1, (char[]){0});

  // aError exceptions are caught here
  aError = &aError_jmp;
  if(setjmp(expected_err)) {
    aFA(&out, "Oops, too far! Goodbye\n"); aFree(&array);
    return aAppend((char***)NULL, aStr("ex28"), aStr("Lets play guess the array length\n"
                                                     "Guess a length: "
                                                     "Error at _: out of bounds (length=52 but pos=100)"
                                                     "Oops, too far! Goodbye\n")); }
  
  for(;;) {
    aFA(&out, "Guess a length: ");
    size_t guess = 100;
    int input_err = 1 /*scanf("%zu", &guess)*/;
    if(input_err == 0 || input_err == EOF) {
    	aF(&out, "Oh dear, I didn't understand! Goodbye\n"); aFree(&array); exit(0); }
    
    // if guess is out of bounds, aAt will call aError_jmp
    // and aError_jmp behaves like an exception
    // i.e. it longjmps to setjmp(array_err) where the error is handled
    (void)aAt(array, guess);
    
    // good luck with winning
    aFA(&out, "The array might be longer...\n"); }
  return NULL; }


char**ex29(void) {
  //char *out = NULL;
  aFmtA(&out, "%ui %ui miss a few %ii %ii", 1, 2, 99, 100);
  return aAppend((char***)NULL, aStr("ex29"), aStr("1 2 miss a few +99 +100")); }


char**ex30(void) {
  //char *out = NULL;
  aFmtA(&out, "as easy as %2ic %3is %4ii", (char)1, (short)2, (int)3);
  return aAppend((char***)NULL, aStr("ex30"), aStr("as easy as +1 +2 +3")); }


char**ex31(void) {
  char*word = aStr("imperturbablorbably");
  //FILE*file = fopen("~/hope.txt", "w");
  FILE*file = outFILE;
  aFmtF(file, "Let me spell it out for you: %v, %c", word);
  out = FILEtoArray(outFILE);
  aFree(&word);
  return aAppend((char***)NULL, aStr("ex31"), aStr("Let me spell it out for you: i, m, p, e, r, t, u, r, b, a, b, l, o, r, b, a, b, l, y")); }




int main(int argsnum, char**args) {
  aF("running tests ...\n");
  unittest(ex1);
  unittest(ex2);
  //	unittest(ex3); // note: this example demonstrates undetected memory corruption
  //  unittest(ex4); // PAGE and docs no longer used
  unittest(ex5);
  unittest(ex6);
  unittest(ex7);
  unittest(ex8);
  unittest(ex8_2);
  unittest(ex9);
  unittest(ex10);
  unittest(ex11);
  unittest(ex12);
  unittest(ex13);
  unittest(ex14);
  unittest(ex14a);
  //unittest(ex14b);
  unittest(ex14c);
  unittest(ex15);
  unittest(ex16);
  unittest(ex16a);
  unittest(ex17);
  unittest(ex18);
  unittest(ex19);
  unittest(ex17a);
  unittest(ex18a);
  unittest(ex19a);
  unittest(ex20);
  #if __has_extension(blocks)
  unittest(ex21);
  unittest(ex22);
  #endif
  unittest(ex22a);
  #if __has_extension(blocks)
  unittest(ex23);
  unittest(ex24);
  unittest(ex25);
  #endif
  unittest(ex25_1);
  unittest(ex25_2);
  unittest(ex25_3);
  unittest(ex25_4);
  unittest(ex25_5);
  unittest(ex26);
  unittest(ex27);
  unittest(ex28);
  unittest(ex29);
  unittest(ex30);
  unittest(ex31);

  if(test_failed) { aF("tests failed\n"); exit(-1); }
  if(argsnum>1) { // remember test was successful
    FILE*outfile = fopen(args[1], "w");
    if(!outfile) { aF("couldn't find file %s",args[2]); exit(-1); } }
  aF("\n... tests passed\n"); }
