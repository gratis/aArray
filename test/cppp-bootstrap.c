/* ===== FREE =====
   Public domain.  No restriction, no need to accredit
     "As freely as you have received, freely give" -- Jesus

   
   ===== ABOUT ====
   `cppp` is a c-pre-pre-processor for c macros, and `$exec script`

   Why not use the c pre-processor?  If macros are involved in an error, compilers include information about them.  This can be messy.  `cppp` selectively hides them from the compiler's view

   I use it to clean c headers, and as a website generator (with `$exec script`)


   ===== USAGE ====
   Replace any #define you want with $define.  Begin them on a new line and end them with a blank line.  This allows macros of macros, multiline macros, and stops the need to join lines with `\`
   
      $define MACRO value
      $define FUNCTION(name, ...) name __VA_ARGS__

   These don't need to end in a blank line:
      $include a/file/path
      $exec script text

   Also:
      $$ becomes $
      $## concatonates like cpp ##

   Then run with:  cppp inputFile outputFile   
   
   ===== OTHER ====
   See test/ for usage examples
   NOTE: function param , and ) can be escaped with \
   NOTE: $exec newlines can be removed with \newline */

#include "../build/aArray-forTests.h"
#include <libgen.h>
#include <unistd.h>

#if defined(__clang__)
  #define UNITTEST_nowarn_start \
    AARRAY_nowarn_pedantic_cpp_start \
    _Pragma("clang diagnostic ignored \"-Wimplicit-fallthrough\"") \
    _Pragma("clang diagnostic ignored \"-Wdisabled-macro-expansion\"") \
    _Pragma("clang diagnostic ignored \"-Wmissing-prototypes\"") \
    _Pragma("clang diagnostic ignored \"-Wbad-function-cast\"") \
    _Pragma("clang diagnostic ignored \"-Wpadded\"")
#elif defined(__GNUC__)
  #define UNITTEST_nowarn_start \
    AARRAY_nowarn_pedantic_cpp_start
#endif
#define UNITTEST_nowarn_end \
  AARRAY_nowarn_pedantic_cpp_end

UNITTEST_nowarn_start

typedef enum {false, true} bool;

static char*macroChars = NULL; // macro name can contain these

char*fileToArray(char *filename) {
  FILE*file = fopen(filename,"r");
  if(!file) { printf("file \"%s\" not found\n",filename); exit(EXIT_FAILURE); }
  fseek(file,0,SEEK_END);
  size_t file_size = (size_t)ftell(file);
  rewind(file);

  char*buffer = NULL;
  aMulti(&buffer, 0, 0, 0, file_size, (uintptr_t)NULL);
  size_t read_size = fread(buffer,sizeof(char),file_size,file);
  if(read_size!=file_size) { printf("read file failure\n"); exit(EXIT_FAILURE); }
  fclose(file);
  return buffer; }

char*processToArray(char*shellprocess) {
  FILE*file = popen(shellprocess,"r");
  if(!file) { printf("couldn't run $exec %s\n",shellprocess); exit(EXIT_FAILURE); }
  char*buffer = NULL;
  char*line;
  char linebuffer[1024];
  while(true) {
    line = fgets(linebuffer, sizeof linebuffer, file);
    if(!line) {
      if(ferror(file)) { printf("couldn't get result of $exec %s\n",shellprocess); exit(EXIT_FAILURE); }
      break; }
    aAA(&buffer, SIZE_MAX, (uintptr_t)linebuffer); }
  pclose(file);
  return buffer; }

typedef struct def {
  char*name;
  struct def**params;
  char*value;
  bool isFunction; } def;

__attribute((noreturn)) void error(char**code, size_t pos) {
  size_t start = pos;
  size_t end = pos;
  if(pos > aL(*code)) exit(EXIT_FAILURE);
  while(start
        && (*code)[start]!='\r' && (*code)[start]!='\n') start--;
  while(end < aL(*code)
        && (*code)[end  ]!='\r' && (*code)[end  ]!='\n') end++;
  char*line = aAppendArray((char**)NULL, (end-start), (uintptr_t)&((*code)[start]));
  printf(".  Error line contained:\n");
  // not freeing defs here, but hey
  aWriteE(line); aFree(&line); aFree(code);
  printf("\n\n");
  exit(EXIT_FAILURE); }

/* // works, but not used
char*parseString(char*code, size_t*_pos) {
  size_t pos = (*_pos)+1, start_pos = pos;
 start:
  while(pos < aLength(code) && code[pos]!='"') pos++;
  if(pos < aLength(code) && code[pos-1]=='\\') { pos++; goto start; }
  if(pos == aL(code)) { printf("string did not end in \""); error(&code, start_pos); }
  *_pos = pos+1;
  return aAA((char**)NULL, pos - start_pos, &code[start_pos]); }

char*parseFileName(char*code, size_t*_pos) {
  size_t pos = *_pos, start_pos = pos;
 start:
  while(pos < aLength(code) &&
        ((code[pos]>='a' && code[pos]<='z') ||
         (code[pos]>='A' && code[pos]<='Z') ||
         (code[pos]>='0' && code[pos]<='9') ||
         code[pos]=='_' || code[pos]=='/' || code[pos]=='.')) pos++;
  if(pos < aLength(code) && code[pos]=='\\') { pos++; goto start; }
  *_pos = pos;
  return aAA((char**)NULL, pos - start_pos, &code[start_pos]); } */

char*parseMacroName(char*code, size_t*_pos, bool getName) {
  size_t pos = *_pos, start_pos = pos;
  while(pos < aLength(code) &&
        ((code[pos]>='a' && code[pos]<='z') ||
         (code[pos]>='A' && code[pos]<='Z') ||
         (code[pos]>='0' && code[pos]<='9') ||
         code[pos]=='_')) pos++;
  *_pos = pos;
  if(!getName) return NULL;
  return aAA((char**)NULL, pos - start_pos, (uintptr_t)&code[start_pos]); }

void parseParams(char** code, size_t* pos, def*** params) {
  //BUG("pP ")
  bool createParams = true;
  if(aL(*params)) createParams = false; // instantiate parameter values
  size_t paramNo = 0;

  if(*pos >= aL(*code) || (*code)[*pos]!='(') {
    printf("functional define not given parameters"); error(code,*pos); }
  (*pos)++; // move beyond first '('
  
  while(true) {
    while(((*code)[*pos]==' ' || (*code)[*pos]=='\t' || (*code)[*pos]=='\n' || (*code)[*pos]=='\r') && *pos < aL(*code)) (*pos)++;
    size_t param_start = *pos;
    char matchComma = ','; // let ... match over commas too

    //if(aL(*params)-1 == paramNo){BUG("    params=%s   ",(*params)[paramNo]->name);}
    if(!createParams && aL(*params)-1 == paramNo
       && aL((*params)[paramNo]->name) == strlen("__VA_ARGS__")
       && strncmp((*params)[paramNo]->name, "__VA_ARGS__",
                  strlen("__VA_ARGS__"))==0)
      matchComma = ')';
    
    while(*pos < aL(*code)
          && ((*code)[(*pos)-1]=='\\'
              || ((*code)[*pos]!=matchComma
                  && (*code)[*pos]!=')'))) {
      size_t inner = 0;
      if((*code)[*pos]=='(') {
        inner++;
        while(*pos < aL(*code) && inner) {
          (*pos)++;
          if((*code)[*pos]=='(') inner++;
          else if((*code)[*pos]==')') inner--; } }
      (*pos)++; }
    
    size_t param_end = (*pos)-1;
    while(((*code)[param_end]==' ' || (*code)[param_end]=='\t' || (*code)[param_end]=='\n' || (*code)[param_end]=='\r') && param_end < aL(*code)) (param_end)--;
    while(param_end && (*code)[param_end]==' ') param_end--;
    size_t param_len = (param_end+1) - param_start;
    
    // this parse code handles both definition time, and call time
    // if a macro is being defined, then populate d->name
    // if a macro is being called, then populate d->value
    def* dp;
    char* nameValue = NULL;
    if(*pos && param_end+1 > param_start)
      nameValue = aAppendArray((char**)NULL,
                               param_len, (uintptr_t)&((*code)[param_start]));
    if(createParams) {
      // create param
      dp = (def*)malloc(sizeof(def));
      //dpNo++;
      if(!dp) { printf("out of memory"); error(code,*pos); }
      dp->name = nameValue;
      //BUG("  __%s__  \n",dp->name);
      dp->value = NULL;
      dp->params = NULL;
      dp->isFunction = false;
      aAppend(params, (uintptr_t)dp); }
    else {// fill out existing param's value
      if(matchComma==',' && paramNo >= aL(*params)) {
        printf("too many parameters"); error(code,*pos); }
      aAppendArray(&((*params)[paramNo]->value), aL(nameValue), (uintptr_t)nameValue);
      // strip \\n \\, \\) from param value
      char*value = (*params)[paramNo]->value;
      size_t n = aL(value)-1; if(aL(value)) while(n--) if(value[n]=='\\' &&
        (value[n+1]=='\n' || value[n+1]=='\r' || value[n+1]==',' || value[n+1]==')'))
          aD(&value, n, 1);
      (*params)[paramNo]->value = value;
      aFree(&nameValue);
      //BUG("append__%s__",(*params)[paramNo]->value);
      if(matchComma==',') paramNo++; }
    
    while((*code)[*pos]==' ' && *pos < aL(*code)) (*pos)++;
    if(*pos == aL(*code)) {
      printf("end of file reached while parsing parameters"); error(code,*pos); }
    if((*code)[*pos]==',') (*pos)++;
    else if((*code)[*pos]==')') { (*pos)++; break; }
    else { printf("unknown parameter given"); error(code,*pos); } } }

void applyDefs(char** code, size_t pos, def*** defs, bool isParams);
  
void parseDef(char** code, size_t* _pos, def*** defs) {
  //BUG("pD ")
  // $ encountered.  It will be one of:
  //    $## concatonation, $define Name Value, or $define Function(Params) Value
  size_t pos = *_pos;
  if(pos < aL(*code)-1 && (*code)[pos+1]=='$') aDelete(code, pos, 1);
  else if(pos < aL(*code)-1 && (*code)[pos+1]=='#') {
    if(pos < aL(*code)-2 && (*code)[pos+2]=='#') {
      // concat
      // apply defs to next token, then concat, and re-apply defs to concat
      size_t concatpos = pos; pos += 3;
      parseMacroName(*code, &pos, false);
      //BUG("%zu--",pos-concatpos);
      size_t codeChunk_len = pos-concatpos-3;
      if(!codeChunk_len) aDelete(code, concatpos, 3);
      else {
        char* codeChunk = aAppendArray((char**)NULL,
                                       codeChunk_len, (uintptr_t)&((*code)[concatpos+3]));
        //printf(">>");aWrite(codeChunk);printf("<<");
        applyDefs(&codeChunk, 0, defs, false);
        aReplaceArray(code, concatpos, 3+codeChunk_len, aL(codeChunk), (uintptr_t)codeChunk);
        aFree(&codeChunk);
        pos = concatpos-1;
        while(pos && aIndexOf(macroChars, (*code)[pos])!=(size_t)-1) pos--;
        *_pos = pos; } }
    else {
      /* stringify */
      printf("$# stringify must be $##"); error(code,pos); } }
  else if(aL(*code)-pos > strlen("define ")
          && strncmp(&((*code)[pos]), "$define ", strlen("$define ")) == 0) {
    if(pos) { pos--;
      while(pos && ((*code)[pos]==' ' || (*code)[pos]=='\t')) pos--;
      if(pos && (*code)[pos]!='\r' && (*code)[pos]!='\n') {
        printf("$define must begin on a blank line, or $ must become $$"); error(code, *_pos); } }
    size_t dollar_start = pos; pos = *_pos;
    // parse $define like c-preprocessor #define
    def* d = (def*)malloc(sizeof(def));
    //defNo++;
    if(!d) { printf("out of memory"); error(code,pos); }
    
    pos += strlen("$define ");
    while(pos < aLength(*code)
          && ((*code)[pos]==' ' || (*code)[pos]=='\t')) pos++;

    size_t defname_start = pos;
    while(pos < aLength(*code)
          && (*code)[pos]!=' ' && (*code)[pos]!='\t'
          && (*code)[pos]!='\r' && (*code)[pos]!='\n' && (*code)[pos]!='(') pos++;
    size_t defname_len = pos - defname_start;
    d->name = aAppendArray((char**)NULL,
                             defname_len, (uintptr_t)&((*code)[defname_start]));
    
    //BUG("building %s\n",d->name);
    if(!aL(d->name)) { printf("no name given for $define"); error(code,pos); }
    d->params = NULL;
    if((*code)[pos]=='(') {
      d->isFunction = true;
      parseParams(code, &pos, &(d->params)); }
    else d->isFunction = false;
    while(pos < aL(*code)
          && ((*code)[pos]==' ' || (*code)[pos]=='\t')) pos++;
    bool blankLine = false;
    if((*code)[pos]=='\r' || (*code)[pos]=='\n') {
      blankLine = true; pos++; }
    size_t defvalue_start = pos;
    
    // def line returns done like the c pre-processor
    //while(pos < aLength(*code)
    //      && (((*code)[pos-1]=='\\' || ((*code)[pos]!='\r'
    //                                        && (*code)[pos]!='\n')))) pos++;
    // def line returns allowed until first blank line
    while(pos < aL(*code)) {
      if((*code)[pos]=='\r' || (*code)[pos]=='\n') {
        // lets not bother with this... even though it's slightly more correct for some file formats
        //if((*code)[pos]=='\r' && pos+1 < aL(*code) && (*code)[pos+1]=='\n') pos++;
        if(blankLine) break;
        blankLine = true; }
      else if((*code)[pos]!='\t' && (*code)[pos]!=' ') blankLine = false;
      pos++; }
    size_t to_blankline_len = (pos-1) - dollar_start + (blankLine? 1 : 0);
    //while(pos && ((*code)[pos]=='\t' || (*code)[pos]==' '
    //              || (*code)[pos]=='\r' || (*code)[pos]=='\n')) pos--;
    size_t defvalue_len = (pos-1) - defvalue_start;
    if(defvalue_start > pos-1) defvalue_len = 0;
    d->value = aAppendArray((char**)NULL,
                            defvalue_len,  (uintptr_t)&((*code)[defvalue_start]));
    //if(*(d->value)) {fprintf(stderr,"--");aWriteE(d->value); fprintf(stderr,"--"); }
    aAppend(defs, (uintptr_t)d);
    //printf("\n:");aWrite(d->name);printf("->(");aWrite(d->value);printf(")");
    (*_pos)--;
    //printf("--%c--",aAt(*code,); exit(EXIT_FAILURE);
    
    aDelete(code, dollar_start, to_blankline_len); }
  else {
    //stop enforcing $ --> $$
    //printf("I don't recognize your dollar usage:  either $$,  $##,  $define MACRO,  $define FUNCTION(...),  $include,  or $exec");
    //error(code,pos);
    } }

int strncmpMSan(char*a, char*b, size_t len) {
   size_t n = (size_t)-1; while(++n < len) if(a[n]!=b[n]) return 1;
   return 0; }
static size_t applyNo;
static def**all_defs = NULL;
void mapToZero(size_t*val) { *val=0; }
void freeParamValue(def**param) { aFree(&((*param)->value)); }
void applyDefs(char**code, size_t pos, def***defs, bool isParams) {
  // apply defs to code
  //BUG("aD ")
  size_t* matchingDefs = NULL;
  if(aL(*defs)) aMulti(&matchingDefs, 0, 0,
                       aL(*defs), 1, (uintptr_t)(size_t[]){0});
  while(pos < aL(*code)) {
    if((*code)[pos]=='$') {
      applyNo = 0;
      if(aL(*code)-pos > strlen("$exec ")
         && strncmpMSan(&((*code)[pos]), "$exec ", strlen("$exec ")) == 0){
        size_t dollar_start = pos;
        size_t script_start = pos += strlen("$exec ");
        while(pos < aLength(*code)
              && (*code)[pos]!='\r' && (*code)[pos]!='\n') {
          if((*code)[pos]=='\\' && pos+1 < aL(*code)) pos++;
          pos++; }
        char*script = aAA((char**)NULL, pos-script_start,
                          (uintptr_t)&(*code)[script_start]);
        // strip \\n
        size_t n = aL(script); if(aL(script)) while(--n) if(script[n-1]=='\\' && (script[n]=='\n' || script[n-1]=='\r')) {
              aD(&script, n-1, 1); }
        // need to macro expand script
        // PARAMS
        if(*defs != all_defs)
          applyDefs(&script, 0, defs, false);
        // MACROS / FUNCTIONS
        applyDefs(&script, 0, &all_defs, false);
        aA(&script, '\0');
        char*scriptResult = processToArray(script);
        aRA(code, dollar_start, pos-dollar_start,
            aL(scriptResult), (uintptr_t)scriptResult);
        pos = dollar_start + aL(scriptResult);
        aFree(&script); aFree(&scriptResult); }
      else {
        parseDef(code, &pos, defs);
        if(aL(*defs) > aL(matchingDefs)) aAppend(&matchingDefs, 0);
        //BUG("==== mD=%zu d=%zu\n\n",aL(matchingDefs), aL(*defs));
        aMap_FUNC(matchingDefs, mapToZero, NULL); } }
    else if(aIndexOf(macroChars,(*code)[pos])==(size_t)-1) {
      // token break, since cannot occur in a macro name
      aMap_FUNC(matchingDefs, mapToZero, NULL); }
    else {
      size_t o = (size_t)-1; while(++o < aL(matchingDefs)) {
        if(pos < aL(*code) && aL(aAt((*defs),o)->name)>aAt(matchingDefs,o) &&
           (*code)[pos]==(*defs)[o]->name[matchingDefs[o]]) {
          //BUG("m %i%s\n", matchingDefs[o],(*defs)[o]->name);
          if(aL((*defs)[o]->name)==++matchingDefs[o]
             // name ends with token break
             && (pos==aL(*code)-1 ||
                 aIndexOf(macroChars, (*code)[pos+1])==(size_t)-1)
             // name begins with a token break
             && (pos-matchingDefs[o]==0
                 || aIndexOf(macroChars,
                             (*code)[pos-matchingDefs[o]])==(size_t)-1)
             // allow higher order macros
             // i.e. dont evaluate 'function macros until they're
             // followed by a '('
             && (!(*defs)[o]->isFunction
                 || (pos+1 < aL(*code) && (*code)[pos+1]=='('))
             ) { //---------
            // match
            def* d = (*defs)[o];
            pos++;

            size_t pos_beforeDef = pos - aL(d->name);
            //printf("\n applying "); aWrite(d->name); aWrite(d->value); printf(" ");
            char* rhs = aConcat((char**)NULL, d->value);
            if(d->isFunction) {
              def** params = aConcat((def***)NULL, d->params);
              def* vargsparam = aL(params)? params[aL(params)-1] : NULL;
              //vargsparam = NULL;
              if(vargsparam && vargsparam->name
                 && aL((vargsparam->name))==3
                 && strncmp(vargsparam->name, "...", 3)==0) {
                aReplaceArray(&(vargsparam->name), 0, 3,
                              SIZE_MAX, (uintptr_t)"__VA_ARGS__"); }
              else vargsparam = NULL;
              parseParams(code, &pos, &params);
              applyDefs(&rhs, 0, &params, true); // where params are sub-defs
              //global_params = params;
              aMap_FUNC(params, freeParamValue, NULL);
              //aLoop_FUNC(params, 0, loopFreeParams, NULL);
              if(vargsparam)
                aReplaceArray(&(vargsparam->name), 0, strlen("__VA_ARGS__"),
                              SIZE_MAX, (uintptr_t)"...");
              aFree(&params); }
            
            aReplaceArray(code, pos_beforeDef, pos - pos_beforeDef,
                          aL(rhs), (uintptr_t)rhs);

            if(applyNo++>100000) {
              printf("looped more than 100000 times"); error(code,pos); }
            //BUG("%s%s\n", d->name, d->params?"()":"");
            if(isParams) pos += aL(rhs) - (pos - pos_beforeDef) - 1;
            else pos = pos_beforeDef - 1;
            aFree(&rhs);
            //else pos += aL(rhs) - (pos - pos_beforeDef);
            aMap_FUNC(matchingDefs, mapToZero, NULL); } }
        else matchingDefs[o] = 0; } }
    pos++; }
  aFree(&matchingDefs); }

void freeDefs(def** def, void*data) { (void)data;
  if((*def)->name) aFree(&(*def)->name);
  if((*def)->params) {
    aMap_FUNC((*def)->params, freeDefs, NULL);
    aFree(&(*def)->params); }
  if((*def)->value) aFree(&(*def)->value);
  free(*def); }

void pathStripFile(char*path) {
  size_t n = (size_t)-1;
  while(path[++n]!='\0') {}
  while(path[--n]!='/') {}
  path[n] = '\0'; }

void applyIncludes(char**code, char*inputDirectory) {
  size_t pos = (size_t)-1;
  while(++pos < aL(*code) - strlen("$include ")) {
    if(strncmp(&((*code)[pos]), "$include ", strlen("$include ")) == 0) {
      size_t dollar_start = pos;  pos += strlen("$include ");
      while(pos < aLength(*code)
            && ((*code)[pos]==' ' || (*code)[pos]=='\t')) pos++;
      char*fileName = NULL;
      size_t fStart = pos, fEnd;
      while(pos < aLength(*code)) {
        if((*code)[pos]=='\r' || (*code)[pos]=='\n') break;
        if((*code)[pos]=='\\' && pos+1 < aL(*code)) pos++;
        pos++; }
      fEnd = pos;
      while(fEnd < aL(*code) && ((*code)[fEnd]==' ' || (*code)[fEnd]=='\t')) fEnd--;
      aAA(&fileName, fEnd-fStart, (uintptr_t)&(*code)[fStart]);
      aA(&fileName, '\0');
      // skip \n at end of $include
      ////if(pos < aL(*code)) pos++;
      // change future includes to be relative to included file location
      chdir(inputDirectory);
      char*fullFileName = realpath(fileName, NULL);
      if(!fullFileName) { printf("\n\n$include %s not found", fileName); error(code, pos); }
      char*file = fileToArray(fullFileName);
      pathStripFile(fullFileName);
      applyIncludes(&file, fullFileName);
      
      aRA(code, dollar_start, pos-dollar_start, aL(file), (uintptr_t)file);
      pos = dollar_start+aL(file);
        
      aFree(&fileName); free(fullFileName); aFree(&file); } } }




int main(int argsnum, char** args) {
  if(argsnum!=3) {
    printf("%d argument%s found.  But need:  cppp inputFile outputFile\n\n", argsnum-1, argsnum-1==1?"":"s");

    char*help = aStr(
      "inputFile can contain:\n"
      "^$$^   -- simply replace with $\n"
      "^$##^  -- concatonate like cpp ##\n"
      "^$define MACRO value^\n"
      "     -- replace any following MACRO with value\n"
      "        all $defines end with a blank line\n"
      "        this makes macros of macros possible\n"
      "^$define FUNCTION(param, ...) param$##__VA_ARGS__^\n"
      "     -- replace FUNCTION with param$##__VA_ARGS__\n"
      "        __VA_ARGS__ is replaced with multi-arg ...\n"
      "^$include file^\n"
      "     -- include file and expand any macros within\n"
      "^$exec script^\n"
      "     -- include result of script unexpanded\n");
    size_t next;
    if(!getenv("TERM")) {
      while((next = aIndexOf(help, '^')) != (size_t)-1)
        aDelete(&help, next, 1);
      aWrite(help); }
    else {
      // convert normal^colored^normal
      // when inside a tty
      const char* style[] = {"\033[1;21m", "\033[22;37m"};
      bool inMarkup = false;
      // assume we got at least one '^'
      while((next = aIndexOf(help, '^')) != (size_t)-1) {
        help[next] = '\0';
        if(inMarkup) printf("%s%s%s", style[0], help, style[1]);
        else         printf("%s", help);
        aDelete(&help, 0, next+1);
        inMarkup = !inMarkup; }
      aWrite(help); }
    exit(EXIT_FAILURE); }
  FILE* outfile = fopen(args[2], "w");
  if(!outfile) { printf("couldn't find output file %s\n",args[2]); exit(EXIT_FAILURE); }
  
  char*code = fileToArray(args[1]);
  char*basePath = realpath(args[1], NULL);
  pathStripFile(basePath);
  applyIncludes(&code, basePath);
  free(basePath);
  
  aAppend(&macroChars,
          'a','b','c','d','e','f','g','h','i','j',
          'k','l','m','n','o','p','q','r','s','t',
          'u','v','w','x','y','z',
          'A','B','C','D','E','F','G','H','I','J',
          'K','L','M','N','O','P','Q','R','S','T',
          'U','V','W','X','Y','Z',
          '0','1','2','3','4','5','6','7','8','9','_');

  //def** defs = NULL;
  applyDefs(&code, 0, &all_defs, false);

  aWriteF(outfile, code);

  aFree(&code); aFree(&macroChars);
  aMap_FUNC(all_defs, freeDefs, NULL);
  aFree(&all_defs); 
  fclose(outfile); }
  
UNITTEST_nowarn_end
